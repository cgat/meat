class BasicAligner < MeatAligner
  process :apply_transform
  process :crop_to_fit
end
