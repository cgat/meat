class MeatAligner < ImageReggy::Aligner::Base
  include ImageReggy::ROpenCV
  transform_type :srt
  control_point_generator :manual

  warning "Reference image will increase in size! This may result in a large image with poor image quality." + \
  " Make sure you have selected an image with a large resolution", if: :reference_upscale


  def export(image_pair)
    initiator = image_pair.initiator
    reference = image_pair.reference
    initiator_derivative = initiator.derivative
    reference_derivative = reference.derivative
    if initiator.has_changed?
      parent = initiator_derivative.captureable
      parent.capture_images.create(image: File.open(initiator.image.file.path), image_state: "MASTER")
    end
    if reference.has_changed?
      parent = reference_derivative.captureable
      parent.capture_images.create(image: File.open(reference.image.file.path), image_state: "MASTER")
    end
    true
  end

  def reference_upscale
    if @t_reference.present?
      scale, rot, translate = extract_srt(@t_reference)
      if (scale>1.1)
        true
      else
        false
      end
    else
      false
    end
  end

end
