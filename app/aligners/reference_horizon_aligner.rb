class ReferenceHorizonAligner < MeatAligner
  process :apply_rotation_to_initiator
  process :apply_transform
  process :crop_to_fit

  def apply_rotation_to_initiator
    manipulate! do |img1, img2, transform1, transform2|
      s,r,t= extract_srt(transform2)
      #first recreate transform2 as just a scale matrix
      transform2 = cv::get_rotation_matrix2d(cv::Point2f.new(0,0), 0, s)
      #now add translation
      transform2[0,2]=t[0]
      transform2[1,2]=t[1]
      #when creating transform1, shift the center based on the translation
      transform1 = cv::get_rotation_matrix2d(cv::Point2f.new(t[0],t[1]), r*180/Math::PI, 1)
      [img1, img2, transform1, transform2]
    end
  end

end
