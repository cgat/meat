class PublisherForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  CRITERIA_CAPTURES_LIST = [["No Captures", "no_caps"], \
                                            ["All Captures", "all_caps"], \
                                            ["Captures With Comparisons", "cap_with_comp"],\
                                            ["Captures With Mastered Comparisons", "cap_with_master_comp"], \
                                            ["Do Not Update Published Captures", "do_nothing_caps"]]
  CRITERIA_HCAPTURES_LIST = [["No Historic Captures", "no_hcaps"], \
                                            ["All Historic Captures", "all_hcaps"],\
                                            ["Historic Captures With Comparisons", "hcap_with_comp"], \
                                            ["Historic Captures With Mastered Comparisons", "hcap_with_master_comp"], \
                                            ["Do Not Update Published Historic Captures", "do_nothing_hcaps"]]
  validates_presence_of :update_criteria_captures, :update_criteria_historic_captures

  attr_accessor :update_criteria_captures, :update_criteria_historic_captures, :captures_data, :historic_captures_data, :jobs_queued, :jobs_processing

  def initialize
    preferences = Preference.first
    self.update_criteria_captures = preferences.update_criteria_captures
    self.update_criteria_historic_captures = preferences.update_criteria_historic_captures

    caps_published = Capture.published
    caps_to_publish = if self.update_criteria_captures !="do_nothing_caps" && !self.update_criteria_captures.blank?
      captures_to_update(self.update_criteria_captures)
    else
      []
    end
    caps_pending_publication = caps_to_publish - caps_published
    caps_pending_unpublication = caps_published - caps_to_publish
    caps_remaining_published = caps_published - caps_pending_unpublication
    caps_unpublished = Capture.where(published: [nil, false])#(published==nil)|(published==false)}
    self.captures_data = [ { value: caps_remaining_published.count, color: "#F38630", title: "Published"}, \
                                     { value: caps_unpublished.count, color: "#69D2E7", title: "Unpublished"}, \
                                     { value: caps_pending_unpublication.count, color: "rgb(209, 115, 102) ", title: "Pending Unpublication"}, \
                                     { value: caps_pending_publication.count, color: "rgb(102, 209, 110)", title: "Pending Publication"}]

    hcaps_published = HistoricCapture.published
    hcaps_to_publish = if self.update_criteria_historic_captures !="do_nothing_hcaps" && !self.update_criteria_historic_captures.blank?
      historic_captures_to_update(self.update_criteria_historic_captures)
    else
      []
    end
    hcaps_pending_publication = hcaps_to_publish - hcaps_published
    hcaps_pending_unpublication = hcaps_published - hcaps_to_publish
    hcaps_remaining_published = hcaps_published - hcaps_pending_unpublication
    hcaps_unpublished = HistoricCapture.where(published: [nil, false])#{(published==nil)|(published==false)}
    self.historic_captures_data = [ { value: hcaps_remaining_published.count, color: "#F38630", title: "Published"}, \
                                                 { value: hcaps_unpublished.count, color: "#69D2E7", title: "Unpublished"}, \
                                                 { value: hcaps_pending_unpublication.count, color: "rgb(209, 115, 102)", title: "Pending Unpublication"}, \
                                                 { value: hcaps_pending_publication.count, color: "rgb(102, 209, 110)", title: "Pending Publication"}]

    self.jobs_queued = Sidekiq::Queue.new("publishing").size
    self.jobs_processing = Sidekiq::Workers.new().size
  end

  def persisted?
    false
  end

  def submit
    update_captures
    update_historic_captures
    self.jobs_queued = Sidekiq::Queue.new("publishing").size
    self.jobs_processing = Sidekiq::Workers.new().size
  end

  def update_captures
    if caps_to_publish = captures_to_update(self.update_criteria_captures)
      caps_to_unpublish = Capture.published - caps_to_publish
      caps_to_publish.each { |cap| PublishWorker.perform_async(cap.id, cap.class.name) } #cap.publish and cap.publish_ancestors
      caps_to_unpublish.each { |cap| UnpublishWorker.perform_async(cap.id, cap.class.name) } #cap.unpublish and cap.unpublish_ancestors
    end
  end

  def update_historic_captures
    if hcaps_to_publish = historic_captures_to_update(self.update_criteria_historic_captures)
      hcaps_to_unpublish = HistoricCapture.published - hcaps_to_publish
      hcaps_to_publish.each { |hcap| PublishWorker.perform_async(hcap.id, hcap.class.name)  } #hcap.publish and hcap.publish_ancestors
      hcaps_to_unpublish.each { |hcap| UnpublishWorker.perform_async(hcap.id, hcap.class.name) } #hcap.unpublish and hcap.unpublish_ancestors
    end
  end

  def captures_to_update(criteria_identifier)
    case criteria_identifier
    when "all_caps"
      Capture.with_capture_images.readonly(false)
    when "cap_with_comp"
      Capture.with_capture_images.has_comparisons_with_images.readonly(false)
    when "cap_with_master_comp"
      Capture.mastered.readonly(false)
    when "no_caps"
      []
    when 'do_nothing_caps', nil, ""
      false
    else
      raise ArgumentError, "The criteria_identifier, #{criteria_identifier}, for captures is invalid"
    end
  end

  def historic_captures_to_update(criteria_identifier)
    case criteria_identifier
    when "all_hcaps"
      HistoricCapture.with_capture_images.readonly(false)
    when "hcap_with_comp"
      HistoricCapture.with_capture_images.has_comparisons_with_images.readonly(false)
    when "hcap_with_master_comp"
      HistoricCapture.mastered.readonly(false)
    when "no_hcaps"
      []
    when 'do_nothing_hcaps', nil, ""
      false
    else
      raise ArgumentError, "The criteria_identifier, #{criteria_identifier}, for historic captures is invalid"
    end
  end

  def criteria_captures_human_name
    (pair = CRITERIA_CAPTURES_LIST.rassoc(self.update_criteria_captures) and pair.first) or "Criteria Currently Not Set"
  end

  def criteria_historic_captures_human_name
    (pair = CRITERIA_HCAPTURES_LIST.rassoc(self.update_criteria_historic_captures) and pair.first) or "Criteria Currently Not Set"
  end

end
