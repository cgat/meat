class SettingsForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  validates_presence_of :mlp_library_local_root

  attr_accessor :mlp_library_local_root, :admin_users, :update_criteria_captures, :update_criteria_historic_captures

  def initialize
    @preferences = Preference.first or raise StandardError, "A preference record must be set before this application can be used"
    @users = User.order(:email)
    self.admin_users = @users.select{|u| u.admin==true }
    self.mlp_library_local_root = @preferences.mlp_library_local_root
    self.update_criteria_captures = @preferences.update_criteria_captures
    self.update_criteria_historic_captures = @preferences.update_criteria_historic_captures
  end

  def persisted?
    false
  end

  def submit(params)
    self.mlp_library_local_root = params[:mlp_library_local_root]

    previous_admins = User.where(admin: true)
    self.admin_users = params[:admin_users].reject{|a|a.blank?}.map{|u|User.find(u)}
    admins_to_revoke = self.admin_users - previous_admins | previous_admins - self.admin_users

    self.update_criteria_captures = params[:update_criteria_captures]
    self.update_criteria_historic_captures = params[:update_criteria_historic_captures]
    if valid?
      @preferences.mlp_library_local_root = self.mlp_library_local_root
      @preferences.update_criteria_captures = self.update_criteria_captures
      @preferences.update_criteria_historic_captures = self.update_criteria_historic_captures

      self.admin_users.each{|u| u.admin=true}
      admins_to_revoke.each{|u| u.admin=false}

      if @preferences.valid? && self.admin_users.all?{|u|u.valid?}
        @preferences.save
        self.admin_users.each{|u|u.save}
        admins_to_revoke.each{|u|u.save}
      else
        return false
      end
    else
      return false
    end
    return true
  end


  def user_collection_list
    @users.map { |u| [u.email, u.id.to_s]}
  end

  def update_criteria_captures_collection_list
    PublisherForm::CRITERIA_CAPTURES_LIST
  end

  def update_criteria_historic_captures_collection_list
    PublisherForm::CRITERIA_HCAPTURES_LIST
  end

end
