class BulkImportForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  validates :image_state, presence: true
  validates :images, presence: true

  attr_accessor :image_state, :images, :parent_class, :parent_id, :association

  def persisted?
    false
  end

  def initialize(params)
    self.parent_class = params[:parent_class]
    self.parent_id = params[:parent_id]
    self.association = params[:association]
    raise StandardError, "The parent object #{parent_class}/#{parent_id} does not respond to #{association}" unless parent_and_association_valid?
    self.image_state = params[:image_state]
    self.images = params[:images]
  end

  def submit
    build_captures
    if valid? && parent.send(association).all?{|cap| cap.valid? }
      parent.save!
      true
    else
      false
    end
  end

  def parent
    @parent ||= parent_class.camelize.constantize.find(parent_id)
  end

  def parent_and_association_valid?
    parent.respond_to?(association)
  end

  def build_captures
    return if images.blank?
    images.each do |img|
      cap = parent.send(association).build
      cap.capture_images.build(image: img, image_state: image_state)
    end
  end

end
