class PublishWorker
  include Sidekiq::Worker
  sidekiq_options queue: "publishing"

  def perform(object_id, object_type)
    object = object_type.constantize.find(object_id)
    object.publish and object.publish_ancestors
  end
end
