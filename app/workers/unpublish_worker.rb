class UnpublishWorker
  include Sidekiq::Worker
  sidekiq_options queue: "publishing"

  def perform(object_id, object_type)
    object = object_type.constantize.find(object_id)
    object.unpublish and object.unpublish_ancestors
  end
end
