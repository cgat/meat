require 'active_support/concern'


##
#Filesystemable is a module that allows the storage of files in a hierarchy file structure that
#reflects the model relationships. The leaf nodes must be files, while intermediate nodes
#become folders.
#
# === Examples
#Consider the following
# model University has many Faculty(s)
# model Faculty has many Department(s)
# model Department has many StudentImage(s)
#Assume that each of these models has a +name+ field which is the desired field to use for storaging the
#StudentImages.
#If the Filesystemable module is properly applied all of these models, then saving a StudentImage model
#to Department would result in a storage location of:
#<root_path>/UniversityOfVicotria/Engineering/Mechanical/studentPhoto0001.jpg
#Where each of the names given would be extracted from the instance of the respective object
#
#The file structure used to store the files does not have to reflect the model relationships exactly.
#For example, if we didn't want to use the department level of the hierarchy, it can be removed
#(see has_filesystemable_descendents) so the resultant storage location from the previous example
#would be <root_path>/UniversityOfVicotria/Engineering/studentPhoto0001.jpg
#
#Custom container folders can also be injected into the file structure. For example, say we wanted
#the images stored in a folder called "StudentPhotos". Filesystemable allows this as well (see
# is_filesystemable setup). The resultant storage location from the previous example would be
#<root_path>/UniversityOfVicotria/Engineering/Mechanical/StudentPhotos/studentPhoto0001.jpg
#
# === How to use the Filesystemable module?
# 1. Determine the root and leaf models of your hierarchy
# 2. Starting from the leaf models, trace your way up the hierarchy, recording the models as you go
#along. These are your intermediary models
# 3. For the root model classes, add the line is_filesystemable :root, [<your children associations>], <options>
# 4. For the leaf model classes, add the line is_filesystemable <your parent association>, :leaf, cw_mount_column => <carrierave_column_name>, <options>
# 5. Decide if you want an intermediary model to be represented in the file structure. If in file structure, add the line
# is_filesystemable <parent associations>, [<children associations>], <options>. If not in file structure, add
# the line has_filesystemable_descendents <parent associations>, [<children associations>]
# 6. Add the database column, "fs_path", to all of your "is_filesystemable" models
# 7. In your carrierwave uploaders (responsible for the files) make sure store_dir looks like
# def store_dir
#  return model.filesystem_dir
# end
# in addition to this:
# after :store, :update_model_fs_path
# def update_model_fs_path(file)
#   model.update_fs_path
# end
#note that (file) is required
# 8. Add a display_name method, defining the name to be used when storaging file or folder, to all the
# is_filesystemable models.
#
# === Options (is_filesystemable)
# [:container_name => String ]                Files or Folders of the current object will be stored under the container_name
# [:container_name_method => String ]   Same as container_name, except the string must be an executable method which returns the container string
# [:name_method => String ]                   A string containing the method that will be used to determine the name of the filesystem object
# [:cw_mount_column => Symbol ]         A symbol representing the carrierwave mounted column (only used with leaves)
#
# === Parameters
# [parent (Symbol) ]                                The belongs_to association of the model which represents the parent in the hierarchy. If root
#                                                           value should be :root.
# [children (Array or Symbol)]                  An array of has_many associations name (as symbols) which represent the children in the
#                                                          hierarchy. If leaf, value should be :leaf.
#
# === Setting the Root
# Currently a Preferernce table is required, which has one row, with the column mlp_library_local_root. Set this column to your root
# directory for things to work properly.
# === Discussion
#A system such as this is much less efficient than a flat file storage system. The benefit is that if the application breaks or
#no longer becomes maintained, the structure of the data still exists in the filesystem. In its current form, it is designed
#to be faster on read queries, but slow on updates. Specifically, when an update occurs at a high level in the hierarchy
#all of it's descendents need to have their fs_paths updated.
#
#It should be noted that the file structure that is created is sparse. A filesystemable node will only exist in the file structure if
#it directly or indirectly stores a file (see the clean function for how this works).
#
# === TODO
# -make into gem
# -remove reliance on Preference table
# -integrate better with Carrierwave
# -add generator (if gem)
##
module Filesystemable
  module Helpers
    #This will move the object from the path specified in fs_path to filesystem_path, and update fs_path if successfull
    #If the file/folder already exists at filesystem_path, then the fs_path will get updated
    def move_filesystemable_object
      move_success = true
      this_fs_path = fs_path
      this_filesystem_path = filesystem_path(true)
      this_filesystem_dir = filesystem_dir(true)
      if File.exists?(this_fs_path) && !File.exists?(this_filesystem_path)
        FileUtils.mkpath(this_filesystem_dir) unless Dir.exists?(this_filesystem_dir)
        ret = FileUtils.mv(this_fs_path.chomp('/'),this_filesystem_path.chomp('/')) #chomp ensures no trailing '/' which may cause different behaviour in move
        move_success = false if ret !=0 #0 is a success from mv
        if move_success
          clean(File.dirname(this_fs_path))
          update_fs_path
        end
      elsif this_fs_path!=this_filesystem_path
        #think of this as a virtual move, as some filesystemable objects may not have a file or folder associated with it
        #because the structure is sparse (an object will only exist in the filesystem if it has file leaves)
        update_fs_path(true)
      end
      return move_success
    end

    def sanitize(name)
      name = name.gsub("\\", "/") # work-around for IE
      name = File.basename(name)
      name = name.gsub(/[^a-zA-Z0-9\.\-\+_]/,"_")
      name = "_#{name}" if name =~ /\A\.+\z/
      name = "unnamed" if name.size == 0
      return name.mb_chars.to_s
    end


    #deletes dir if it is empty. If it's parents are empty after clean, clean the parents as well
    def clean(dir)
      #method could be potentially dangerous, so restrict clean to only folders in library
      if (dir.nil? || !Regexp.new(Preference.first.mlp_library_local_root).match(dir) || dir==Preference.first.mlp_library_local_root || dir.split(File::SEPARATOR).count<2 || !Dir.exists?(dir) )
        return nil
      end
      parent = dir.truncate(-1,separator: File::SEPARATOR, omission: "")

      #we consider a directory deleteable if it is empty, or the only files in it are hidden_files (start with '.') or of type .mlp, .log
      if (Dir.entries(dir).reject{|i|i=~/\.(mlp|log)$/ || i=~/^\./}.empty?)
        #While we could use FileUtils.rm_rf to remove the folder and all its contents at this point
        #I choose to delete all the files in the directory (the files that are okay to delete according to the
        #above if statement), then remove the directory. This way, I don't accidentally delete a subdirectory,
        #even though if we get to this point we should never have a subdirectory... just being cautious.
        removeable_entries = Dir.entries(dir) - [".", ".."]
        removeable_entries.each do |f|
          FileUtils.rm_f(File.join(dir,f))
        end
        FileUtils.rmdir(dir)
        clean(parent)
      else
        return Dir.entries(dir) - %w{ . .. .DS_Store}
      end
    end

    #This method is currently not in use. It is not generic and specific to MEAT
    #It may be used in replacement of update_fs_path_of_descendents, taking
    #a different approach on updating fs_paths. Instead of many fast updates
    #This will do a few slower updates (updating all rows of each leaf table).
    #Some stuff has changed around, and I suspect that this would need to be
    #applied to all descendent tables to work properly (rather than just the leaves)
    def update_fs_path_of_subtables(old_path,new_path)
      update_tables = ["capture_images","images","metadata_files","field_notes"]
      update_tables.each do |table|
        connection.execute("UPDATE #{table} set fs_path = replace(fs_path,'#{old_path}', '#{new_path}')")
      end
    end

    #Updates the fs_path column of all of the descendents.
    def update_fs_path_of_descendents
      return if filesystem_children.blank?
      filesystem_children.each do |child_association|
        association = send(child_association)
        if association.respond_to?(:each)
          #handles has_many relationships
          association.each { |ca| ca.update_fs_path if ca.respond_to?(:fs_path); ca.update_fs_path_of_descendents }
        elsif association.present?
          #handles has_one relationships
          association.update_fs_path if association.respond_to?(:fs_path)
          association.update_fs_path_of_descendents
        end
      end
    end

    #Updates the fs_path. Using force skips the check to see if the
    #dynamic filesystem_path differs from the current fs_path. Only
    #do this if you already know they are different
    def update_fs_path(force=false)
      if self.fs_path != self.filesystem_path(true) || force
        update_column(:fs_path, self.filesystem_path(true))
      end
    end
  end

  module IsFilesystemable

    def is_filesystemable(parent, children, opts={})
      #normally we would just check to see if the column_names include
      #fs_path, however, when loading the schema in a new system
      #column_names causes an error because the table doesn't exist yet.
      #Therefore, we check to see if the table exists in the db before.
      if (table_exists? && !column_names.include?("fs_path"))
        raise StandardError, "filesystemable models must have an fs_path column"
      end
      include Helpers
      opts.reverse_merge!({container_folder: "", container_folder_method: nil, name_method: "sanitize(display_name)", cw_mount_column: nil})

      class_eval %Q(
          def fs_dir
            fs_path.split(File::SEPARATOR).slice(0..-2).join(File::SEPARATOR)
          end
          #the filesystem path of the current object. If called with default parameters it will
          #simply return fs_path, rather than querying all of its parents.
          def filesystem_path(dynamic=false)
            !dynamic && fs_path.present?  ?  fs_path : File.join(filesystem_dir(true),filesystem_name)
          end
          #the filesystem name of the object
          def filesystem_name
           #{opts[:name_method].to_s}
          end
          #the directory to store the filesystem object
          def filesystem_dir(dynamic=false)
            !dynamic && fs_path.present? ?  fs_dir : File.join(filesystem_parent.filesystem_path(true),#{opts[:container_folder_method] or '"'+opts[:container_folder]+'"'})
          end
          def filesystem_parent
            #{parent.to_s}
          end
          def filesystem_children
            #{children}
          end

        )
      if parent==:root
        #if parent, overwrite the filesystem_dir method to use the library root as the default parent directory.
        class_eval %Q(
          def filesystem_dir(dynamic=false)
            !dynamic && fs_path.present? ?  fs_dir : File.join(Preference.first.mlp_library_local_root , #{opts[:container_folder_method] or '"'+opts[:container_folder]+'"'})
          end
          def filesystem_parent
            nil
          end
          )
      elsif children==:leaf
        #params check
        #note that carrierwave will naturally delete the file in its callback
        #but in order to clean the parent directory, we first need to remove
        #the file ourselves
        after_destroy :clean_up
        class_eval %Q(
          def clean_up
            remove_#{opts[:cw_mount_column].to_s}!
            clean(filesystem_dir(true))
          end
          def filesystem_children
            nil
          end
          )
      end

      after_create :update_fs_path
      around_update do |filesystemable, block|
        ActiveRecord::Base.transaction do
          block.call
          #The folder or file gets moved
          if filesystemable.fs_path != filesystemable.filesystem_path(true)
            unless filesystemable.move_filesystemable_object
              raise ActiveRecord::Rollback
            end
            update_fs_path_of_descendents
          end
        end
      end

    end

  end

  module HasFilesystemableDescendents

    def has_filesystemable_descendents(parent,children)
      include Helpers
      delegate :filesystem_path, to: parent
      class_eval %Q(
        #If an intermediary model is not filesystemable, but gets moved another filesystemable parent
        #we need to move its nearest descendents as well.
        def move_nearest_fs_descendants(force=false)
          if #{parent.to_s}_id_changed? || ( respond_to?("#{parent.to_s}_type_changed?") && #{parent.to_s}_type_changed?) || force
            #{children}.each do |child_association|
               association = send(child_association)
               if association.respond_to?(:each)
                send(child_association).each{ |c| c.move_filesystemable_object }
               else
                association.move_filesystemable_object
               end
            end
          end
        end
        def filesystem_children
          #{children}
        end
        ##
        #In the case where the parent of has_filesystemable_descendents (hfd) object
        #is another hfd, this will simply delegate the move_nearest_fs_desc method to
        #its children.
        ##
        def move_filesystemable_object
          move_nearest_fs_descendants(true)
        end
        )
      after_update :move_nearest_fs_descendants
    end

  end

end
ActiveRecord::Base.send(:extend,Filesystemable::IsFilesystemable)
ActiveRecord::Base.send(:extend,Filesystemable::HasFilesystemableDescendents)
