module Publisher
  def is_publishable(parent_association, children_associations, publish_proc=nil, unpublish_proc=nil)
    class_eval do
      @publish_proc = publish_proc
      @unpublish_proc = unpublish_proc
      if parent_association==:root
        define_method(:publisher_parent) { :root }
      else
        define_method(:publisher_parent) { self.send(parent_association) }
      end
      if children_associations==:leaf
        define_method(:publisher_children) { [] }
      else
        define_method(:publisher_children) do
          children_associations.map do |child_association|
            if self.send(child_association).respond_to?(:to_a)
              self.send(child_association).to_a
            else
              self.send(child_association)
            end
          end.flatten
        end
      end
      def published?
        self.published==true
      end
      def publish
        blk = self.class.instance_variable_get(:@publish_proc)
        block_result = true
        block_result = instance_eval(&blk) if blk
        if block_result && !self.published?
          self.published=true
          self.save
        elsif self.published?
          true
        else
          false
        end
      end
      def unpublish
        blk = self.class.instance_variable_get(:@unpublish_proc)
        block_result = true
        block_result = instance_eval(&blk) if blk
        if block_result && self.published?
            self.published=false
            self.save
        elsif !self.published?
          true
        else
          false
        end
      end
      def publish_ancestors
        if self.publisher_parent==:root
          true
        else
          self.publisher_parent.publish
          self.publisher_parent.publish_ancestors
        end
      end
      def unpublish_ancestors
        if self.publisher_parent==:root
          true
        elsif !self.publisher_parent.has_published_children?
          self.publisher_parent.unpublish
          self.publisher_parent.unpublish_ancestors
        end
      end
      def has_published_children?
        publisher_children.any?{|child| child.published? }
      end

    end
  end
end
ActiveRecord::Base.send(:extend,Publisher)
