module JstreeBuilderHelper

  def jstree_node_hash(object, options={})
    node_hash = {}
    node_hash[:data] = {title: options.fetch(:title) { object.try(:display_name) } }
    node_hash[:data][:icon] = options[:icon] if options[:icon].present?
    node_hash[:attr] = {id: options.fetch(:div_id) {"#{object.class.name}_#{object.id.to_s}"}, class: options.fetch(:div_class) { "" } }
    node_hash[:metadata] = options.fetch(:metadata) { {id: object.id.to_s, obj_class: object.class.name} }
    node_hash[:children] = options.fetch(:children) { [] }
    node_hash[:state] = options[:state] if options[:state].present?
    node_hash
  end

  def unsorted_nodes(object, unsorted_association)
    unless unsorted_association=="unsorted_hcaptures" || unsorted_association=='unsorted_captures'
      raise ArgumentError, "unsorted_association can only be 'unsorted_captures' or 'unsorted_hcaptures'"
    end
    title = unsorted_association=="unsorted_captures" ? "Unsorted Captures" : "Unsorted Historic Captures"
    unsorted_capture_nodes = object.send(unsorted_association).map{|uc| jstree_node_hash(uc, title: uc.display_name, icon: ActionController::Base.helpers.asset_path('camera_icon_small.png') )}
    unsorted_capture_nodes.sort_by!{|cap_node| Naturally.normalize(cap_node[:data][:title])}
    jstree_node_hash(object, title: title, \
                                     icon: ActionController::Base.helpers.asset_path('camera_icon_small_red.png'), \
                                     div_id: "#{object.class.name}_#{object.id.to_s}_#{title.gsub(" ","")}",\
                                     metadata: {},\
                                     children: unsorted_capture_nodes)
  end

  def get_children(obj, relatives=[])
    case obj.class.name
    when 'Project'
      project_children = obj.stations.order("name")
      project_children_hash = get_stations_nodes(obj, relatives)
      project_children_hash << unsorted_nodes(obj, 'unsorted_captures') if obj.unsorted_captures.present?
      project_children_hash << unsorted_nodes(obj, 'unsorted_hcaptures') if obj.unsorted_hcaptures.present?
      return project_children_hash
    when 'Surveyor'
      surveyor_children = obj.surveys.order("name")
      surveyor_children = surveyor_children.joins(:stations).where('stations.id IN (?)', Station.select(:id).send(session[:filter])).uniq if session[:filter] && session[:filter]!='none'
      surveyor_children_hash = surveyor_children.map do |c|
        if relatives.include?(c)
          jstree_node_hash(c, title: c.name, children: get_children(c, relatives), state: 'open')
        else
          jstree_node_hash(c, title: c.name, state: 'closed')
        end
      end
      return surveyor_children_hash
    when 'Survey'
      survey_children = obj.survey_seasons.order("year")
      survey_children = survey_children.joins(:stations).where('stations.id IN (?)', Station.select(:id).send(session[:filter])).uniq if session[:filter] && session[:filter]!='none'
      survey_children_hash = survey_children.map  do |c|
        if relatives.include?(c)
          jstree_node_hash(c, title: c.year.to_s, children: get_children(c, relatives), state: 'open')
        else
          jstree_node_hash(c, title: c.year.to_s, state: 'closed')
        end
      end
      survey_children_hash << unsorted_nodes(obj, "unsorted_captures") if obj.unsorted_captures.present?
      survey_children_hash << unsorted_nodes(obj, "unsorted_hcaptures") if obj.unsorted_hcaptures.present?
      return survey_children_hash
    when 'SurveySeason'
      season_children = obj.stations.sort_by{|s| Naturally.normalize(s.name) }
      season_children_hash = get_stations_nodes(obj, relatives)
      season_children_hash << unsorted_nodes(obj, "unsorted_captures") if obj.unsorted_captures.present?
      season_children_hash << unsorted_nodes(obj, "unsorted_hcaptures") if obj.unsorted_hcaptures.present?
      return season_children_hash
    when 'Station'
      station_children = []
      historic_visit = obj.historic_visit
      if historic_visit.present? && relatives.include?(historic_visit)
        station_children << jstree_node_hash(historic_visit, title: historic_visit.display_name, children: get_children(historic_visit, relatives), state: 'open')
      elsif historic_visit.present?
        station_children << jstree_node_hash(historic_visit, title: historic_visit.display_name, state: 'closed')
      end
      visits = obj.visits.order("date")
      visits.map do |c|
        if relatives.include?(c)
          station_children << jstree_node_hash(c, title: c.display_name, children: get_children(c, relatives), state: 'open')
        else
          station_children << jstree_node_hash(c, title: c.display_name, state: 'closed')
        end
      end
      station_children <<  unsorted_nodes(obj, 'unsorted_captures') if obj.unsorted_captures.present?
      return station_children
    when 'Visit'
      visit_children = []
      obj.locations.order("location_identity").each do  |c|
        if relatives.include?(c)
          visit_children << jstree_node_hash(c, title: c.display_name, children: get_children(c, relatives), state: 'open')
        else
          visit_children << jstree_node_hash(c, title: c.display_name, state: 'closed')
        end
      end
      visit_children << unsorted_nodes(obj, 'unsorted_captures') if obj.unsorted_captures.present?
      return visit_children
    when 'HistoricVisit'
      historic_visit_children = obj.historic_captures.sort_by{|c| Naturally.normalize(c.display_name)}.map do |c|
        jstree_node_hash(c, title: c.display_name, icon: ActionController::Base.helpers.asset_path('camera_icon_small.png'))
      end
      return historic_visit_children
    when 'Location'
      location_children = obj.captures.sort_by{|c| Naturally.normalize(c.display_name) }.map do |c|
        jstree_node_hash(c, title: c.display_name, icon: ActionController::Base.helpers.asset_path('camera_icon_small.png'))
      end
      return location_children
    end
    return []
  end

  def get_stations_nodes(obj, relatives)
    stations = if session[:filter].present? && session[:filter]!='none'
      Station.send(session[:filter]).where_parent(obj) #obj.stations.send(session[:filter])
    else
      obj.stations
    end
    stations = stations.sort_by{|s| Naturally.normalize(s.name) }
    #we need to choose an icon for each station. To do this, we
    #query for stations of a certain type, then see if the current station
    #is part of that type. An icon folder will either be blue or red. Red
    #means there is some issue. Blue means no issues.
    stations_with_issues = Station.with_issues.where_parent(obj) #obj.stations.with_issues
    grouped_stations = Station.grouped_state
    located_stations = Station.located_state
    repeated_stations = Station.repeated_state
    partially_mastered_stations = Station.partially_mastered_state
    mastered_stations = Station.mastered_state
    station_hashes = stations.map do |c|
      icon_color = ''
      if stations_with_issues.include?(c)
        icon_color = '_red'
      end
      icon_state = ''
      if grouped_stations.include?(c)
        icon_state = '_grouped'
      elsif located_stations.include?(c)
        icon_state = '_located'
      elsif repeated_stations.include?(c)
        icon_state = '_repeated'
      elsif partially_mastered_stations.include?(c)
        icon_state = '_partially_mastered'
      elsif mastered_stations.include?(c)
        icon_state = '_mastered'
      end
      icon = ActionController::Base.helpers.asset_path("station#{icon_color}_folder#{icon_state}.png")
      if relatives.include?(c)
        jstree_node_hash(c, title: c.name, icon: icon,  children: get_children(c, relatives), state: 'open')
      else
        jstree_node_hash(c, title: c.name, icon: icon, state: 'closed')
      end
    end
    station_hashes
  end

  def build_tree
    tree = []
    relatives = []
    #get the object that should be selected
    if session[:current_tree_node_type].present? && session[:current_tree_node_id].present? && selected_obj=session[:current_tree_node_type].classify.constantize.find_by_id(session[:current_tree_node_id])
      relatives = get_relatives(selected_obj)
    end
    surveyor_name = Proc.new do |surveyor|
      if surveyor.last_name.present? && surveyor.given_names.present?
        "#{surveyor.last_name}, #{surveyor.given_names}"
      elsif surveyor.last_name.present?
        surveyor.last_name
      elsif surveyor.affiliation.present?
        surveyor.affiliation
      else
        "Unknown"
      end
    end
    surveyors = Surveyor.order(:last_name)
    surveyors = surveyors.joins(:stations).where('stations.id IN (?)', Station.select(:id).send(session[:filter])).uniq if session[:filter] && session[:filter]!='none'
    surveyors = surveyors.to_a.map do |c|
      if relatives.include?(c)
        jstree_node_hash(c, title: surveyor_name.call(c), children: get_children(c, relatives), state: 'open' )
      else
        jstree_node_hash(c, title: surveyor_name.call(c), state: 'closed' )
      end
    end
    projects = Project.order(:name)
    projects = projects.joins(:stations).where('stations.id IN (?)', Station.select(:id).send(session[:filter])).uniq  if session[:filter] && session[:filter]!='none'
    projects = projects.to_a.map do |c|
      if relatives.include?(c)
        jstree_node_hash(c, title: c.display_name, children: get_children(c, relatives), state: 'open' )
      else
        jstree_node_hash(c, title: c.display_name, state: 'closed')
      end
    end
    tree << {data: "Surveyors", attr: {id: "Surveyors_root"}, children: surveyors }
    tree << {data: "Projects", attr: {id: "Projects_root"}, children: projects}
    return tree
  end

end
