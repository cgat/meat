class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.email=='sunyata@uvic.ca'
        can :manage, :all
    elsif user.admin
        can :manage, :all
        cannot :destroy, Surveyor do |surveyor|
            surveyor.surveys.count>0
        end
        cannot :destroy, Survey do |survey|
            survey.survey_seasons.count>0 || \
            survey.unsorted_captures.count>0 || \
            survey.unsorted_hcaptures.count>0
        end
        cannot :destroy, SurveySeason do |survey_season|
            survey_season.stations.count>0 || \
            survey_season.unsorted_captures.count>0 || \
            survey_season.unsorted_hcaptures.count>0
        end
        cannot :destroy, Project do |project|
            project.stations.count>0 || \
            project.unsorted_captures.count>0 || \
            project.unsorted_hcaptures.count>0
        end
    else
        can :read, :all
        can :download, [CaptureImage,FieldNote,MetadataFile,LocationImage,ScenicImage]
        can :comparisons, [Capture,HistoricCapture]
        can :open_metadata_folder, [Project,Survey,SurveySeason]
    end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
