ImageReggy::AlignmentUploader.class_eval do
  def filename
    derivative = model.derivative
    if derivative.present? && derivative.image_secure_token.present? && original_filename.present?
      original_filename.sub("_#{derivative.image_secure_token}","")
    end
  end
end
