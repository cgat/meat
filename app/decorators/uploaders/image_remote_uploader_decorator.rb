ImageRemoteUploader.class_eval do
  include CarrierWave::Vips
  include VipsUploadMethods

  process :convert_to_8bit
  process :convert => 'jpeg', if: :not_jpeg?
  process :set_content_type_with_custom => [false,"image/jpeg"]
  process :quality => 80
  process :resize_to_limit => [ 2100, 2100 ]

  # Create different versions of your uploaded files:
  version :medium do
    process :convert => :jpeg
    process :quality => 85
    process :set_content_type_with_custom => [false,"image/jpeg"]
    process :resize_to_limit => [ 900, 900 ]
  end
  version :thumb, from_version: :medium do
    process :convert => :jpeg
    process :quality => 85
    process :set_content_type_with_custom => [false,"image/jpeg"]
    process :resize_to_limit => [ 150, 150 ]
  end

end
