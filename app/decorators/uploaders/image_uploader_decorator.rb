
#Installation Notes when using this uploader:
#1. You need to have libvips installed. If not installed, run 'brew install vips'. Currently using 7.32
#2. You need to have ImageMagick installed if you want to be able to create version images for 3FR and psd images. 'brew install imagemagick'. Currently using 6.8.0-10
#3. ImageMagick will delegate to ufraw-batch to read 3FR raw files. You need to install ufraw, 'brew install ufraw', and
#4. You need to have `dcraw` installed. 'brew install dcraw', though this should be installed with 'ufraw'
#setup imagemagick to use ufraw-batch. In the imagemagick installation directory, you should be able to locate the delegate.xml file.
#In this file, make sure the following line is included: <delegate decode="dng:decode" command="&quot;ufraw-batch&quot; --wb=auto --saturation=0.85 --gamma=0.18 --clip=digital --create-id=also --out-type=ppm --output=%u.ppm &quot;%i&quot;"/>
#Note that the ufraw-batch parameters listed in the delegate are tweaked for the 3FR files. May not work well for other raw images, if we start to use another camera.
#
#The current setup also relies on a forked version carrierwave-vips (fixed issue where it failed to work with alphanumeric file extensions)

ImageUploader.class_eval do
  include ::CarrierWave::Backgrounder::Delay
  include CarrierWave::Vips
  include VipsUploadMethods
  after :store, :update_model_fs_path


  #usually the update_fs_path is called in the after_save of filesystemable,
  #but with carrierwave files, the file isn't stored until after the after_save
  #callbacks of activerecord. Hence this after :store method
  def update_model_fs_path(file)
    model.update_fs_path
  end

  def store_dir
    return model.filesystem_dir
  end

  # Create different versions of your uploaded files:
  version :medium do
    def store_dir
      if Rails.env.test?
        "uploads/test/versions"
      else
        "uploads/versions"
      end
    end

    def full_filename (for_file = model.image.file)
      "medium_#{secure_token}.jpeg"
    end

    def url
      if model.image.present? && File.exists?(self.file.path)
        super
      elsif model.image_tmp.present?
        self.processing_url
      else
        self.default_url
      end
    end

    def move_to_cache
      false
    end
    process :convert_to_8bit
    process :convert => :jpeg
    process :quality => 90
    process :set_content_type_with_custom => [false,"image/jpeg"]
    process :resize_to_limit => [ 900 , 900 ]
  end

  version :thumb, from_version: :medium do
    def store_dir
      if Rails.env.test?
        "uploads/test/versions"
      else
        "uploads/versions"
      end
    end

    def full_filename (for_file = model.image.file)
      "thumb_#{secure_token}.jpeg"
    end

    def url
      if model.image.present? && File.exists?(self.file.path)
        super
      elsif model.image_tmp.present?
        self.processing_url
      else
        self.default_url
      end
    end

    def move_to_cache
      false
    end
    process :convert => :jpeg
    process :quality => 90
    process :set_content_type_with_custom => [false,"image/jpeg"]
    process :resize_to_limit => [ 150 , 150 ]
  end
end
