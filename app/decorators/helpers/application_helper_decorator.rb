require 'socket'

ApplicationHelper.class_eval do

  def admin_app?
    true
  end

  def comparison_path(capture_id, historic_capture_id)
    return comparisons_path+"?capture_id=#{capture_id}&historic_capture_id=#{historic_capture_id}"
  end

  #this link_to creates a <a></a> link tag for local files in the file system.
  def link_to_local_file(name, file_path)
    href_attr = "href=\"file:\/\/\/#{file_path}\""
    "<a #{href_attr}>#{name}</a>".html_safe
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    render_prepend = ""
    obj_class = new_object.class.to_s
    case obj_class
    when 'Location'
      new_capture = new_object.captures.build
      new_capture_image = new_capture.capture_images.build
      render_prepend = "locations/"
    when 'Capture'
      new_capture_image = new_object.capture_images.build
      #setup_historic_captures_from_station(f.object.visit.station_id)
      render_prepend = "captures/"
    when 'HistoricCapture'
      new_capture_image = new_object.capture_images.build
      #setup_historic_captures_from_station(f.object.visit.station_id)
      render_prepend = "historic_captures/"
    when 'CaptureImage'
      render_prepend = "capture_images/"
    end
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(render_prepend + association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end


  #There are few features that allow the user to open and modify files on the server. These don't make
  #any sense if the client is remote. This helper determines if the client is interacting with MEAT on the
  #server directly.
  def client_on_server?
    r_ip=request.remote_ip
    r_ip=='127.0.0.1' || r_ip==local_ip rescue false
  end

  private
    #Gets the local ip address of the server without invoking the shell. See link below for details
    #http://coderrr.wordpress.com/2008/05/28/get-your-local-ip-address/
    def local_ip
      orig, Socket.do_not_reverse_lookup = Socket.do_not_reverse_lookup, true  # turn off reverse DNS resolution temporarily

      UDPSocket.open do |s|
        s.connect '64.233.187.99', 1
        s.addr.last
      end
    ensure
      Socket.do_not_reverse_lookup = orig
    end

end

class Menu
    attr_accessor :title, :type, :items, :delete
    def initialize(title, type, items=[])
      @title = title
      @type = type
      @items = items
    end

    def add(item)
      if block_given?
        @items << item
      else
        @items<<item
      end
    end

    def add_delete(obj_url, alert_msg)
      self.delete = <<-html
      <div class='delete-link' style="float: right;">
        <ul class='nav'>
          <li>
            <a id='object_delete' href="#{obj_url}" data-method="delete" rel="no-follow" data-confirm="#{alert_msg}"><span class='icomoon-remove' title='Delete'></span></a>
          </li>
        </ul>
      </div>
      html
    end

    def to_html
      case @type
      when :boostrap
        brand = if @title.present?
         <<-brand
         <a class="brand" href="#">#{@title}</a>
         brand
        else
          ""
        end

        menu_html  = <<-html
        <div class="navbar">
        <div class="navbar-inner">
        <div class='container' style='width: auto;'>
        #{brand}
        <img id="data-pane-loading" src="#{ActionController::Base.helpers.asset_path('processing_throbber2.gif')}" style="display: none; float: left;">
        <ul class='nav' role='navigation'>
        html
        @items.each do |i|
          if i.is_a? Menu
            i.type = :bootstrap_sub
            menu_html << i.to_html
          elsif i.is_a? String
            menu_html << i.html_safe
          else
            unless i.has_key?(:display) && !i[:display]
              menu_html << "<li class='#{i[:class]}'>#{i[:link]}</li>"
            end
          end
        end
        menu_html << <<-html
        </ul>
        #{delete}
        </div>
        </div>
        </div>
        html
        menu_html.html_safe
      when :bootstrap_sub
        menu_html = <<-html
        <li class='dropdown'>
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">#{@title}<b class="caret"></b></a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        html
        @items.each do |i|
          unless i.has_key?(:display) && !i[:display]
            menu_html << "<li class='#{i[:class]}'>#{i[:link]}</li>"
          end
        end
        menu_html << "</ul></li>"
      end
    end
  end
