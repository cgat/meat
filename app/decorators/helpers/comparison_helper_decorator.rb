ComparisonHelper.class_eval do
  #in the comparison.html.erb page, this methods helps to control
  #which images get displayed and which ones don't.
  def style_display(display_img_id, cur_img_id, index)
    #set style to display or not to display
    style= "display: none;" #assume no display
    if display_img_id.present? && cur_img.id==display_img_id
      style =""
    elsif display_img_id.blank? && index==1
      style==""
    end
    style
  end

  def print_aligned(base_capture, comparison_capture)
    base_capture.capture_images.each{|ci| comparison_capture.capture_images.each{|ci2| yield(ci,ci2).html_safe if ci.aligned?(ci2) } }
  end
end
