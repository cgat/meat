CapturesController.class_eval do
  load_and_authorize_resource

  def new
    #setup_historic_captures_from_station isn't set for survey/project/seasons because
    #a capture cannot reference a historic capture when sorted under these objects
    if params.has_key?(:visit_id) && visit=Visit.find_by_id(params[:visit_id])
      @capture.capture_owner = visit
      setup_historic_captures_from_station(visit.station.id)
    elsif params.has_key?(:location_id) && location=Location.find_by_id(params[:location_id])
      @capture.capture_owner = location
      setup_historic_captures_from_station(location.visit.station.id)
    elsif params.has_key?(:station_id) && station=Station.find_by_id(params[:station_id])
      @capture.capture_owner = station
      setup_historic_captures_from_station(station.id)
    elsif params.has_key?(:survey_id) && survey=Survey.find_by_id(params[:survey_id])
      @capture.capture_owner = survey
    elsif params.has_key?(:survey_season_id) && survey_season=SurveySeason.find_by_id(params[:survey_season_id])
      @capture.capture_owner = survey_season
    elsif params.has_key?(:project_id) && project=Project.find_by_id(params[:project_id])
      @capture.capture_owner = project
    end
    @capture.capture_images.build
  end

  def create
    if @capture.save
      flash[:success] = "Capture successfully created!"
      redirect_to @capture
    else
      if params.has_key?(:visit_id) && visit=Visit.find_by_id(params[:visit_id])
        setup_historic_captures_from_station(visit.station.id)
      elsif params.has_key?(:location_id) && location=Location.find_by_id(params[:location_id])
        setup_historic_captures_from_station(location.visit.station.id)
      elsif params.has_key?(:station_id) && station=Station.find_by_id(params[:station_id])
        setup_historic_captures_from_station(station.id)
      end
      render 'new'
    end
  end

  def edit
    setup_historic_captures_from_station(@capture.station.id) if @capture.station.present?
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def destroy
    parent = @capture.capture_owner
    @capture.destroy
    flash[:success] = "The capture and its children objects have been deleted successfully!"
    redirect_to parent
  end

  def update
    if @capture.update_attributes(params[:capture])
      flash[:success] = "Capture Information Updated"
      redirect_to @capture
    else
      setup_historic_captures_from_station(@capture.station.id) if @capture.station.present?
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end

  def move
    if params.has_key?(:parent_id) && params.has_key?(:parent_type) && params.has_key?(:id)
      @capture = Capture.find(params[:id])
      @capture.capture_owner_id = params[:parent_id]
      @capture.capture_owner_type = params[:parent_type]
      if @capture.save
        flash.now[:success] = "Capture successfully moved!"
        head :ok
      else
        flash[:error] = "Capture failed to be move :("
        redirect_to @capture
      end
    end
  end

  #in a merge one capture will be merged into another, with one of the two being deleted
  #the merge_out node is the one that will be deleted, while the merge_in node is the
  #one that will receive all the data from the other node
  def merge
    if params.has_key?(:merge_out_id) && params.has_key?(:merge_in_id)
      @capture = Capture.find(params[:merge_in_id]) #don't change the name of this variable, as it will be used to render the show page
      merge_out = Capture.find(params[:merge_out_id])
      if params[:merge_out_id]!=params[:merge_in_id] && @capture.merge(merge_out)
        @capture.reload
        flash.now[:success] = "Captures successfully merged!"
        js_to_render=<<-js
          $("#treeViewDiv").jstree("delete_node","#Capture_#{params[:merge_out_id]}");
          $.pjax({url: "/captures/#{@capture.id}", container: '[data-pjax-container]'});
        js
        render js: js_to_render
      else
        flash.now[:error] = "These two captures are not mergeable."
        head :ok
      end
    end
  end

  def overlays
    capture = Capture.find(params[:id])
    if capture.has_aligned_comparisons?
      respond_to do |format|
        format.html { render template: 'shared/overlays', locals: {base_comparison: capture, comparisons: capture.comparisons.with_capture_images } }
        format.js { render template: 'shared/overlays' }
      end
    else
      @capture = capture
      render 'show'
    end
  end

end
