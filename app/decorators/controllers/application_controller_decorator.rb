ApplicationController.class_eval do

  after_filter :flash_to_headers



  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "Access denied!"
    redirect_to root_url
  end

  def update_historic_captures
    setup_historic_captures_from_station(params[:station_id])
    render 'shared/update_historic_captures'
  end

  def setup_historic_captures_from_station(station_id)
    if (station = Station.find_by_id(station_id)) && !station.historic_visit.nil? && !station.historic_visit.historic_captures.nil?
      @historic_captures_from_station = station.historic_visit.historic_captures.map{|h| [h.display_name, h.id]}
      @historic_captures_from_station
    else
      @historic_captures_from_station = []
    end
  end

  def setup_captures_from_station(station_id)
    if (station = Station.find_by_id(station_id)) && (captures=station.captures) && captures.present?
      @captures_from_station= captures.map{|c|[c.display_name,c.id]}
      @captures_from_station
    else
      @captures_from_station=[]
    end
  end

  def default_nav_state
    "tree"
  end

  def treedata
    session[:nav_state]='tree'
    if params.has_key?(:type) && params.has_key?(:id)
      klass = params[:type].classify.constantize rescue nil
      @opening_obj = klass.find_by_id(params[:id]) if klass
    end
    render 'shared/jstreedata'
  end

  def filters
    session[:nav_state] = "tree"
    session[:filter] = params[:filter_name]
    if current_node.present?
      redirect_to current_node
    else
      redirect_to root_url
    end
  end

  #http://stackoverflow.com/questions/366311/how-do-you-handle-rails-flash-with-ajax-requests
  def flash_to_headers
    return unless request.xhr?
    response.headers['X-Message'] = flash_message
    response.headers["X-Message-Type"] = flash_type.to_s
    #flash.discard # don't want the flash to appear when you reload page
  end

  private

  def flash_message
    [:error, :warning, :notice, :success].each do |type|
      return flash[type] unless flash[type].blank?
    end
    return ""
  end

  def flash_type
    [:error, :warning, :notice, :success].each do |type|
      return type unless flash[type].blank?
    end
    return ""
  end

  #if the time parameter is set to blank in a form
  #a hidden day, month, and year still get set,
  #which ends up submitting 00:00 to the db
  #This method sees if the time is blank and configs
  #params accordingly
  #params: this should be the form params. in the controller
  #you would pass in params[:<form_name>]
  #It returns the params is proper form
  def filter_time_params(params, time_attr)
    #always set year to 2000, regardless (so that times can be compared equally)
    params["#{time_attr.to_s}(1i)"] = "2000"
    return params unless params.has_key?("#{time_attr.to_s}(5i)") && params.has_key?("#{time_attr.to_s}(4i)")
    if params["#{time_attr.to_s}(4i)"].blank? || params["#{time_attr.to_s}(5i)"].blank?
      params.delete("#{time_attr.to_s}(5i)")
      params.delete("#{time_attr.to_s}(4i)")
      params.delete("#{time_attr.to_s}(3i)")
      params.delete("#{time_attr.to_s}(2i)")
      params.delete("#{time_attr.to_s}(1i)")
      params[time_attr]=nil
    end
    return params
  end

end
