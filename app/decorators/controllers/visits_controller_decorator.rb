VisitsController.class_eval do
  load_and_authorize_resource
  skip_load_resource only: [:create,:update]

  def new
    if params.has_key?(:station_id) && Station.find_by_id(params[:station_id])
      @visit.station = Station.find_by_id(params[:station_id])
    end
    setup_historic_captures_from_station(@visit.station.try(:id))
    location = @visit.locations.build(location_identity: "1")
    capture = location.captures.build
    capture_image = capture.capture_images.build
    #keyword and participant are used in hidden forms that get opend when clicking the "Create ___" link in the form
    @keyword = Keyword.new
    @participant = Participant.new
  end


  def create
    params[:visit]=filter_time_params(params[:visit],:start_time)
    params[:visit]=filter_time_params(params[:visit],:finish_time)
    @visit = Visit.new(params[:visit])
    if @visit.save
      flash[:success] = "Successfully create visit."
      redirect_to @visit
    else
      @keyword = Keyword.new
      @participant = Participant.new
      setup_historic_captures_from_station(@visit.station.id) if @visit.station.present?
      render 'new'
    end
  end

  def update
    params[:visit]=filter_time_params(params[:visit],:start_time)
    params[:visit]=filter_time_params(params[:visit],:finish_time)
    @visit = Visit.find(params[:id])
    if @visit.update_attributes(params[:visit])
      flash[:success] = "Visit Information Updated"
      redirect_to @visit
    else
      setup_historic_captures_from_station(@visit.station.id) if @visit.station.present?
      @keyword = Keyword.new
      @participant = Participant.new
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def edit
    @keyword = Keyword.new
    @participant = Participant.new
    setup_historic_captures_from_station(@visit.station.try(:id))
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def destroy
    parent = @visit.station
    @visit.destroy
    flash[:success] = "The visit and its locations, captures, and capture images have been deleted successfully!"
    redirect_to parent
  end


end
