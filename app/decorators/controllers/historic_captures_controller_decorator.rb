HistoricCapturesController.class_eval do
  load_and_authorize_resource

  def edit
    setup_captures_from_station(@historic_capture.station.id) if @historic_capture.station.present?
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def destroy
    parent = @historic_capture.capture_owner
    @historic_capture.destroy
    flash[:success] = "The historic capture and its children objects have been deleted successfully!"
    redirect_to parent
  end

  def new
    if params.has_key?(:historic_visit_id) && historic_visit=HistoricVisit.find_by_id(params[:historic_visit_id])
      @historic_capture.capture_owner = historic_visit
      setup_captures_from_station(historic_visit.station.id)
    elsif params.has_key?(:survey_id) && survey=Survey.find_by_id(params[:survey_id])
      @historic_capture.capture_owner = survey
    elsif params.has_key?(:survey_season_id) && survey_season=SurveySeason.find_by_id(params[:survey_season_id])
      @historic_capture.capture_owner = survey_season
    elsif params.has_key?(:project_id) && project=Project.find_by_id(params[:project_id])
      @historic_capture.capture_owner = project
    end
    @historic_capture.capture_images.build
  end

  def create
    if @historic_capture.save
      flash[:success] =  "Historic Capture successfully created!"
      redirect_to @historic_capture
    else
      if params.has_key?(:historic_visit_id) && historic_visit=HistoricVisit.find_by_id(params[:historic_visit_id])
        setup_captures_from_station(historic_visit.station.id)
      end
      render 'new'
    end
  end

  def update
    if @historic_capture.update_attributes(params[:historic_capture])
      flash[:success] = "Historic Capture Information Updated"
      redirect_to @historic_capture
    else
      setup_captures_from_station(@historic_capture.station.id) if @historic_capture.station.present?
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end

  def move
    if params.has_key?(:parent_id) && params.has_key?(:parent_type) && params.has_key?(:id)
      @hcapture = HistoricCapture.find(params[:id])
      @hcapture.capture_owner_id = params[:parent_id]
      @hcapture.capture_owner_type = params[:parent_type]
      if @hcapture.save
        flash.now[:success] = "HistoricCapture successfully moved!"
        head :ok
      else
        flash[:error] = "HistoricCapture failed to be move :("
        redirect_to @hcapture
      end
    end
  end

  #in a merge one capture will be merged into another, with one of the two being deleted
  #the merge_out node is the one that will be deleted, while the merge_in node is the
  #one that will receive all the data from the other node
  def merge
    if params.has_key?(:merge_out_id) && params.has_key?(:merge_in_id)
      @historic_capture = HistoricCapture.find(params[:merge_in_id]) #don't change the name of this variable, as it will be used to render the show page
      merge_out = HistoricCapture.find(params[:merge_out_id])
      if params[:merge_out_id]!=params[:merge_in_id] && @historic_capture.merge(merge_out)
        @historic_capture.reload
        flash.now[:success] = "Historic Captures successfully merged!"
        js_to_render=<<-js
          $("#treeViewDiv").jstree("delete_node","#HistoricCapture_#{params[:merge_out_id]}");
          $.pjax({url: "#{historic_capture_path(@historic_capture.id)}", container: '[data-pjax-container]'});
        js
        render js: js_to_render
      else
        flash.now[:error] = "These two historic captures are not mergeable."
        head :ok
      end
    end
  end

  def overlays
    @historic_capture = HistoricCapture.find(params[:id])
    if @historic_capture.has_viewable_comparisons?
      respond_to do |format|
        format.html { render template: 'shared/overlays', locals: {base_comparison: @historic_capture, comparisons: @historic_capture.comparisons.with_capture_images } } #
        format.js { render template: 'shared/overlays' }
      end
    else
      render 'show'
    end
  end


end
