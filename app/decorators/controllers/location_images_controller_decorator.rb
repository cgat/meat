LocationImagesController.class_eval do
  load_and_authorize_resource

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show' }
    end
  end

  def destroy
    parent = @location_image.image_owner
    @location_image.destroy
    flash[:success] = "The location image has been deleted successfully."
    redirect_to parent
  end

  def download
    head(:not_found) and return if (@location_image = LocationImage.find_by_id(params[:id])).nil? || !@location_image.file_present?
    head(:ok)
    send_file @location_image.image.file.path, x_sendfile: true, disposition: 'attachment', filename: @location_image.display_name
  end

  def open
    head(:not_found) and return if (@location_image = LocationImage.find_by_id(params[:id])).nil? || !@location_image.file_present?
    head(:ok)
    `open "#{@location_image.image.file.path}"`
  end

  def move
    if params.has_key?(:parent_id) && params.has_key?(:parent_type) && params.has_key?(:id)
      @location_image = LocationImage.find(params[:id])
      @location_image.image_owner_id = params[:parent_id]
      @location_image.image_owner_type = params[:parent_type]
      if (params[:parent_type]=='Location' || params[:parent_type]=='Visit' ||  params[:parent_type]=='Station') && @location_image.save
        flash.now[:success] = "Location Image successfully moved!"
        if params[:new_parent_descendent_of_showing_page]=="true"
          render js: "$('#location_image_row_#{@location_image.id} .drag_column').remove();"
        else
          render 'shared/remove_loc_img_from_page'
        end
      else
        flash.now[:error] = "Location Image failed to be move :("
        head :bad_request
      end
    end
  end
end
