LocationsController.class_eval do
  load_and_authorize_resource

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def edit
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def update
    if @location.update_attributes(params[:location])
      flash[:success] = "Successfully updated location"
      redirect_to @location
    else
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end

  def destroy
    parent = @location.visit
    @location.destroy
    flash[:success] = "The location and its children objects have been deleted successfully!"
    redirect_to parent
  end

end
