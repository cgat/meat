SurveyorsController.class_eval do
  load_and_authorize_resource

  def new
  end

  def edit
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def update
    if @surveyor.update_attributes(params[:surveyor])
      flash[:success] = "Surveyor successfully updated"
      redirect_to @surveyor
    else
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end

  def create
    if @surveyor.save
      flash[:success] = "Surveyor successfully created"
      redirect_to @surveyor
    else
      render 'new'
    end
  end

  def destroy
    @surveyor.destroy
    flash[:success] = "Surveyor successfully deleted"
    redirect_to '/'
  end

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

end
