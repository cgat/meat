SurveysController.class_eval do
  load_and_authorize_resource

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def edit
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def destroy
    parent = @survey.surveyor
    @survey.destroy
    flash[:success] = "The survey and its children objects have been deleted successfully!"
    redirect_to parent
  end

  def update
    if @survey.update_attributes(params[:survey])
      flash[:success] = "Survey Information Updated"
      redirect_to @survey
    else
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end

  def new
    if params.has_key?(:surveyor_id) && Surveyor.find_by_id(params[:surveyor_id])
      @survey.surveyor = Surveyor.find_by_id(params[:surveyor_id])
    end
  end

  def create
    if @survey.save
      flash[:success] = "Successfully created survey."
      redirect_to @survey
    else
      render 'new'
    end
  end

  def open_metadata_folder
    head(:not_found) and return if @survey.nil?
    @survey.open_metadata_folder
    head(:ok)
  end



end
