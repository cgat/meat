HistoricVisitsController.class_eval do
  load_and_authorize_resource

  def new
    if params.has_key?(:station_id) && Station.find_by_id(params[:station_id])
      @historic_visit.station = Station.find_by_id(params[:station_id])
    end
    setup_captures_from_station(@historic_visit.station.id) if @historic_visit.station.present?
    historic_capture = @historic_visit.historic_captures.build
    historic_capture_image = historic_capture.capture_images.build
  end

  def create
    if @historic_visit.save
      flash[:success] = "Successfully created visit."
      redirect_to @historic_visit
    else
      setup_captures_from_station(@historic_visit.station.id) if @historic_visit.station.present?
      render 'new'
    end
  end

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def edit
    setup_captures_from_station(@historic_visit.station.id) if @historic_visit.station.present?
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def destroy
    parent = @historic_visit.station
    @historic_visit.destroy
    flash[:success] = "The historic visit and its children objects have been deleted successfully!"
    redirect_to parent
  end

  def update
    if @historic_visit.update_attributes(params[:historic_visit])
      flash[:success] = "Historic Visit Information Updated"
      redirect_to @historic_visit
    else
      setup_captures_from_station(@historic_visit.station.id) if @historic_visit.station.present?
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end


end
