CaptureImagesController.class_eval do
  load_and_authorize_resource



  def destroy
    parent = @capture_image.captureable
    @capture_image.destroy
    flash[:success] = "The capture image has been deleted successfully."
    redirect_to parent
  end

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def download
    head(:not_found) and return if (@capture_image = CaptureImage.find_by_id(params[:id])).nil? || !@capture_image.file_present?
    head(:ok)
    send_file @capture_image.image.file.path, x_sendfile: true, disposition: 'attachment', filename: @capture_image.display_name
  end

  def open
    head(:not_found) and return if (@capture_image = CaptureImage.find_by_id(params[:id])).nil? || !@capture_image.file_present?
    head(:ok)
    `open "#{@capture_image.image.file.path}"`
  end

  def move
    if params.has_key?(:parent_id) && params.has_key?(:parent_type) && params.has_key?(:id)
      @capture_image = CaptureImage.find(params[:id])
      @capture_image.captureable_id = params[:parent_id]
      @capture_image.captureable_type = params[:parent_type]
      if (params[:parent_type]=='Capture' || params[:parent_type]=='HistoricCapture') && @capture_image.save
        flash.now[:success] = "Capture Image successfully moved!"
        render 'shared/remove_cap_img_from_page'
      else
        flash[:error] = "Capture Image failed to be move :("
        head :bad_request
      end
    end
  end

end
