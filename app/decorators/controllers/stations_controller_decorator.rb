StationsController.class_eval do
  load_and_authorize_resource

  def edit
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def destroy
    parent = @station.station_owner
    @station.destroy
    flash[:success] = "The station and its children objects have been deleted successfully!"
    redirect_to parent
  end

  def update
    if @station.update_attributes(params[:station])
      flash[:success] = "Station Information Updated"
      redirect_to @station
    else
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end


  def new
    if params.has_key?(:survey_season_id) && ss=SurveySeason.find_by_id(params[:survey_season_id])
      @station.station_owner = ss
    elsif params.has_key?(:project_id) && p=Project.find_by_id(params[:project_id])
      @station.station_owner = p
    end
  end

  def create
    if @station.save
      flash[:success] = "Successfully created station."
      redirect_to @station
    else
      render 'new'
    end
  end

  def open_old
    head(:not_found) and return if @station.nil? || !Dir.exists?(@station.legacy_path)
    head(:ok)
    `open "#{@station.legacy_path}"`
  end

end
