SurveySeasonsController.class_eval do
  load_and_authorize_resource

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def edit
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def destroy
    parent = @survey_season.survey
    @survey_season.destroy
    flash[:success] = "The survey season and its children objects have been deleted successfully!"
    redirect_to parent
  end

  def update
    if @survey_season.update_attributes(params[:survey_season])
      flash[:success] = "Survey Season Information Updated"
      redirect_to @survey_season
    else
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit'}
      end
    end
  end

  def new
    if params.has_key?(:survey_id) && Survey.find_by_id(params[:survey_id])
      @survey_season.survey = Survey.find_by_id(params[:survey_id])
    end
  end

  def create
    if @survey_season.save
      flash[:success] = "Successfully created survey season."
      redirect_to @survey_season
    else
      render 'new'
    end
  end

  def move
    if params.has_key?(:parent_id) && params.has_key?(:id)
      @survey_season = SurveySeason.find(params[:id])
      @survey_season.survey_id = params[:parent_id]
      if @survey_season.save
        # if we move to an unpublisehd survey, we need to publish
        # it and its parents
        if @survey_season.published && !@survey_season.parent.published
          @survey_season.publish_ancestors
        end
        flash.now[:success] = "Survey Season successfully moved!"
        head :ok
      else
        flash[:error] = "Survey Season failed to be move :("
        redirect_to @survey_season
      end
    end
  end

  def open_metadata_folder
    head(:not_found) and return if @survey_season.nil?
    @survey_season.open_metadata_folder
    head(:ok)
  end


end
