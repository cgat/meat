ProjectsController.class_eval do
  load_and_authorize_resource

  def show
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show' }
    end
  end

  def new
  end

  def create
    if @project.save
      flash[:success] = "Project successfully created"
      redirect_to @project
    else
      render 'new'
    end
  end

  def edit
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit' }
    end
  end

  def update
    if @project.update_attributes(params[:project])
      flash[:success] = "Project successfully updated"
      redirect_to @project
    else
      respond_to do |format|
        format.html { render 'edit' }
        format.js { render 'edit' }
      end
    end
  end

  def destroy
    @project.destroy
    flash[:success] = "Project successfully deleted"
    redirect_to '/'
  end

  def open_metadata_folder
    head(:not_found) and return if @project.nil?
    @project.open_metadata_folder
    head(:ok)
  end

end
