ScenicImage.class_eval do
  is_filesystemable :image_owner, :leaf, container_folder: "Scenics", name_method: "self.image.file.filename", cw_mount_column: :image
end
