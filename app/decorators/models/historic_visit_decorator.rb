HistoricVisit.class_eval do
  is_filesystemable :station, [:historic_captures]
  is_publishable :station, [:historic_captures]
end
