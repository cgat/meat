Survey.class_eval do
  is_filesystemable :surveyor, [:survey_seasons, :unsorted_captures, :unsorted_hcaptures]
  is_publishable :surveyor, [:survey_seasons, :unsorted_captures, :unsorted_hcaptures]
end
