MetadataFile.class_eval do
  is_filesystemable :metadata_owner, :leaf, container_folder: "Metadata", cw_mount_column: :metadata_file
end
