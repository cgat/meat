Location.class_eval do
  has_filesystemable_descendents :visit, [:captures, :location_photos]
  is_publishable :visit, [:captures]
end
