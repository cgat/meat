FieldNote.class_eval do
  is_filesystemable :visit, :leaf, container_folder: "Field_notes", name_method: "self.field_note_file.file.filename", cw_mount_column: :field_note_file
end
