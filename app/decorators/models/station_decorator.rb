Station.class_eval do
  is_filesystemable :station_owner, [:visits, :unsorted_captures, :unsorted_location_photos, :historic_visit, :metadata_files]
  is_publishable :station_owner, [:visits, :unsorted_captures, :historic_visit] #, :unsorted_location_photos
end
