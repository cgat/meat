Visit.class_eval do
  is_filesystemable :station, [:locations, :unsorted_captures, :field_notes, :metadata_files, :unsorted_location_photos]
  is_publishable :station, [:locations, :unsorted_captures]
end
