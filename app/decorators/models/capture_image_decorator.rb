CaptureImage.class_eval do
  is_filesystemable :captureable, :leaf, container_folder_method: "self.image_state.humanize", name_method: "self.image.file.filename", cw_mount_column: :image
  is_publishable :captureable, :leaf
  alias :published? :remote
  alias :published :remote
  alias :published= :remote=

  store_in_background :image
  #process_in_background :image_remote
end
