
Project.class_eval do
  is_filesystemable :root, [:stations, :unsorted_captures, :unsorted_hcaptures], container_folder: "Organized_by_Project"
  is_publishable :root, [:stations, :unsorted_captures, :unsorted_hcaptures]
end
