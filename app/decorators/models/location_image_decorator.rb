LocationImage.class_eval do
  is_filesystemable :image_owner, :leaf, container_folder: "Location_Photos", name_method: "self.image.file.filename", cw_mount_column: :image
end
