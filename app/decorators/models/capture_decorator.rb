
Capture.class_eval do
  has_filesystemable_descendents :capture_owner, [:capture_images]
  delegate :publish, :to => :preview_capture_image, :prefix => true, :allow_nil => true

  publisher_proc = Proc.new do
    preview_cap_img = self.preview_capture_image;
    if preview_cap_img.present? && !preview_cap_img.published?
      #we want to make sure only one image of a cap is published
      #at any time, so unpublish anything that may have been previously
      #published
      self.capture_images.each{|ci| ci.unpublish }
      #now publish the preview capture image
      self.preview_capture_image_publish
    else
      true
    end
  end
  unpublisher_proc = Proc.new do
    self.capture_images.all?{|ci| ci.unpublish }
  end
  is_publishable :capture_owner, [:capture_images], publisher_proc, unpublisher_proc
end
