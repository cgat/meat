SurveySeason.class_eval do
  is_filesystemable :survey, [:stations, :unsorted_captures, :unsorted_hcaptures]
  is_publishable :survey, [:stations, :unsorted_captures, :unsorted_hcaptures]
end
