Surveyor.class_eval do
  is_filesystemable :root,  [:surveys], container_folder: "Organized_by_Surveyor"
  is_publishable :root,  [:surveys]
end
