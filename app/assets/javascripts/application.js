// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jstree
//= require meat_base/application
//= require image_reggy/application
//= require_tree .
//= require_self
//= require bootstrap
//= require jquery.jgrowl
//= require multi-select
//= require Chart
//= require legend


//This is a global variable used to determine
//if the select_node jstree event should function
//as normal. Specifically, the jstree binds
//an event handler that loads a data-pane
//page when selected. We want to stop
//this functionality when we are updating the
//selected node because someone has clicked a
//link the data-pane page. If you change this variable
//you need to make changes to the js_tree.js.erb file
//and select_jstree_node function in this file
var isSelectNodeActive = true;

$(document).ready(function() {
	pjax_functionality();
	$('.dropdown-toggle').dropdown();
	hide_drag_column();
	visit_edit_multiselects();
	toggle_hvisit_info();
});

var pjax_functionality = function(){
	$(document).on('pjax:success', select_jstree_node);
	$(document).on('pjax:success', hide_drag_column);
	$(document).on('pjax:success', function(){
		$('.dropdown-toggle').dropdown();
	});
	$(document).on('pjax:success', toggle_hvisit_info);
	$(document).on('pjax:start', function() {
		$('#data-pane a.brand').hide();
		$('#data-pane #data-pane-loading').show();
	});
}



//**** this is for flash with ajax ****
var fade_flash = function() {
$("#flash_notice").delay(10000).fadeOut("slow");
$("#flash_alert").delay(10000).fadeOut("slow");
$("#flash_error").delay(10000).fadeOut("slow");
$("#flash_success").delay(10000).fadeOut("slow");
};
fade_flash();

var show_ajax_message = function(request) {
    var msg = request.getResponseHeader('X-Message');
    var type = request.getResponseHeader('X-Message-Type');
    if (msg!=null && msg!="" && type!=null && type!="") {
		$.jGrowl(msg,{theme: "flash_"+type });
	}
};
//listen for the ajaxComplete event. When it is done, get the X-message (flash message)
// and X-Message-Type (flash type) from the header
$(document).ajaxComplete(function(event, request) { show_ajax_message(request); });
//**** end *****

//This function toggles certain fields within the
//historic visit /historic capture form. Because this fields aren't
//accessed as much, we don't show them by
//default. If the user clicks the link, the fields
//are exposed and the link disappears.
var toggle_hvisit_info = function() {
	$("a.camera_info_link").on("click", function(event) {
		$(this).next().slideToggle("slow");
		$(this).toggle();
		return event.preventDefault();
	});
	$('a.lac_info_link').on("click", function(event) {
		$(this).next().slideToggle("slow");
		$(this).toggle();
		return event.preventDefault();
	});
	$('div.camera_info_fields').hide();
	$('div.lac_info_fields').hide();
}



//This function will select a node in the jstree
//based off of the current url. It is primarily used
//when a user navigates to a object page using a
//link in the data-pane (such as a breadcrumb)
var select_jstree_node = function() {
	select_id = convertUrlToID(window.location.pathname);
	if ($("#"+select_id).length) {
		$.jstree._reference("#treeViewDiv").deselect_all();
		isSelectNodeActive = false;
		$.jstree._reference("#treeViewDiv").select_node("#"+select_id);
		isSelectNodeActive = true;
	}
}

//The drag_column refers to the column in the
//capture image and locaiton image tables with the
//drag icon (4 arrows pointing in all directions).
//This function hides the column if the user is not
//an administrator (in which case they don't have
//access to this functionality
var hide_drag_column = function(){
	if(window.isAdmin==undefined || !window.isAdmin) {
		$('.drag_column').hide();
	}
}


//converts the given url to an ID representation
//that I use to refer to nodes in the jstree
//Specifically, "/surveyors/3" would convert to
// "Surveyor_3" and "/survey_seasons/4" would
//convert to "SurveySeason_4"
var convertUrlToID = function(url) {
	url_array = url.split("/")
	id = url_array[2];
	model = url_array[1];
	model = model.toTitleCase();
	model = model.toSingular();
	model = model.replace("_","")
	ID = model+"_"+id
	return ID;
}

//We appendTo body to get around an issue where modal aren't shown in the foreground
//if an ancestor has position: fixed.
//See http://stackoverflow.com/questions/10636667/bootstrap-modal-appearing-under-background
$(document).ready(function() {
	$('#tree_legend_modal').appendTo('body');
	$('#filter_description_modal').appendTo('body');
});

// ****** Addition String functionality ******
String.prototype.toTitleCase = function() {
    return this.replace(/(?:^|\s|_)\w/g, function(match) {
        return match.toUpperCase();
    });
};

//note that this function is not very smart, it just removes 's'
//from the word (fine for circumstances I'm using this)
String.prototype.toSingular = function() {
	if (this.charAt(this.length-1)=="s") {
		return this.substring(0,this.length-1);
	}
	else{
		return this;
	}
};

String.prototype.toUnderscore = function(){
	var newStr = this.replace(/([A-Z])/g, function($1){return "_"+$1.toLowerCase();});
	return newStr.substring(1);
};
String.prototype.toPlural = function(){
	if(this[this.length-1]!='s') {
		return this+"s";
	}
	else {
		return this;
	}
};
// ***** end of String functionality ******
