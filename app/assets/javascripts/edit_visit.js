$(document).ready(function() {
  visit_edit_multiselects();
  $('#keyword_new_modal').appendTo("body");
  $('#photographer_new_modal').appendTo("body");
  $('#fn_author_new_modal').appendTo("body");
  $('#hiking_party_participant_new_modal').appendTo("body");
});
$(document).on('pjax:success', function(){
  visit_edit_multiselects();
  $('#keyword_new_modal').appendTo("body");
  $('#photographer_new_modal').appendTo("body");
  $('#fn_author_new_modal').appendTo("body");
  $('#hiking_party_participant_new_modal').appendTo("body");
});

//Sets up the mulitselect fields in the visit form
var visit_edit_multiselects = function(){
  jQuery('#settings_form_admin_users').multiSelect({
    selectableHeader: "<div class='multiselect-header'>Non Admin Users</div>",
    selectionHeader: "<div class='multiselect-header'>Admin Users</div>"
  });
  jQuery('#visit_photographer_ids').multiSelect({
    selectableHeader: "<div class='multiselect-header'>Choose a pariticpant</div>",
    selectionHeader: "<div class='multiselect-header'>Selected photographer(s)</div>"
  });
  jQuery('#visit_fn_author_ids').multiSelect({
    selectableHeader: "<div class='multiselect-header'>Choose an author</div>",
    selectionHeader: "<div class='multiselect-header'>Selected authors(s)</div>"
  });
  jQuery('#visit_hiking_party_participant_ids').multiSelect({
    selectableHeader: "<div class='multiselect-header'>Choose a pariticpant</div>",
    selectionHeader: "<div class='multiselect-header'>Selected participants(s)</div>"
  });
  jQuery('#visit_keyword_ids').multiSelect({
    selectableHeader: "<div class='multiselect-header'>Choose a keyword</div>",
    selectionHeader: "<div class='multiselect-header'>Selected keyword(s)</div>"
  });
};
