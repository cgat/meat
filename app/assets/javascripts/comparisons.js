$(document).ready(function() {
    setup();
    $(document).on('pjax:complete', setup);
  });

var setup = function() {
  set_dropdown_events("display_bc_image","bc", "base_comparison");
  set_tab_events();
  $('#comparison_right_tab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    set_tab_events();
  });
}

//In the comparison page, each capture to compare will have a dropdown box
//and a set of images. If the user changes the dropdown box, we want to show
//the image associated with the selection. This is done by hiding all the images
//in a specified container and show the image associated with the selected
//item.
var set_dropdown_events = function(select_name, image_id_prefix, image_container) {
  $("select[name="+select_name+"]").change( function () {
    $("#"+image_container+" img").hide();
    $("#"+image_id_prefix+"_"+this.value).show();
  });
}
//One capture/hcapture may have many comparisons. Each comparison is shown
//in a tab. When a tab is changed, we need to setup the events associated with that
//capture/hcapture.
var set_tab_events = function () {
  var tab_id = $("#comparison_right").find('div.active').attr('id');
  if (typeof tab_id != 'undefined') {
    var obj_id = tab_id.split("_")[1];
    //console.log("display_c_image_"+obj_id);
    set_dropdown_events("display_c_image_"+obj_id, "c", "comparison_"+obj_id);
    }
  };
