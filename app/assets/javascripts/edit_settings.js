$(document).ready(function() {
  settings_edit_multiselects();
});
$(document).on('pjax:success', function(){
  settings_edit_multiselects();
});

//Sets up the mulitselect fields in the visit form
var settings_edit_multiselects = function(){
  jQuery('#settings_form_admin_users').multiSelect({
    selectableHeader: "<div class='multiselect-header'>Non Admin Users</div>",
    selectionHeader: "<div class='multiselect-header'>Admin Users</div>"
  });
};
