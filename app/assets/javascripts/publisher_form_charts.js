
var load_charts = function() {
  if ($('#capture_chart').length>0) {
    captures_data = $('#capture_chart').data('content')
    capturesChart = new Chart($("#capture_chart").get(0).getContext("2d")).Pie(captures_data);
    legend($("#capture_legend").get(0), captures_data);

    historic_captures_data = $('#historic_capture_chart').data('content')
    hcapturesChart = new Chart($("#historic_capture_chart").get(0).getContext("2d")).Pie(historic_captures_data);
  }
}
$(document).on('pjax:success', load_charts);
$(document).ready(load_charts);

