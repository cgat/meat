class GlassPlateListingsController < ApplicationController
  load_and_authorize_resource
  def new
    load_parent_resources
    respond_to do |format|
      format.html { render 'new' }
      format.js { render 'new' }
    end
  end

  def create
    if @glass_plate_listing.save
      flash[:success] = "Glass plate listing successfully created"
      redirect_to @glass_plate_listing.survey_season
    else
      render 'new'
    end
  end

  def edit
    load_parent_resources
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def update
    if @glass_plate_listing.update_attributes(params[:glass_plate_listing])
      redirect_to @glass_plate_listing.survey_season
    end
  end

  def destroy
    @survey_season = @glass_plate_listing.survey_season
    @glass_plate_listing.destroy
    flash[:success] = "Glass plate listing successfully deleted"
    redirect_to @survey_season
  end

private
  def load_parent_resources
    @survey_season = SurveySeason.find(params[:survey_season_id])
    @survey = @survey_season.parent
    @surveyor = @survey.parent
  end
end
