class PublisherFormsController < ApplicationController
  load_and_authorize_resource

  def edit
    @publisher_form = PublisherForm.new
  end

  def update
    @publisher_form = PublisherForm.new
    if @publisher_form.submit
      flash[:success] = "Successfully initiated publish update"
    end
    render 'edit'
  end

end
