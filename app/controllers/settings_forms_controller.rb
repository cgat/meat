class SettingsFormsController < ApplicationController
  load_and_authorize_resource

  def edit
    @settings_form = SettingsForm.new
  end

  def update
    @settings_form = SettingsForm.new
    if @settings_form.submit(params[:settings_form])
      flash[:success] = "Successfully updated settings"
      if params.has_key?("go_to_publisher")
        redirect_to publisher_path
      else
        redirect_to root_url
      end
    else
      render 'edit'
    end
  end

end
