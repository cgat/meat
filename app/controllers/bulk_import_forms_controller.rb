class BulkImportFormsController < ApplicationController
  #authorize_resource

  before_filter :set_current_tree_node

  def new
    @bulk_import_form = BulkImportForm.new(params)
    @bulk_import_form.parent_class = params[:parent_class]
    @bulk_import_form.parent_id = params[:parent_id]
    @bulk_import_form.association = params[:association]
  end

  def create
    @bulk_import_form = BulkImportForm.new(params[:bulk_import_form])
    if @bulk_import_form.submit
      flash[:success] = "Successfully imported images."
      redirect_to @bulk_import_form.parent
    else
      render 'new'
    end
  end

  def set_current_tree_node
    if @bulk_import_form
      session[:current_tree_node]=@bulk_import_form.parent_class+'_'+@bulk_import_form.parent_id
      session[:current_tree_node_type] = @bulk_import_form.parent_class
      session[:current_tree_node_id] = @bulk_import_form.parent_id
    else
      p = params[:bulk_import_form] || params
      session[:current_tree_node]=p[:parent_class]+'_'+p[:parent_id]
      session[:current_tree_node_type] = p[:parent_class]
      session[:current_tree_node_id] = p[:parent_id]
    end
  end

end
