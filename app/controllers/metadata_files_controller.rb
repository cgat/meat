class MetadataFilesController < ApplicationController
  load_and_authorize_resource

  def download
    head(:not_found) and return if (@metadata_file = MetadataFile.find_by_id(params[:id])).nil? || !@metadata_file.file_present?
    head(:ok)
    send_file @metadata_file.metadata_file.file.path, x_sendfile: true, disposition: 'attachment', filename: @metadata_file.display_name
  end

  def open
    head(:not_found) and return if (@metadata_file = MetadataFile.find_by_id(params[:id])).nil? || !@metadata_file.file_present?
    head(:ok)
    `open "#{@metadata_file.metadata_file.file.path}"`
  end

  def move
    if params.has_key?(:parent_id) && params.has_key?(:parent_type) && params.has_key?(:id)
      @metadata_file = MetadataFile.find(params[:id])
      @metadata_file.metadata_owner_id = params[:parent_id]
      @metadata_file.metadata_owner_type = params[:parent_type]
      if (["Station","Visit"].include?(params[:parent_type])) && @metadata_file.save
        flash.now[:success] = "Metadata File successfully moved!"
        render 'shared/remove_md_file_from_page'
      else
        flash.now[:error] = "Metadata File failed to be move :("
        head :bad_request
      end
    end
  end
end
