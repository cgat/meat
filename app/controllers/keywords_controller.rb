class KeywordsController < ApplicationController
  load_and_authorize_resource

  def new
    respond_to do |format|
      format.js { 'new_modal' }
      format.html { 'new' }
    end
  end

  def create
    if @keyword.save
      respond_to do |format|
        format.js { render 'shared/update_visit_form_keywords' }
      end
    else
      respond_to do |format|
        format.js { render 'shared/error_visit_form_keywords'}
      end
    end
  end

end
