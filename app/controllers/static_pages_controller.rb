class StaticPagesController < ApplicationController
  def home
  end

  def user_guide
    render layout: "no_nav_tree"
  end

  def faq
    render layout: "no_nav_tree"
  end

end
