
class FieldNotesController < ApplicationController
  load_and_authorize_resource
  def download
    head(:not_found) and return if (@field_note = FieldNote.find_by_id(params[:id])).nil? || !@field_note.file_present?
    head(:ok)
    send_file @field_note.field_note_file.file.path, x_sendfile: true, disposition: 'attachment'
  end

  def open
    head(:not_found) and return if (@field_note = FieldNote.find_by_id(params[:id])).nil? || !@field_note.file_present?
    head(:ok)
    `open "#{@field_note.field_note_file.file.path}"`
  end
end
