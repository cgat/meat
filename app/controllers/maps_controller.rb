class MapsController < ApplicationController
  load_and_authorize_resource
  def new
    load_parent_resources
    respond_to do |format|
      format.html { render 'new' }
      format.js { render 'new' }
    end
  end

  def create
    if @map.save
      flash[:success] = "Map successfully created"
      redirect_to @map.survey_season
    else
      render 'new'
    end
  end

  def edit
    load_parent_resources
    respond_to do |format|
      format.html { render 'edit' }
      format.js { render 'edit'}
    end
  end

  def update
    if @map.update_attributes(params[:map])
      redirect_to @map.survey_season
    end
  end

  def destroy
    @survey_season = @map.survey_season
    @map.destroy
    flash[:success] = "Map successfully deleted"
    redirect_to @survey_season
  end

private
  def load_parent_resources
    @survey_season = SurveySeason.find(params[:survey_season_id])
    @survey = @survey_season.parent
    @surveyor = @survey.parent
  end
end
