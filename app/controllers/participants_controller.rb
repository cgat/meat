class ParticipantsController < ApplicationController
  load_and_authorize_resource
  skip_load_resource only: :create

  def new
    respond_to do |format|
      format.js { 'new_modal' }
      format.html { 'new' }
    end
  end

  def create
    #participant_type is used for determining which "Create Participant" link was selected in the
    #visit form (Photographer, Author, or Hiking Party). This way we can add the newly created
    #participant to all the forms. see update_visit_form_participants.js.erb
    if params[:participant].has_key?(:participant_type)
      @participant_type = params[:participant][:participant_type]
      params[:participant].delete(:participant_type)
    end
    @participant = Participant.new(params[:participant])
    if @participant.save
      respond_to do |format|
        format.js { render 'shared/update_visit_form_participants' }
      end
    else
      respond_to do |format|
        format.js { render 'shared/error_visit_form_participants'}
      end
    end
  end

end
