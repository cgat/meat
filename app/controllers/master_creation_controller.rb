class MasterCreationController < ApplicationController

  def new
    @image_pair_form = ImageReggy::ImagePairForm.new
    authorize! :new, @image_pair_form
    @initiator = CaptureImage.find(params[:id])
    parent = @initiator.captureable
    @comparable_capture_images  = parent.comparisons.map{|cap| cap.capture_images }.flatten
    if @comparable_capture_images.blank? || ALIGNERS.blank?
      flash['error'] = 'Cannot master this capture image. No comparison captures or no aligners.'
      redirect_to @initiator
    else
      render 'new'
    end
  end

  def create
    @image_pair_form = ImageReggy::ImagePairForm.new
    authorize! :create, @image_pair_form
    if @image_pair_form.create(params[:image_pair_form])
      redirect_to image_reggy.image_pair_alignment_path(@image_pair_form.image_pair, :set_control_points)
    else
      @initiator = CaptureImage.find(params[:id])
      parent = @initiator.captureable
      @comparable_capture_images  = parent.comparisons.map{|cap| cap.capture_images }.flatten
      render 'new'
    end
  end


end
