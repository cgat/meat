require "bundler/capistrano"
require 'sidekiq/capistrano'
# Load RVM's capistrano plugin.
require "rvm/capistrano"
require 'capistrano-unicorn'


server "142.104.186.168", :web, :app, :db, primary: true
#only use this if the deployment server is setup with sub uris
#(on the lab imac, check /usr/local/nginx/conf/nginx.conf)
# set :default_environment, {
#   'RAILS_RELATIVE_URL_ROOT' => '/meat'
# }
#set :asset_env, "#{asset_env} RAILS_RELATIVE_URL_ROOT='/meat'"
set :sidekiq_cmd, "bundle exec sidekiq --config config/sidekiq.yml"
set :shared_children, shared_children + %w{public/uploads}
set :application, "meat"
set :repository,  "https://cgat@bitbucket.org/cgat/meat.git"
set :scm, :git
set :user, "mlp"
set :deploy_to, "/rails_applications/deployments/meat/"
set :deploy_via, :remote_cache
set :use_sudo, false

#rvm-capistrano stuff
set :rvm_ruby_string, :local
before 'deploy', 'rvm:install_rvm'
before 'deploy', 'rvm:install_ruby'
set :bundle_dir, ''
set :bundle_flags, '--system --quiet'

default_run_options[:pty] = true

after 'deploy:restart', 'unicorn:restart'   # app preloaded
before 'deploy:assets:precompile' do
  run "#{try_sudo} ln -s #{shared_path}/.env #{latest_release}/"
end

namespace :deploy do
end
