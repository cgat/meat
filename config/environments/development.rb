Meat::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default_url_options = { :host => 'localhost:3000' }

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Expands the lines which load the assets
  config.assets.debug = true

  config.eager_load = false

  ############
  #If you want to run development with the asset pipeline, uncomment this,
  #precompile assets with RAILS_ENV set to production, and run rails server
  #in development (unset RAILS_ENV)
  # Don't fallback to assets pipeline if a precompiled asset is missed
  #config.assets.compile = false
  # Generate digests for assets URLs
  #config.assets.digest = true
  #config.assets.css_compressor = :sass
  #############


end
