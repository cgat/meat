require 'sidekiq/web'
Meat::Application.routes.draw do

  devise_for :users
  mount ImageReggy::Engine => "/image_reggy"

  root to: 'static_pages#home', layout: 'app/views/layouts/application.html.erb'

  get '/user_guide', to: 'static_pages#user_guide', layout: 'app/views/layouts/no_nav_tree.html.erb'
  get '/faq', to: 'static_pages#faq', layout: 'app/views/layouts/no_nav_tree.html.erb'
  #to access the sidekiq background jobs throught the web interface
  mount Sidekiq::Web, at: "/sidekiq"

  post 'captures/move'
  post 'captures/merge'
  post 'historic_captures/move'
  post 'historic_captures/merge'
  post 'capture_images/move'
  post 'location_images/move'
  post 'metadata_files/move'
  post 'survey_seasons/move'

  resources :location_images

  resources :surveyors do
    resources :surveys
  end
  resources :surveys, except: :new do
    resources :survey_seasons
    resources :captures
    resources :historic_captures
  end
  resources :stations, except: :new do
    resources :visits
    resources :historic_visits
    resources :captures
  end
  resources :survey_seasons, except: :new do
    resources :stations
    resources :captures
    resources :historic_captures
    resources :glass_plate_listings
    resources :maps
  end

  resources :captures, except: :new do
    resources :capture_images
    member do
      get 'comparisons'
      get 'overlays'
    end
  end

  resources :projects do
    resources :stations
    resources :captures
    resources :historic_captures
  end

  resources :historic_captures do
    member do
      get 'comparisons'
      get 'overlays'
    end
  end

  #if these routes are changed, make sure javascript is reloaded on client.
  get 'application/update_historic_captures', as: 'update_historic_captures'
  get 'application/treedata', to: 'application#treedata', as: 'treedata'
  get 'filters/:filter_name', to: 'application#filters', as: "filter"

  resources :bulk_import_forms
  get 'settings', to: 'settings_forms#edit'
  patch 'settings', to: 'settings_forms#update'
  post 'settings', to: 'settings_forms#update'
  get 'publisher', to: 'publisher_forms#edit'
  patch 'publisher', to: 'publisher_forms#update'
  post 'publisher', to: 'publisher_forms#update'


  resources :visits, except: :new do
    resources :captures
  end
  resources :locations, except: :new do
    resources :captures
  end
  resources :keywords
  resources :participants


  resources :historic_visits, except: :new
  resources :capture_images, except: :new do
    member do
      get 'download'
      get 'open'
      resource 'master_creation', controller: 'master_creation', as: 'master_creation', only: [:new, :create]
    end
  end

  resources :location_images, except: :new do
    member do
      get "download"
      get "open"
    end
  end

  get "/field_note/:id/open" => "field_notes#open"
  get "/field_note/:id/download" => "field_notes#download"
  get "/metadata_file/:id/open" => "metadata_files#open"
  get "/metadata_file/:id/download" => "metadata_files#download"

  get "/stations/:id/open_old" => "stations#open_old"

  #unmanaged metadata folders
  get "/surveys/:id/open_metadata_folder" => "surveys#open_metadata_folder"
  get "/survey_seasons/:id/open_metadata_folder" => "survey_seasons#open_metadata_folder"
  get "/projects/:id/open_metadata_folder" => "projects#open_metadata_folder"



  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
