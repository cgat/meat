class VIPSSidekiqMiddleware
  def call(worker, job, queue)
    yield
  ensure
    ::VIPS::thread_shutdown if defined?(::VIPS)
  end
end

Sidekiq.configure_server do |config|
  config.server_middleware do |chain|
    chain.add VIPSSidekiqMiddleware
  end
end