# Be sure to restart your server when you modify this file.

Meat::Application.config.session_store :cookie_store, key: '_meat_session'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# Meat::Application.config.session_store :active_record_store

#see rails 4.0 to 4.1 upgrade guide
Rails.application.config.action_dispatch.cookies_serializer = :hybrid
