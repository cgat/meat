app_path = "/rails_applications/deployments/meat/current"
working_directory app_path
pid "#{app_path}/tmp/pids/unicorn.pid"
stderr_path "#{app_path}/log/unicorn.stderr.log"
stdout_path "#{app_path}/log/unicorn.stdout.log"

listen "/tmp/unicorn.meat.sock"
worker_processes 2
#long timeouts for big file processing
timeout 360
