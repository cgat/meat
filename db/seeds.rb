# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#make the root of the dev library
`mkdir MLPLibraryDev`
user = User.where(email: "test@example.com", admin: true).first_or_create
user.reset_password("password123", "password123")
pref = Preference.where(mlp_library_local_root: Rails.root.join('MLPLibraryDev').to_s).first_or_create
wheeler = Surveyor.where(last_name: 'Wheeler', given_names: 'A.O.', affiliation: "Dominion Land Survey", short_name: "WHE").first_or_create

irrigation = Survey.where(name: "Canadian Irrigation Survey").first_or_create
irrigation.surveyor = wheeler
irrigation.save!

season = SurveySeason.where(year: "1896").first_or_create
season.survey = irrigation
season.save!

station = Station.where(name: "B South").first_or_create
station.station_owner = season
station.save!
