# This migration comes from image_reggy (originally 20140206105638)
class ChangeImageMetaColumnInAlignmentImages < ActiveRecord::Migration
  def up
    change_column :image_reggy_alignment_images, :image_meta, :text
  end

  def down
    change_column :image_reggy_alignment_images, :image_meta, :string
  end
end
