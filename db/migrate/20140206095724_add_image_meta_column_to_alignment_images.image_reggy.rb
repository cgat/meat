# This migration comes from image_reggy (originally 20140124122901)
class AddImageMetaColumnToAlignmentImages < ActiveRecord::Migration
  def change
    add_column :image_reggy_alignment_images, :image_meta, :string
  end
end
