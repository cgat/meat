# This migration comes from image_reggy (originally 20140203094907)
class AddAlignerColumnToImagePairs < ActiveRecord::Migration
  def change
    add_column :image_reggy_image_pairs, :aligner, :string
  end
end
