# This migration comes from image_reggy (originally 20140114222221)
class AddDimensionColumnsToAlignmentImage < ActiveRecord::Migration
  def change
    add_column :image_reggy_alignment_images, :width, :integer
    add_column :image_reggy_alignment_images, :height, :integer
  end
end
