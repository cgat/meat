# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130808231243) do

  create_table "cameras", :force => true do |t|
    t.string   "model"
    t.string   "unit"
    t.string   "format"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "make"
  end

  create_table "capture_images", :force => true do |t|
    t.integer  "captureable_id"
    t.string   "hash_key"
    t.text     "image"
    t.float    "file_size"
    t.integer  "x_dim"
    t.integer  "y_dim"
    t.integer  "bit_depth"
    t.string   "image_state"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "image_remote"
    t.string   "captureable_type"
    t.string   "image_secure_token"
    t.text     "legacy_path"
    t.string   "comments"
    t.text     "fs_path"
    t.string   "image_tmp"
    t.boolean  "image_remote_processing"
  end

  add_index "capture_images", ["captureable_id", "captureable_type"], :name => "index_capture_images_on_captureable_id_and_captureable_type"
  add_index "capture_images", ["legacy_path"], :name => "index_capture_images_on_legacy_path"

  create_table "captures", :force => true do |t|
    t.integer  "capture_owner_id"
    t.string   "fn_photo_reference"
    t.float    "f_stop"
    t.string   "shutter_speed"
    t.integer  "iso"
    t.integer  "focal_length"
    t.integer  "camera_id"
    t.integer  "lens_id"
    t.datetime "capture_datetime"
    t.float    "lat"
    t.float    "long"
    t.float    "elevation"
    t.integer  "azimuth"
    t.string   "comments"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.boolean  "alternate"
    t.string   "capture_owner_type"
  end

  add_index "captures", ["capture_owner_id", "capture_owner_type"], :name => "index_captures_on_capture_owner_id_and_capture_owner_type"

  create_table "comparison_indices", :force => true do |t|
    t.integer  "capture_id"
    t.integer  "historic_capture_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "field_notes", :force => true do |t|
    t.string   "field_note_file"
    t.integer  "visit_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.text     "legacy_path"
    t.text     "fs_path"
  end

  add_index "field_notes", ["legacy_path"], :name => "index_field_notes_on_legacy_path"
  add_index "field_notes", ["visit_id"], :name => "index_field_notes_on_visit_id"

  create_table "fn_authors_visits", :id => false, :force => true do |t|
    t.integer "participant_id", :null => false
    t.integer "visit_id",       :null => false
  end

  add_index "fn_authors_visits", ["participant_id", "visit_id"], :name => "index_fn_authors_visits_on_participant_id_and_visit_id", :unique => true

  create_table "hiking_parties", :force => true do |t|
    t.integer  "participant_id"
    t.integer  "visit_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "historic_captures", :force => true do |t|
    t.string   "plate_id"
    t.string   "fn_photo_reference"
    t.float    "f_stop"
    t.string   "shutter_speed"
    t.integer  "focal_length"
    t.integer  "camera_id"
    t.integer  "lens_id"
    t.datetime "capture_datetime"
    t.string   "digitization_location"
    t.datetime "digitization_datetime"
    t.string   "lac_ecopy"
    t.string   "lac_wo"
    t.string   "lac_collection"
    t.string   "lac_box"
    t.string   "lac_catalogue"
    t.string   "condition"
    t.string   "comments"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "capture_owner_id"
    t.string   "capture_owner_type"
  end

  create_table "historic_visits", :force => true do |t|
    t.integer  "station_id"
    t.date     "date"
    t.text     "comments"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "fs_path"
  end

  create_table "images", :force => true do |t|
    t.string   "image"
    t.string   "hash_key"
    t.float    "file_size"
    t.integer  "x_dim"
    t.integer  "y_dim"
    t.integer  "bit_depth"
    t.string   "image_remote"
    t.integer  "image_owner_id"
    t.string   "image_owner_type"
    t.string   "image_secure_token"
    t.text     "legacy_path"
    t.string   "comments"
    t.float    "lat"
    t.float    "long"
    t.float    "elevation"
    t.float    "f_stop"
    t.string   "shutter_speed"
    t.integer  "iso"
    t.integer  "focal_length"
    t.datetime "capture_datetime"
    t.integer  "camera_id"
    t.integer  "lens_id"
    t.string   "type"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.text     "fs_path"
    t.string   "image_tmp"
    t.boolean  "image_remote_processing"
  end

  add_index "images", ["image_owner_id"], :name => "index_images_on_image_owner_id"
  add_index "images", ["image_owner_type"], :name => "index_images_on_image_owner_type"

  create_table "keyword_visit_associations", :force => true do |t|
    t.integer  "keyword_id"
    t.integer  "visit_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "keywords", :force => true do |t|
    t.string   "keyword"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "lens", :force => true do |t|
    t.string   "brand"
    t.string   "focal_length"
    t.float    "max_aperture"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "location_photos", :force => true do |t|
    t.integer  "location_id"
    t.string   "hash_key"
    t.string   "image"
    t.float    "file_size"
    t.integer  "x_dim"
    t.integer  "y_dim"
    t.integer  "bit_depth"
    t.integer  "camera_id"
    t.datetime "capture_datetime"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "image_remote"
  end

  create_table "locations", :force => true do |t|
    t.integer  "visit_id"
    t.text     "location_narrative"
    t.string   "location_identity"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.float    "lat"
    t.float    "long"
    t.integer  "legacy_photos_start"
    t.integer  "legacy_photos_end"
    t.float    "elevation"
  end

  create_table "metadata_files", :force => true do |t|
    t.string   "metadata_file"
    t.text     "legacy_path"
    t.integer  "metadata_owner_id"
    t.string   "metadata_owner_type"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.text     "fs_path"
  end

  create_table "participants", :force => true do |t|
    t.string   "last_name"
    t.string   "given_names"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "pg_search_documents", :force => true do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "photographers_visits", :id => false, :force => true do |t|
    t.integer "participant_id", :null => false
    t.integer "visit_id",       :null => false
  end

  add_index "photographers_visits", ["participant_id", "visit_id"], :name => "index_photographers_visits_on_participant_id_and_visit_id", :unique => true

  create_table "preferences", :force => true do |t|
    t.string   "mlp_library_local_root"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "projects", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.text     "description"
    t.text     "legacy_path"
    t.text     "fs_path"
  end

  create_table "stations", :force => true do |t|
    t.string   "name"
    t.float    "lat"
    t.float    "long"
    t.float    "elevation"
    t.string   "nts_sheet"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.text     "fs_path"
    t.integer  "station_owner_id"
    t.string   "station_owner_type"
  end

  add_index "stations", ["station_owner_id"], :name => "index_stations_on_station_owner_id"
  add_index "stations", ["station_owner_type"], :name => "index_stations_on_station_owner_type"

  create_table "survey_seasons", :force => true do |t|
    t.integer  "survey_id"
    t.integer  "year"
    t.string   "geographic_coverage"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.text     "legacy_path"
    t.text     "fs_path"
    t.boolean  "unknown",             :default => false
  end

  add_index "survey_seasons", ["legacy_path"], :name => "index_survey_seasons_on_legacy_path"

  create_table "surveyors", :force => true do |t|
    t.string   "last_name"
    t.string   "given_names"
    t.string   "short_name"
    t.string   "affiliation"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.text     "fs_path"
    t.boolean  "unknown",     :default => false
  end

  create_table "surveys", :force => true do |t|
    t.string   "name"
    t.string   "historical_map_sheet"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.text     "legacy_path"
    t.integer  "surveyor_id"
    t.text     "fs_path"
    t.boolean  "unknown",              :default => false
  end

  add_index "surveys", ["legacy_path"], :name => "index_surveys_on_legacy_path"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "visits", :force => true do |t|
    t.integer  "station_id"
    t.date     "date"
    t.time     "start_time"
    t.time     "finish_time"
    t.string   "pilot"
    t.string   "rw_call_sign"
    t.text     "visit_narrative"
    t.boolean  "illustration"
    t.text     "weather_narrative"
    t.float    "weather_temp"
    t.float    "weather_ws"
    t.float    "weather_gs"
    t.float    "weather_pressure"
    t.float    "weather_rh"
    t.float    "weather_wb"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.text     "fs_path"
  end

end
