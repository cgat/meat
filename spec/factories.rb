require 'faker'
include ActionDispatch::TestProcess #include so that we can use the fixture_file_upload with carrierwave uploads
FactoryGirl.define do

#Note: sub factories with _invalid are filled with invalid attribute values and
#sub facatories with _alternate should have unique fields compared to the default.
#Both are used in the feature specs. If an attribute does not have a good invalid
#value when filled on the web frontend, then make sure the attribute = nil
  #####GPS related traits#####
  trait :decimal_lat_long do
    lat 48.595139
    long -123.881836
    elevation 2000
    lat_d nil
    lat_m nil
    lat_s nil
    lat_dir nil
    long_d nil
    long_m nil
    long_s nil
    long_dir nil
  end
  trait :DMS_lat_long do
    long_d 114
    long_m 43
    long_s 7.518
    long_dir 1
    lat_d 51
    lat_m 8
    lat_s 33.23
    lat_dir 1
    elevation 2000
  end
  trait :DMS_lat_long_invalid do
    lat_d 10000
    lat_m 10000
    lat_s 10000
    lat_dir 10000
    long_d 10000
    long_m 10000
    long_s 10000
    long_dir 10000
    elevation "notNumber"
  end
  trait :DMS_lat_long_alternate do
    lat_d 12
    lat_m 12
    lat_s 13
    lat_dir 1
    long_d 31
    long_m 21
    long_s 2
    long_dir 1
    elevation 1002
  end
  trait :MinDec_lat_long do
    long_d 114
    long_m 43.34
    long_s nil
    long_dir 1
    lat_d 50
    lat_m 8.45
    lat_s nil
    lat_dir 1
    elevation 2000
  end
  trait :UTM do
    zone '11U'
    easting 650000
    northing 5530000
    elevation 2000
  end
  ###########################
  factory :user do
    email "test@example.com"
    password "testing5678"
    admin true
  end

  factory :project do
    sequence(:name) { |n| "Project#{n}" }
    description "This is a Repeat Project"
    factory :project_invalid do
      name "a"*256
      description nil
    end
    factory :project_alternate do
      description "Another Repeat Project"
    end
    factory :project_with_metadata do
      transient do
        metadata_file_count 5
      end
      after(:create) do |this_project, evaluator|
        FactoryGirl.create_list(:metadata_file, evaluator.metadata_file_count, metadata_owner: this_project)
      end
    end
  end
  factory :surveyor do
    sequence(:last_name)  { |n| "LastName #{n}" }
    sequence(:given_names) { |n| "FirstName#{n}"}
    sequence(:short_name) { |n| n.to_s }
    affiliation "Government"
    factory :surveyor_invalid do
      last_name "a"*256
      given_names "b"*256
      short_name "ASDFG"
      affiliation "a"*256
    end
    factory :surveyor_alternate do
      affiliation "SRD"
    end
  end


  factory :survey do
  	sequence(:name) { |n| "SurveyName #{n}" }
  	historical_map_sheet "map123"
  	surveyor
  	factory :survey_with_survey_seasons do
  	  after(:create) do |this_survey|
  	    FactoryGirl.create_list(:survey_season,5,survey: this_survey)
	    end
    end
  	factory :survey_invalid do
  	  name "a"*256
  	  historical_map_sheet "a"*256
	  end
	  factory :survey_alternate do
	    historical_map_sheet "map432"
    end

  end


  factory :survey_season do
    sequence(:geographic_coverage) { |n| "SomePlace#{n}" }
    sequence(:year) { |n| Date.current.year-100-n }
    survey
    factory :survey_season_invalid do
      geographic_coverage "a"*256
      year 12
    end
    factory :survey_season_alternate do
    end
  end

  factory :station do
    sequence(:name) { |n| "Stn. #{n}"}
    association :station_owner, factory: :survey_season
    factory :station_owned_by_project do
      association :station_owner, factory: :project
    end
    factory :station_decimal_lat_long, traits: [:decimal_lat_long]
    factory :station_DMS_lat_long, traits: [:DMS_lat_long]
    factory :station_MinDec_lat_long, traits: [:MinDec_lat_long]
    factory :station_UTM, traits: [:UTM]
    factory :station_invalid do
      name "a"*256
    end
    factory :station_alternate do
      nts_sheet "Sheet87764"
    end
    factory :station_with_metadata do
      transient do
        metadata_file_count 5
      end
      after(:create) do |this_station, evaluator|
        FactoryGirl.create_list(:metadata_file, evaluator.metadata_file_count, metadata_owner: this_station)
      end
    end
  end

  factory :visit do
    sequence(:date) { |n| Date.current-n }
    start_time Time.zone.now
    finish_time Time.zone.now + 800
    pilot 'Tom'
    rw_call_sign 'Golf Alpha Tango'
    visit_narrative 'a'*256 + 'b'*10
    illustration false
    weather_narrative 'a'*256 + 'b'*10
    weather_temp 25.1
    weather_ws 20.3
    weather_gs 30.2
    weather_pressure 2.3
    weather_rh 10.2
    weather_wb 20.43
    fn_physical_location "Book 4, 2008, pg. 28-32"
    fn_transcription_comment "Couldn't find captures"
    station
    after(:create) do |this_visit|
      participant = FactoryGirl.create(:participant)
      FactoryGirl.create(:hiking_party, participant: participant, visit: this_visit)
      keyword = FactoryGirl.create(:keyword)
      FactoryGirl.create(:keyword_visit_association, keyword: keyword, visit: this_visit)
      photographer = FactoryGirl.create(:photographer)
      this_visit.photographers << photographer
      fn_author = FactoryGirl.create(:fn_author)
      this_visit.fn_authors << fn_author
      this_visit.save!
    end
    trait :with_field_notes do
      after(:create) do |this_visit|
        field_note = FactoryGirl.create(:field_note, visit_id: this_visit.id)
      end
    end
    factory :visit_with_field_notes, traits: [:with_field_notes]
    trait :with_metadata do
      transient do
        metadata_file_count 5
      end
      after(:create) do |this_visit, evaluator|
        FactoryGirl.create_list(:metadata_file, evaluator.metadata_file_count, metadata_owner: this_visit)
      end
    end
    factory :visit_with_metadata, traits: [:with_metadata]
    factory :visit_invalid do
      date nil
      start_time nil
      finish_time nil
      pilot "a"*21
      rw_call_sign "a"*31
      visit_narrative nil
      illustration nil
      weather_narrative nil
      weather_temp -1000
      weather_ws -1
      weather_gs -1
      weather_pressure -1
      weather_rh -1
      weather_wb -1000
      fn_physical_location nil
      fn_transcription_comment nil
    end
    factory :visit_alternate do
      start_time Time.zone.now+10
      finish_time Time.zone.now+810
      pilot 'Ted'
      rw_call_sign 'Cheeta Cougar 123'
      visit_narrative 'c'*256 + 'd'*10
      illustration true
      weather_narrative 'c'*256 + 'a'*10
      weather_temp 23
      weather_ws 21
      weather_gs 31
      weather_pressure 2
      weather_rh 11
      weather_wb 21
      fn_physical_location "Binder 1, 2009"
      fn_transcription_comment "Another COmment"
    end
    factory :visit_date_only do
      start_time nil
      finish_time nil
      pilot nil
      rw_call_sign nil
      visit_narrative nil
      illustration nil
      weather_narrative nil
      weather_temp nil
      weather_ws nil
      weather_gs nil
      weather_pressure nil
      weather_rh nil
      weather_wb nil
    end
  end

  factory :historic_visit do
    date nil #explicitly set to nil in factory. Only set when creating factory, because it has a dependence on survey_season year
    sequence(:comments) { |n| "This is a comment#{n}"+("a"*256) }
    station
  end

  factory :historic_capture do
    sequence(:fn_photo_reference) {|n|"BRI123#{n}"}
    f_stop 16
    shutter_speed "1/100"
    focal_length 35
    camera
    lens
    digitization_location "Library Archives Canada, Ottawa"
    digitization_datetime DateTime.civil_from_format(:utc, 2011,04,15,2,12,2)
    lac_ecopy 'e123456789'
    lac_wo 'WO-12345'
    lac_collection 'Collection100'
    lac_box '123A'
    lac_catalogue 'catalogue 2'
    comments "This is a comment about a historic capture"
    association :capture_owner, factory: :historic_visit
    trait :with_images do
      transient do
        image_count 5
      end
      after(:create) do |hcapture, evaluator|
        FactoryGirl.create_list(:historic_image_fake_image, evaluator.image_count, captureable: hcapture, do_not_save_metadata: true)
      end
    end
    factory :historic_capture_with_images, traits: [:with_images]
    factory :unsorted_hcapture_survey_season do
      association :capture_owner, factory: :survey_season
    end
    factory :unsorted_hcapture_survey do
      association :capture_owner, factory: :survey
    end

    trait :without_camera_info do
      f_stop nil
      shutter_speed nil
      focal_length nil
      camera nil
      lens nil
    end
    trait :without_fn_info do
      fn_photo_reference nil
      comments nil
      plate_id nil
      digitization_location nil
      digitization_datetime nil
      lac_ecopy nil
      lac_collection nil
      lac_box nil
      lac_wo nil
      lac_catalogue nil
    end
    trait :without_camera_lens do
      camera nil
      lens nil
    end
    trait :mastered do
      after(:create) do |this_hcapture|
        cap=FactoryGirl.create(:capture)
        cap_image = FactoryGirl.create(:capture_image, :historic_image, :bw_jpeg, captureable: cap)
        cap_image = FactoryGirl.create(:capture_image, :historic_image, :bw_jpeg, captureable: this_hcapture)
        this_hcapture.comparison_indices.create(capture_id: cap.id)
      end
    end
    trait :with_comparison_with_images do
      after(:create) do |this_hcapture|
        cap=FactoryGirl.create(:capture, :with_images)
        this_hcapture.comparison_indices.create(capture_id: cap.id)
      end
    end
    trait :with_comparisons do
      transient do
        comparison_count 5
      end
      after(:create) do |this_capture, evaluator|
        evaluator.comparison_count.times do |_|
          cap=FactoryGirl.create(:capture)
          this_capture.comparison_indices.create(capture_id: cap.id)
        end
      end
    end

    factory :hcapture_from_img, traits: [:without_fn_info,:with_images,:without_camera_lens]
    factory :hcapture_from_fn, traits: [:without_camera_info]
    factory :hcapture_without_attributes, traits: [:without_fn_info, :without_camera_lens, :without_camera_info]
    factory :hcapture_with_comparisons, traits: [:with_comparisons]

    factory :historic_capture_invalid do
      fn_photo_reference "a"*256
      plate_id "a"*256
      digitization_location "L"*256
      lac_ecopy 'e'*256
      lac_wo 'W'*256
      lac_collection 'C'*256
      lac_box '1'*256
      lac_catalogue 'c'*256
      comments "a"*256
    end
    factory :historic_capture_alternate do
      f_stop 8
      shutter_speed "1/500"
      focal_length 80
      digitization_location "BC, Ottawa"
      digitization_datetime DateTime.civil_from_format(:utc, 2011,03,14,7,12,0)
      lac_ecopy 'e98873121'
      lac_wo 'WO-54321'
      lac_collection 'Collection 101'
      lac_box '123B'
      lac_catalogue 'catalogue #3'
      comments "another comment"
    end

  end

  factory :location do
    visit
    sequence(:location_identity) { |n| "A#{n}"}
    location_narrative 'Narrative at location'
    factory :location_decimal_lat_long, traits: [:decimal_lat_long]
    factory :location_DMS_lat_long, traits: [:DMS_lat_long]
    factory :location_MinDec_lat_long, traits: [:MinDec_lat_long]
    factory :location_UTM, traits: [:UTM]
    factory :location_with_location_photos do
      after(:create) do |location1|
        FactoryGirl.create_list(:location_image, 5, image_owner: location1)
      end
    end
    factory :location_with_cimages do
      after(:create) do |location1|
        FactoryGirl.create(:capture_with_images, capture_owner: location1)
      end
    end
  end

  factory :comparison_index do
    historic_capture
    capture
  end


  factory :capture do
    sequence(:fn_photo_reference) { |n| "A12345#{n}"}
    camera
    lens
    f_stop 16
    shutter_speed "1/100"
    iso 50
    focal_length 35
    capture_datetime DateTime.now-400
    azimuth 21
    alternate false
    comments "This is a comment on a capture"
    association :capture_owner, factory: :location
    factory :capture_invalid do
      fn_photo_reference "a"*256
      comments "a"*256
      azimuth -500
      alternate nil
    end
    factory :capture_alternate do
      azimuth 100
      comments "EV++"
      alternate true
    end

    trait :owned_by_visit do
      association :capture_owner, factory: :visit
    end
    trait :owned_by_station do
      association :capture_owner, factory: :station
    end
    trait :owned_by_survey_season do
      association :capture_owner, factory: :survey_season
    end
    trait :owned_by_survey do
      association :capture_owner, factory: :survey
    end
    trait :with_nested_gps_image do
      after(:create) do |this_capture|
        FactoryGirl.create(:repeat_image_gps_small, captureable: this_capture)
      end
    end
    trait :without_camera_info do
      f_stop nil
      shutter_speed nil
      iso nil
      focal_length nil
      capture_datetime nil
      camera nil
      lens nil
    end
    trait :without_fn_info do
      fn_photo_reference nil
      azimuth nil
      comments nil
    end
    trait :without_camera_lens do
      camera nil
      lens nil
    end

    trait :with_images do
      transient do
        image_count 5
      end
      after(:create) do |this_capture, evaluator|
        FactoryGirl.create_list(:repeat_image_fake_image, evaluator.image_count, captureable: this_capture, do_not_save_metadata: true)
      end
    end
    trait :mastered do
      after(:create) do |this_capture|
        hcap=FactoryGirl.create(:historic_capture)
        cap_image = FactoryGirl.create(:capture_image, :repeat_image, :color_jpeg, captureable: hcap)
        cap_image = FactoryGirl.create(:capture_image, :repeat_image, :color_jpeg, captureable: this_capture)
        this_capture.comparison_indices.create(historic_capture_id: hcap.id)
      end
    end
    trait :with_comparison_with_images do
      after(:create) do |this_capture|
        hcap=FactoryGirl.create(:historic_capture, :with_images)
        this_capture.comparison_indices.create(historic_capture_id: hcap.id)
      end
    end
    trait :with_comparisons do
      transient do
        comparison_count 5
      end
      after(:create) do |this_capture, evaluator|
        evaluator.comparison_count.times do |_|
          hcap=FactoryGirl.create(:historic_capture)
          this_capture.comparison_indices.create(historic_capture_id: hcap.id)
        end
      end
    end

    factory :capture_decimal_lat_long, traits: [:decimal_lat_long]
    factory :capture_DMS_lat_long, traits: [ :DMS_lat_long]
    factory :capture_MinDec_lat_long, traits: [ :MinDec_lat_long]
    factory :capture_UTM, traits: [ :UTM]
    factory :capture_with_nested_gps_image, traits: [:without_camera_info, :with_nested_gps_image]
    factory :unsorted_capture_visit, traits: [:owned_by_visit]
    factory :unsorted_capture_survey_season, traits: [:owned_by_survey_season]
    factory :unsorted_capture_survey, traits: [:owned_by_survey]
    factory :capture_with_images, traits: [:with_images]
    factory :unsorted_capture_with_images, traits: [:owned_by_survey_season, :with_images]
    factory :capture_from_fn, traits: [:without_camera_info]
    factory :capture_from_img, traits: [:without_fn_info, :with_images, :without_camera_lens]
    factory :capture_with_comparisons, traits: [:with_comparisons]
    factory :capture_without_attributes, traits: [:without_fn_info, :without_camera_lens,:without_camera_info]
  end

  factory :capture_image do

    trait :repeat_image do
      association :captureable, factory: :capture
    end
    trait :historic_image do
      association :captureable, factory: :historic_capture
    end
    trait :color_jpeg do
      image { fixture_file_upload(Rails.root.join('spec','photos','color_repeat.jpg'))}
      remote "0"
    end
    trait :color_tiff do
      image { fixture_file_upload(Rails.root.join('spec','photos','color_repeat.tif'))}
      remote "0"
    end
    trait :color_3fr do
      image { fixture_file_upload(Rails.root.join('spec','photos','color_repeat.3FR'))}
      remote "0"
    end
    trait :color_fff do
      image { fixture_file_upload(Rails.root.join('spec','photos','fff_full.fff'))}
      remote "0"
    end
    trait :psd do
      image { fixture_file_upload(Rails.root.join('spec','photos','psd_full.psd'))}
      remote "0"
    end
    trait :gps_small do
      image { fixture_file_upload(Rails.root.join('spec','photos','gps_small.tif'))}
      remote "0"
    end
    trait :bw_jpeg do
      image { fixture_file_upload(Rails.root.join('spec','photos','bw_historic.jpg'))}
      remote "0"
    end
    trait :bw_tiff do
      image { fixture_file_upload(Rails.root.join('spec','photos','bw_historic.tif'))}
      remote "0"
    end
    trait :fake_image do
      image { fixture_file_upload(Rails.root.join('spec','photos','bw_historic.jpg'))}
      file_size 12355
      x_dim 3000
      y_dim 2222
      bit_depth 8
      remote "0"
    end
    #process_image_upload true
    hash_key 'ABC123959'
    comments 'This is a comment'
    image_state 'RAW'
    factory :repeat_image_no_image, traits: [:repeat_image]
    factory :historic_image_no_image, traits: [:historic_image]
    factory :repeat_image_color_tiff, traits: [:repeat_image, :color_tiff]
    factory :repeat_image_color_jpeg, traits: [:repeat_image, :color_jpeg]
    factory :repeat_image_color_3fr, traits: [:repeat_image, :color_3fr]
    factory :repeat_image_color_fff, traits: [:repeat_image, :color_fff]
    factory :repeat_image_fake_image, traits: [:repeat_image, :fake_image]
    factory :repeat_image_gps_small, traits: [:repeat_image, :gps_small]
    factory :historic_image_bw_tiff, traits: [:historic_image, :bw_tiff]
    factory :historic_image_psd, traits: [:historic_image, :psd]
    factory :historic_image_bw_jpeg, traits: [:historic_image, :bw_jpeg]
    factory :historic_image_fake_image, traits: [:historic_image, :fake_image]

  end


  factory :image do
    trait :location do
      type "LocationImage"
    end
    trait :scenic do
      type "ScenicImage"
    end
    trait :owned_by_location do
      association :image_owner, factory: :location
    end
    trait :owned_by_survey do
      association :image_owner, factory: :survey
    end
    trait :owned_by_survey_season do
      association :image_owner, factory: :survey_season
    end
    trait :gps_small do
      image { fixture_file_upload(Rails.root.join('spec','photos','gps_small.tif'))}
      remote "0"
    end
    trait :fake_image do
      image { fixture_file_upload(Rails.root.join('spec','photos','bw_historic.jpg'))}
      file_size 12355
      x_dim 3000
      y_dim 2222
      bit_depth 8
      f_stop 16
      shutter_speed "1/100"
      iso 50
      focal_length 35
      capture_datetime DateTime.now - 400
      remote "0"
    end
    trait :with_jpeg do
      image { fixture_file_upload(Rails.root.join('spec','photos','color_repeat.jpg'))}
      remote "0"
    end
    trait :with_tiff do
      image { fixture_file_upload(Rails.root.join('spec','photos','color_repeat.tif'))}
      remote "0"
    end
    trait :with_psd do
      image { fixture_file_upload(Rails.root.join('spec','photos','psd_full.psd'))}
      remote "0"
    end
    camera
    lens
    hash_key 'ABC123959'
    comments 'This is a comment'

    factory :location_image, traits: [:location, :owned_by_location, :fake_image], parent: :image, class: 'LocationImage'
    factory :image_decimal_lat_long, traits: [:scenic, :owned_by_survey_season, :decimal_lat_long,:fake_image], parent: :image, class: 'ScenicImage'
    factory :image_DMS_lat_long, traits: [:scenic, :owned_by_survey_season, :DMS_lat_long, :fake_image], parent: :image, class: 'ScenicImage'
    factory :image_MinDec_lat_long, traits: [:scenic, :owned_by_survey_season, :MinDec_lat_long, :fake_image], parent: :image, class: 'ScenicImage'
    factory :image_UTM, traits: [:scenic, :owned_by_survey_season, :UTM, :fake_image], parent: :image, class: 'ScenicImage'
    factory :real_image_jpeg, traits: [ :scenic, :owned_by_survey_season, :with_jpeg], parent: :image, class: 'ScenicImage'
    factory :real_image_tiff, traits: [ :scenic, :owned_by_survey_season, :with_tiff], parent: :image, class: 'ScenicImage'
    factory :real_image_psd, traits: [ :scenic, :owned_by_survey_season, :with_psd], parent: :image, class: 'ScenicImage'
    factory :scenic_image, traits: [:scenic, :owned_by_survey_season, :fake_image], parent: :image, class: 'ScenicImage'
    factory :scenic_image_with_gps, traits: [:scenic, :gps_small, :owned_by_survey], parent: :image, class: 'ScenicImage'

  end

  factory :metadata_file do
    metadata_file { fixture_file_upload(Rails.root.join('spec','photos','metadata.xls'))}
    association :metadata_owner, factory: :station
  end

  factory :field_note do
    field_note_file { fixture_file_upload(Rails.root.join('spec','photos','scanned_field_note.jpg'))}
    factory :field_note_transcribed_pdf do
      field_note_file { fixture_file_upload(Rails.root.join('spec','photos','transcribed_field_note.pdf'))}
    end
    visit
  end


  factory :camera do
    model "Hasselblad H3DII"
    sequence(:unit) {|n| "#{n}"}
    format "Digital"
  end

  factory :lens do
    brand "Hasselblad"
    focal_length '35mm'
    max_aperture 2.8
  end

  factory :participant, aliases: [:photographer, :fn_author] do
    sequence(:last_name) { |n| "Tom#{n}"}
    sequence(:given_names) { |n| "Arnold#{n}"}
  end

  factory :keyword do
    sequence(:keyword) { |n| "firestorm #{n}"}
  end

  factory :keyword_visit_association do
    keyword
    visit
  end

  factory :hiking_party do
    participant
    visit
  end


end

#allows to build attributes with associations
def build_attributes(*args)
  ba = FactoryGirl.build(*args).attributes.delete_if do |k, v|
    ["id", "created_at", "updated_at","fs_path","legacy_path"].member?(k)
  end
  af = FactoryGirl.attributes_for(*args)
  ba.symbolize_keys.merge(af).delete_if{|k,v|v.blank?}
end
def create_attributes(*args)
  af = FactoryGirl.attributes_for(*args)
  ba = FactoryGirl.create(*args,af).attributes.delete_if do |k, v|
    ["id", "created_at", "updated_at","fs_path","legacy_path"].member?(k)
  end
  ba.symbolize_keys.merge(af).delete_if{|k,v|v.blank?}
end
