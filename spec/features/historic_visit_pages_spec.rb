#!/bin/env ruby
# encoding: utf-8
require 'spec_helper'

feature "Historic Visit pages" do
  let(:historic_visit) { FactoryGirl.create(:historic_visit)}
  before(:each) { login_as_admin }
  describe "new historic visit" do
    let(:station) { FactoryGirl.create(:station) }
    describe "filling out the historic visit form (which is nested) with nothing" do
      it "creates a historic visit but does not create a historic capture"do
        visit new_station_historic_visit_path(station)
        click_button "Submit"
        expect(station.historic_visit).to be_present
        expect(station.historic_visit.historic_captures).to be_empty
      end
    end
  end

end
