require 'spec_helper'

feature "Surveyor pages" do
  let(:model) { Surveyor }
  describe "object creation" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("surveyor",:build) }
    include_examples "object creation"
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("surveyor",:create) }
    include_examples "object edit"
  end
  describe "object destroy" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:surveyor) }
    include_examples "object destroy"
  end
  describe "adding a child object" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:surveyor) }
    include_examples "adding a child object", Survey
  end
  describe "js tree interactions" do
    before(:each) { FactoryGirl.create(:surveyor) }
    include_examples "js tree"
  end
end
