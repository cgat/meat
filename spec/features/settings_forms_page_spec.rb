require 'spec_helper'

feature "Setting Form" do
  before(:each) { login_as_admin }
  scenario "form is submittable" do
    user = User.create(email: "new@example.com", password: "123password", password_confirmation: "123password")
    visit settings_path
    page.should have_content("General Settings")
    new_value = '/Some/Directory'
    fill_in "settings_form_mlp_library_local_root", with: new_value
    page.should have_content("Publisher Criteria Settings")
    page.should have_content("Update Criteria For Captures")
    page.should have_field("Do Not Update Published Captures")
    page.should have_field("No Captures")
    page.should have_field("All Captures")
    page.should have_field("Captures With Comparisons")
    page.should have_field("Captures With Mastered Comparisons")
    choose "All Captures"
    page.should have_content("Update Criteria For Historic Captures")
    page.should have_field("Do Not Update Published Historic Captures")
    page.should have_field("No Historic Captures")
    page.should have_field("All Historic Captures")
    page.should have_field("Historic Captures With Comparisons")
    page.should have_field("Historic Captures With Mastered Comparisons")
    choose "All Historic Captures"
    select "new@example.com", from: "settings_form_admin_users"
    click_button "Update Settings"
    expect(current_path).to eq "/"
    visit settings_path
    page.should have_field("settings_form_mlp_library_local_root", with: new_value)
    page.should have_checked_field("All Captures")
    page.should have_checked_field("All Historic Captures")
    page.should have_select("settings_form_admin_users", selected: ["new@example.com","test@example.com"])
  end

  scenario "form handles invalid data properly" do
    visit settings_path
    fill_in "settings_form_mlp_library_local_root", with: ""
    click_button("Update Settings")
    page.should have_content("Invalid Field")
  end
end
