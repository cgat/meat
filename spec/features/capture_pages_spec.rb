#!/bin/env ruby
# encoding: utf-8
require 'spec_helper'

feature "Capture pages" do
  let(:capture) { FactoryGirl.create(:capture)}
  shared_examples "selecting historic captures" do
    scenario "selecting one historic capture for comparison" do
      visit edit_capture_path(@capture.id)
      select @hcap1.display_name, from: "Historic captures"
      click_button "Submit"
      expect(page).to have_selector("a.brand", text: "Capture")
      expect(page).to have_no_selector("a.brand", text: "Edit Capture")
      expect(page).to have_text(@hcap1.display_name)
      expect(page).to have_no_text(@hcap2.display_name)
    end
    scenario "selecting two historic capture for comparison" do
      visit edit_capture_path(@capture.id)
      select @hcap1.display_name, from: "Historic captures"
      select @hcap2.display_name, from: "Historic captures"
      click_button "Submit"
      expect(page).to have_selector("a.brand", text: "Capture")
      expect(page).to have_no_selector("a.brand", text: "Edit Capture")
      expect(page).to have_text(@hcap1.display_name)
      expect(page).to have_text(@hcap2.display_name)
    end
  end
  describe "object creation" do
    let(:model) { Capture }
    let!(:attr_hash) { factory_to_attr_hash("capture",:create, reject_attrs: [:capture_owner_id, :capture_owner_type, :camera_id, :lens_id, :f_stop, :shutter_speed, :iso, :focal_length, :capture_datetime]) }
    before(:each) { login_as_admin }
    describe "with station parent" do
      let(:parent_model) { Station }
      include_examples "object creation"
      scenario "with a capture image", js: true do
        station = FactoryGirl.create(:station)
        visit new_station_capture_path(station.id)
        #click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        page.all(:css,'.image_state').last.select("RAW")
        click_button "Submit"
        expect(page).to have_text('color_repeat.jpg')
        expect(page).to have_selector("a.brand",text: "Capture")
      end
      scenario "clicking the Add Unsorted Capture link goes to the new capture page" do
        station = FactoryGirl.create(:station)
        visit station_path(station.id)
        click_link "Add Unsorted Capture"
        expect(page).to have_text("Add New Capture")
      end
    end
    describe "with visit parent" do
      let(:parent_model) { Visit }
      include_examples "object creation"
      scenario "with a capture image", js: true do
        visit = FactoryGirl.create(:visit)
        visit new_visit_capture_path(visit.id)
        #click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        page.all(:css,'.image_state').last.select("RAW")
        click_button "Submit"
        expect(page).to have_text('color_repeat.jpg')
        expect(page).to have_selector("a.brand",text: "Capture")
      end
      scenario "clicking the Add Unsorted Capture link goes to the new capture page" do
        visit = FactoryGirl.create(:visit)
        visit visit_path(visit.id)
        click_link "Add Unsorted Capture"
        expect(page).to have_text("Add New Capture")
      end
    end
    describe "with location parent" do
      let(:parent_model) { Location }
      scenario "with a capture image", js: true do
        location = FactoryGirl.create(:location)
        visit new_location_capture_path(location.id)
        #click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        page.all(:css,'.image_state').last.select("RAW")
        click_button "Submit"
        expect(page).to have_text('color_repeat.jpg')
        expect(page).to have_selector("a.brand",text: "Capture")
      end
      scenario "clicking the Add Capture link goes to the new capture page" do
        location = FactoryGirl.create(:location)
        visit location_path(location.id)
        click_link "Add Capture"
        expect(page).to have_text("Add New Capture")
      end
    end
    describe "with survey parent" do
      let(:parent_model) { Survey }
      scenario "with a capture image", js: true do
        survey = FactoryGirl.create(:survey)
        visit new_survey_capture_path(survey.id)
        #click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        page.all(:css,'.image_state').last.select("RAW")
        click_button "Submit"
        expect(page).to have_text('color_repeat.jpg')
        expect(page).to have_selector("a.brand",text: "Capture")
      end
      scenario "clicking the Add An Unsorted Capture link goes to the new capture page" do
        survey = FactoryGirl.create(:survey)
        visit survey_path(survey.id)
        click_link "Add Unsorted Capture"
        expect(page).to have_text("Add New Capture")
      end
    end
    describe "with survey season parent" do
      let(:parent_model) { SurveySeason }
      scenario "with a capture image", js: true do
        survey_season = FactoryGirl.create(:survey_season)
        visit new_survey_season_capture_path(survey_season.id)
        #click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        page.all(:css,'.image_state').last.select("RAW")
        click_button "Submit"
        expect(page).to have_text('color_repeat.jpg')
        expect(page).to have_selector("a.brand",text: "Capture")
      end
      scenario "clicking the Add An Unsorted Capture link goes to the new capture page" do
        survey_season = FactoryGirl.create(:survey_season)
        visit survey_season_path(survey_season.id)
        click_link "Add Unsorted Capture"
        expect(page).to have_text("Add New Capture")
      end
    end
    describe "with project parent" do
      let(:parent_model) { Project }
      scenario "with a capture image", js: true do
        project = FactoryGirl.create(:project)
        visit new_project_capture_path(project.id)
        #click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        page.all(:css,'.image_state').last.select("RAW")
        click_button "Submit"
        expect(page).to have_text('color_repeat.jpg')
        expect(page).to have_selector("a.brand",text: "Capture")
      end
      scenario "clicking the Add An Unsorted Capture link goes to the new capture page" do
        project = FactoryGirl.create(:project)
        visit project_path(project.id)
        click_link "Add Unsorted Capture"
        expect(page).to have_text("Add New Capture")
      end
    end
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    describe do
      let(:model) { Capture }
      let(:parent_model) { Location }
      let!(:attr_hash) { factory_to_attr_hash("capture",:create, reject_attrs: [:capture_owner_id, :capture_owner_type, :camera_id, :lens_id, :f_stop, :shutter_speed, :iso, :focal_length, :capture_datetime]) }
      include_examples "object edit"
    end
    describe "historic capture comparisons" do
      context "capture is owned by a location" do
        #making sure that hcaps and cap have same station
        before(:each) do
          station = FactoryGirl.create(:station)
          visit = FactoryGirl.create(:visit, station: station)
          location = FactoryGirl.create(:location, visit: visit)
          @capture = FactoryGirl.create(:capture, capture_owner: location)
          historic_visit = FactoryGirl.create(:historic_visit, station: station)
          @hcap1 = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
          @hcap2 = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
        end
        include_examples "selecting historic captures"
      end
      context "capture is owned by a visit" do
        before(:each) do
          station = FactoryGirl.create(:station)
          visit = FactoryGirl.create(:visit, station: station)
          @capture = FactoryGirl.create(:capture, capture_owner: visit)
          historic_visit = FactoryGirl.create(:historic_visit, station: station)
          @hcap1 = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
          @hcap2 = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
        end
        include_examples "selecting historic captures"
      end
      context "capture is owned by a station" do
        before(:each) do
          station = FactoryGirl.create(:station)
          @capture = FactoryGirl.create(:capture, capture_owner: station)
          historic_visit = FactoryGirl.create(:historic_visit, station: station)
          @hcap1 = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
          @hcap2 = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
        end
        include_examples "selecting historic captures"
      end
      context "capture is owned by a survey season" do
        let(:capture) {FactoryGirl.create(:unsorted_capture_survey_season)}
        scenario "cannot select historic captures when owned outside station" do
          visit edit_capture_path(capture.id)
          expect(page).to have_selector("a.brand", "Edit Capture")
          expect(page).to have_no_selector(".historic_captures_select")
        end
      end
      context "capture is owned by a survey" do
        let(:capture) {FactoryGirl.create(:unsorted_capture_survey)}
        scenario "cannot select historic captures when owned outside station" do
          visit edit_capture_path(capture.id)
          expect(page).to have_selector("a.brand", "Edit Capture")
          expect(page).to have_no_selector(".historic_captures_select")
        end
      end
    end
    describe "capture image edit" do
      let(:capture) {FactoryGirl.create(:capture_with_images)}
      describe "image state" do
        ["RAW","MASTER","INTERIM","MISC","GRIDDED"].each do |image_state|
          scenario "changed to #{image_state}" do
            visit edit_capture_path(capture.id)
            ci = capture.capture_images.first
            within "##{ci.image_secure_token}" do
              page.find('.image_state').select(image_state)
            end
            click_button "Submit"
            expect(page).to have_selector("a.brand", text: "Capture")
            ci.reload
            expect(ci.image_state).to eq(image_state)
          end
        end
      end
      describe "delete" do
        scenario "delete an image using the checkbox" do
          visit edit_capture_path(capture.id)
          ci = capture.capture_images.first
          within "##{ci.image_secure_token}" do
            page.find('.delete_check_box').set(true)
          end
          click_button "Submit"
          expect(page).to have_selector("a.brand","Capture")
          expect(CaptureImage.find_by_id(ci.id)).to_not be_present
        end
      end
      scenario "add a new image when images already exist", js: true, driver: :selenium do
        visit edit_capture_path(capture.id)
        expect {
          click_link "Add capture image"
          page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
          page.all(:css,'.image_state').last.select("RAW")
          click_button "Submit"
        }.to change{capture.capture_images.count}.by(1)
      end
      scenario "add a new image, but don't set image state", js: true do
        visit edit_capture_path(capture.id)
        click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        click_button "Submit"
        expect(page).to have_text("Invalid")
      end
      scenario "add a new image then remove it", js: true do
        visit edit_capture_path(capture.id)
        expect {
          click_link "Add capture image"
          page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
          page.all(:css,'.image_state').last.select("RAW")
          click_link "Remove"
          click_button "Submit"
        }.to_not change{capture.capture_images.count}
      end
    end
  end


end
