require "spec_helper.rb"
feature "Map Navigation" do
  before(:each) { visit map_path }
  it "should have a map link" do
    page.should have_link("map_link")
  end
end
