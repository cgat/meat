require 'spec_helper'

# common to all
# -there exists a filter menu in the navigation pane header
# -the filter menu drops down to show filter options
# -clicking a filter will show an ordered list of stations related to that filter
# -refreshing the browser will keep this state
shared_examples_for "filters" do |filter_name|
  it "is shown in the filter menu", js: true do
    visit root_path
    page.should have_xpath("//option[@value='#{filter_path(filter_name)}']")
  end
  it "lists the filtered stations in the nav pane", js: true do
    #sets the current tree object to station1
    visit station_path(station1)
    visit filter_path(filter_name)
    page.should have_link(station1.display_name)
    # within("#Station_#{station1.id}") do
    #   page.should have_xpath("//ins[@background-image='url(http://envi-mountain-0003.envi.uvic.ca/assets/folder_red.png)']")
    # end
  end
end
feature "Navigation Tree Filters" do

  describe 'has_hcaps_with_missing_comparisons' do
    let!(:station1) do
      hcap = FactoryGirl.create(:historic_capture)
      station = hcap.station
      cap = FactoryGirl.create(:capture, capture_owner: station)
      station
    end
    include_examples "filters", :has_hcaps_with_missing_comparisons
  end

  describe "has_caps_with_missing_comparisons" do
    let!(:station1) do
      cap = FactoryGirl.create(:capture)
      station = cap.station
      station
    end
    include_examples "filters", :has_caps_with_missing_comparisons
  end

  describe "has_caps_without_capture_images" do
    let!(:station1) do
      cap = FactoryGirl.create(:capture)
      station = cap.station
      station
    end
    include_examples "filters", :has_caps_without_capture_images
  end

  describe "has_hcaps_without_capture_images" do
    let!(:station1) do
      hcap = FactoryGirl.create(:historic_capture)
      station = hcap.station
      station
    end
    include_examples "filters", :has_hcaps_without_capture_images
  end

  describe "has_visits_with_not_transcribed_but_with_fns" do
    let!(:station1) do
      visit = FactoryGirl.create(:visit_date_only, :with_field_notes)
      station = visit.station
      station
    end
    include_examples "filters", :has_visits_with_not_transcribed_but_with_fns
  end

  describe "has_visits_no_scanned_fns" do
    let!(:station1) do
      visit = FactoryGirl.create(:visit_date_only)
      station = visit.station
      station
    end
    include_examples "filters", :has_visits_no_scanned_fns
  end

  describe "has_captures_without_azimuths" do
    let!(:station1) do
      cap = FactoryGirl.create(:capture, azimuth: nil)
      station = cap.station
      station
    end
    include_examples "filters", :has_captures_without_azimuths
  end

  describe "grouped_state" do
    let!(:station1) do
      hcap = FactoryGirl.create(:historic_capture)
      station = hcap.station
      station
    end
    include_examples "filters", :grouped_state
  end

  describe "located_state" do
    let!(:station1) do
      hcap = FactoryGirl.create(:historic_capture)
      station = hcap.station
      station.lat = 90
      station.long = 90
      station.save!
      station
    end
    include_examples "filters", :located_state
  end

  describe "repeated_state" do
    let!(:station1) do
      hcap = FactoryGirl.create(:historic_capture)
      station = hcap.station
      cap = FactoryGirl.create(:capture, capture_owner: station)
      cap.add_comparison(hcap)
      station.lat = 90
      station.long = 90
      station.save!
      station
    end
    include_examples "filters", :repeated_state
  end

  describe "mastered_state" do
    let!(:station1) do
      cap = FactoryGirl.create(:capture, :mastered)
      station = cap.station
      station
    end
    include_examples "filters", :mastered_state
  end

end
