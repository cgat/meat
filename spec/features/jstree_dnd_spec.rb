# require 'spec_helper'

# feature "JS tree drag and drop" do
#   before(:each) { login_as_admin }
#   shared_examples "dragging and dropping object allowed" do |parent_association, original_parent, new_parent, obj_type_to_move|
#     it "can move a #{obj_type_to_move.to_s} from a #{original_parent.to_s} to a #{new_parent.to_s}", js: true, driver: :selenium do
#       original_parent_object = FactoryGirl.create(original_parent)
#       subject = FactoryGirl.create(obj_type_to_move, parent_association => original_parent_object)
#       new_parent_object = FactoryGirl.create(new_parent)
#       visit "/#{subject.class.name.underscore.pluralize}/#{subject.id}"
#       open_node_parents(new_parent_object)
#       #these have_selectors are required to ensure that the execute_script method
#       #actually has the DOM elements to execute against, otherwise we have a race condition
#       expect(page).to have_selector("##{subject.class.name}_#{subject.id}")
#       expect(page).to have_selector("##{new_parent.to_s.camelcase}_#{new_parent_object.id}")
#       page.execute_script("$('#treeViewDiv').jstree('move_node','##{subject.class.name}_#{subject.id}','##{new_parent.to_s.camelcase}_#{new_parent_object.id}')")
#       #this forces capybara to wait for things to end
#       page.find("##{new_parent.to_s.camelcase}_#{new_parent_object.id} ##{subject.class.name}_#{subject.id}")
#       expect(page).to have_selector('.flash_success', text: "#{subject.class.name} successfully moved!")
#       subject.reload
#       expect(subject.send(parent_association)).to eq(new_parent_object)
#     end
#   end
#   shared_examples "dragging and dropping object disallowed" do |parent_association, original_parent, new_parent, obj_type_to_move|
#     it "can move a #{obj_type_to_move.to_s} from a #{original_parent.to_s} to a #{new_parent.to_s}", js: true do
#       original_parent_object = FactoryGirl.create(original_parent)
#       subject = FactoryGirl.create(obj_type_to_move, parent_association => original_parent_object)
#       new_parent_object = FactoryGirl.create(new_parent)
#       visit "/#{subject.class.name.underscore.pluralize}/#{subject.id}"
#       open_node_parents(new_parent_object)
#       expect(page).to have_selector("##{subject.class.name}_#{subject.id}")
#       expect(page).to have_selector("##{new_parent.to_s.camelcase}_#{new_parent_object.id}")
#       page.execute_script("$('#treeViewDiv').jstree('move_node','##{subject.class.name}_#{subject.id}','##{new_parent.to_s.camelcase}_#{new_parent_object.id}')")
#       expect(page).to have_no_selector('.flash_success', text: "#{subject.class.name} successfully moved!")
#       subject.reload
#       expect(subject.send(parent_association)).to eq(original_parent_object)
#     end
#   end
#   # [:location,:visit,:station,:survey_season,:survey,:project].each do |original_parent|
#   #     [:location,:visit,:station,:survey_season,:survey,:project].each do |new_parent|
#   describe "with a capture" do
#     [:location].each do |original_parent|
#       [:station].each do |new_parent|
#         include_examples "dragging and dropping object allowed", :capture_owner, original_parent, new_parent, :capture
#       end
#     end
#     include_examples "dragging and dropping object disallowed", :capture_owner, :location, :surveyor, :capture

#   end
#   describe "with a historic capture" do
#     [:historic_visit,:survey_season,:survey,:project].each do |original_parent|
#       [:historic_visit,:survey_season,:survey,:project].each do |new_parent|
#         include_examples "dragging and dropping object allowed", :capture_owner, original_parent, new_parent, :historic_capture
#       end
#     end
#     [:visit,:location,:station].each do |new_parent|
#       include_examples "dragging and dropping object disallowed", :capture_owner, :historic_visit, new_parent, :historic_capture
#     end
#   end

#   it "can perform multi select moves" do
#     #drag and droping with jstree+capybara is unpredictable, so not sure how to do multiselect test.
#   end
# end
