require 'spec_helper'

feature "Project pages" do
  let(:model) { Project }
  describe "object creation" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("project",:build) }
    include_examples "object creation"
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("project",:create) }
    include_examples "object edit"
  end
  describe "object destroy" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:project) }
    include_examples "object destroy"
  end
  describe "adding a child object" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:project) }
    include_examples "adding a child object", Station
  end
  describe "js tree interactions" do
    before(:each) { FactoryGirl.create(:project) }
    include_examples "js tree"
  end
  describe "unmanaged metadata" do
    before(:each) { login_as_admin }
    include_examples "unmanaged metadata folder", "project"
  end
end
