require 'spec_helper'

feature "Visit pages" do
  let(:model) { Visit }
  let(:parent_model) { Station }
  describe "object show" do

  end
  describe "object creation" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("visit",:build, reject_attrs: [:station_id]) }
    include_examples "object creation"
    describe "add unsorted capture" do
      let(:model) { Capture }
      let(:parent_model) { Visit }
      let!(:attr_hash) { factory_to_attr_hash("capture",:create, reject_attrs: [:capture_owner_id, :capture_owner_type, :camera_id, :lens_id, :f_stop, :shutter_speed, :iso, :focal_length, :capture_datetime]) }
      include_examples "object creation"
      describe do
        scenario "with a capture image", js: true do
          visit = FactoryGirl.create(:visit)
          visit new_visit_capture_path(visit.id)
          #click_link "Add capture image"
          page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
          page.all(:css,'.image_state').last.select("RAW")
          page.first(:xpath, "//input[@value='Submit']").click
          expect(page).to have_text('color_repeat.jpg')
          expect(page).to have_selector('a.brand',text: "Capture")
        end
        scenario "clicking the Add Unsorted Capture link goes to the new capture page" do
          visit = FactoryGirl.create(:visit)
          visit visit_path(visit.id)
          click_link "Add Unsorted Capture"
          expect(page).to have_text("Add New Capture")
        end
      end
    end
    context do
      before(:each) do
        visit_new_and_set_date
      end
      scenario "submit with blank time start and end time" do
        select "", from: "visit_start_time_4i"
        select "", from: "visit_start_time_5i"
        select "", from: "visit_finish_time_4i"
        select "", from: "visit_finish_time_5i"
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_text("Invalid")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Add New")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(page).to have_no_text("4:20") #the default from visit_new_and_set_date method should not be there
        expect(page).to have_no_text("16:20")
        expect(page).to have_no_text("00:00")
      end
      scenario "with nested location" do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:location_narrative,Visit,[[Location,0]]), with: "It was a good location"
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_text("Invalid")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Add New")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.count).to eq(1)
        expect(Visit.last.locations.first.captures).to be_empty
      end
      scenario "with nested location and capture" do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:location_narrative,Visit,[[Location,0]]), with: "It was a good location"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_text("Invalid")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Add New")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(page).to have_text("FNPHOTOREF1")
        expect(Visit.last.locations.first.captures.count).to eq(1)
      end
      scenario "with nested location, capture, and capture image" do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:location_narrative,Visit,[[Location,0]]), with: "It was a good location"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        attach_file get_form_node_name(:image,Visit,[[Location,0],[Capture,0],[CaptureImage,0]]), Rails.root.join("spec","photos","color_repeat.jpg")
        select "RAW", from: get_form_node_name(:image_state,Visit,[[Location,0],[Capture,0],[CaptureImage,0]])
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_text("Invalid")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.first.capture_images.count).to eq(1)
      end
      scenario "remove location", js: true do
        click_link "Remove this location"
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.count).to eq(0)
      end
      scenario "fill in location, then remove", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        click_link "Remove this location"
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.count).to eq(0)
      end
      scenario "add another location, fill in, sumbit", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        click_link "Add location"
        location_fieldsets = all('.location')
        expect(location_fieldsets.count).to eq(2)
        location_fieldsets[1].fill_in("Location identity", with: "123")
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.count).to eq(2)
      end
      scenario "remove capture", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        click_link "Remove this capture"
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.count).to eq(0)
      end
      scenario "fill in capture, then remove", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        click_link "Remove this capture"
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.count).to eq(0)
      end
      scenario "add another capture, leave blank, submit", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        all('a', text: /Add capture$/).first.click
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.count).to eq(1)
      end
      scenario "add another capture, fill in, submit", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        all('a', text: /Add capture$/).first.click
        capture_fieldsets = all('.capture')
        expect(capture_fieldsets.count).to eq(2)
        capture_fieldsets[1].fill_in("Field note photo reference", with: "AJDJS")
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.count).to eq(2)
      end
      scenario "remove capture image", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        all('.remove_fields', text: /Remove$/)[0].click
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.first.capture_images.count).to eq(0)
      end
      scenario "add capture image with no capture info filled in" do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        attach_file get_form_node_name(:image,Visit,[[Location,0],[Capture,0],[CaptureImage,0]]), Rails.root.join("spec","photos","color_repeat.jpg")
        select "RAW", from: get_form_node_name(:image_state,Visit,[[Location,0],[Capture,0],[CaptureImage,0]])
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.first.capture_images.count).to eq(1)
      end
      scenario "add another capture image", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        attach_file get_form_node_name(:image,Visit,[[Location,0],[Capture,0],[CaptureImage,0]]), Rails.root.join("spec","photos","color_repeat.jpg")
        select "RAW", from: get_form_node_name(:image_state,Visit,[[Location,0],[Capture,0],[CaptureImage,0]])
        click_link "Add capture image"
        within 'div.capture_image' do
          ci_fieldsets = all('fieldset')
          expect(ci_fieldsets.count).to eq(2)
          ci_fieldsets[1].all('input')[0].set(Rails.root.join("spec","photos","color_repeat.jpg"))
          ci_fieldsets[1].find('select').find(:option,"MASTER").select_option
        end
        #this fixes a bug in the chrome driver where the submit button is unclickable because it is disabled (even though it is not)
        click_link("Add capture image")
        page.first(:xpath, "//input[@name='commit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.first.capture_images.count).to eq(2)
      end
      scenario "add another capture image, fill in, then remove", js: true do
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
        attach_file get_form_node_name(:image,Visit,[[Location,0],[Capture,0],[CaptureImage,0]]), Rails.root.join("spec","photos","color_repeat.jpg")
        select "RAW", from: get_form_node_name(:image_state,Visit,[[Location,0],[Capture,0],[CaptureImage,0]])
        click_link "Add capture image"
        within 'div.capture_image' do
          ci_fieldsets = all('fieldset')
          expect(ci_fieldsets.count).to eq(2)
          ci_fieldsets[1].all('input')[0].set(Rails.root.join("spec","photos","color_repeat.jpg"))
          ci_fieldsets[1].find('select').find(:option,"MASTER").select_option
          all('.remove_fields', /Remove$/)[1].click
        end
        page.first(:xpath, "//input[@value='Submit']").click
        expect(page).to have_no_selector("a.brand", text: "Add New Visit")
        expect(page).to have_selector("a.brand", text: "Visit")
        expect(page).to have_no_selector("a.brand", text: "Edit")
        expect(Visit.last.locations.first.captures.first.capture_images.count).to eq(1)
      end
      scenario "match capture image remote"
      describe "keywords" do
        scenario "is populated with full list"
        scenario "is a sorted list"
      end
      describe "hiking party" do
        scenario "is populated with full list of participants"
        scenario "is a sorted list by first name"
      end
      describe "field notes" do
        scenario "is a field in the visit form" do
          expect(page).to have_field("Field note files")
          expect(page).to have_selector("#visit_field_notes")
        end
        # Test not working. File gets attached, but the form submissions doesn't seem to wait
        # for the file to upload(?). Multiple doesn't work
        # scenario "is able to accept a file", js: true, driver: :selenium do
        #   #Rails.root.join("spec","photos", "scanned_field_note.jpg").to_s could not get multiple file tests working on rails 4
        #   attach_file "Field note files", Rails.root.join("spec","photos", "transcribed_field_note.pdf").to_s
        #   click_button("Submit")
        #   #page.first(:xpath, "//input[@value='Submit']").click
        #   byebug
        #   expect(page).to have_selector("a",text: "transcribed_field_note.pdf")
        # end
      end
      describe "metadata files" do
        include_examples "metadata file in create page"
      end
    end
    describe "historic captures field in capture", js: true do
      let(:hvisit_related) {hv_related = FactoryGirl.create(:historic_visit, station: Station.first) }
      let(:hvisit_other) { hv_other = FactoryGirl.create(:historic_visit) }
      let!(:hcapture_related1) { FactoryGirl.create(:historic_capture, fn_photo_reference: "ZZZ", capture_owner: hvisit_related) }
      let!(:hcapture_related2) { FactoryGirl.create(:historic_capture, fn_photo_reference: "AAA", capture_owner: hvisit_related) }
      let!(:hcapture_other) { FactoryGirl.create(:historic_capture, capture_owner: hvisit_other)}
      before(:each) do
        visit_new_and_set_date
        fill_in get_form_node_name(:location_identity,Visit,[[Location,0]]), with: "1"
        fill_in get_form_node_name(:fn_photo_reference,Visit,[[Location,0],[Capture,0]]), with: "FNPHOTOREF1"
      end
      scenario "is populated with the appropriate historic captures" do
        within find(:select, "Historic captures") do
          list = all('option').map(&:text)
          expect(list).to include(hcapture_related1.display_name)
          expect(list).to include(hcapture_related2.display_name)
          expect(list).to_not include(hcapture_other.display_name)
        end
      end
      scenario "is a sorted list", :skip do
        within find(:select, "Historic captures") do
          list = all('option').map(&:text)
          sorted_list = [hcapture_related1.display_name, hcapture_related2.display_name].sort
          expect(list).to eq(sorted_list)
        end
      end

      scenario "add another capture, the historic captures field is populated with the appropriate historic captures" do
        all('a', text: /Add capture$/).first.click
        within all(:select, "Historic captures")[1] do
          list = all('option').map(&:text)
          expect(list).to include(hcapture_related1.display_name)
          expect(list).to include(hcapture_related2.display_name)
          expect(list).to_not include(hcapture_other.display_name)
        end
      end
      scenario "select two historic captures", js: true, driver: :selenium do
        select hcapture_related1.display_name, from: "Historic captures"
        select hcapture_related2.display_name, from: "Historic captures"
        click_button("Submit")
        Visit.last.locations.first.captures.first.historic_captures.map(&:display_name).should =~ [hcapture_related1.display_name, hcapture_related2.display_name]
      end
    end
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("visit",:create, reject_attrs: [:station_id]) }
    include_examples "object edit"
    describe "Field notes" do
      before(:each) do
        @visit = FactoryGirl.create(:visit_with_field_notes)
        visit edit_visit_path(@visit.id)
      end
      scenario "can delete existing field note files" do
        within '#field_note_fields_table' do
          page.first('.delete_check_box').set(true)
        end
        expect { page.first(:xpath, "//input[@value='Submit']").click }.to change{@visit.field_notes.count}.by(-1)
      end
      scenario "can delete and add field note files at the same time" do
        attach_file "Field note files", [Rails.root.join("spec","photos", "transcribed_field_note.pdf").to_s]
        within '#field_note_fields_table' do
          page.first('.delete_check_box').set(true)
        end
        expect { page.first(:xpath, "//input[@value='Submit']").click }.to change{@visit.field_notes.count}.by(0)
      end
    end
    describe "metadata files" do
      subject { FactoryGirl.create(:visit_with_metadata) }
      before(:each) do
        #@visit = FactoryGirl.create(:visit_with_metadata)
        visit edit_visit_path(subject.id)
      end
      include_examples "metadata file in edit page"
    end

  end
  describe "object destroy" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:visit) }
    include_examples "object destroy"
  end
  describe "js tree interactions" do
    before(:each) { FactoryGirl.create(:visit) }
    include_examples "js tree"
  end
end

def visit_new_and_set_date
  stn = Station.first
  visit new_station_visit_path(stn.id)
  select "2005", from: "visit_date_1i"
  select "June", from: "visit_date_2i"
  select "13", from: "visit_date_3i"
  select "04", from: "visit_start_time_4i"
  select "20", from: "visit_start_time_5i"
  select "16", from: "visit_finish_time_4i"
  select "20", from: "visit_finish_time_5i"
end
