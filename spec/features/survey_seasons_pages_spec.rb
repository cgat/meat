require 'spec_helper'

feature "Survey Season pages" do
  let(:model) { SurveySeason }
  let(:parent_model) { Survey }
  describe "object creation" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("survey_season",:build, reject_attrs: [:survey_id]) }
    include_examples "object creation"
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("survey_season",:create, reject_attrs: [:survey_id]) }
    include_examples "object edit"
  end
  describe "object destory" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:survey_season) }
    include_examples "object destroy"
  end
  describe "adding a child object" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:survey_season) }
    include_examples "adding a child object", Station
  end
  describe "js tree interactions" do
    before(:each) { FactoryGirl.create(:survey_season) }
    include_examples "js tree"
  end
  describe "unmanaged metadata" do
    before(:each) { login_as_admin }
    include_examples "unmanaged metadata folder", "survey_season"
  end
end
