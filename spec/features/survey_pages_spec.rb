require 'spec_helper'

feature "Surveyor pages" do
  let(:model) { Survey }
  let(:parent_model) { Surveyor }
  describe "object creation" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("survey",:build, reject_attrs: [:surveyor_id]) }
    include_examples "object creation"
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("survey",:create, reject_attrs: [:surveyor_id]) }
    include_examples "object edit"
  end
  describe "object destroy" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:survey) }
    include_examples "object destroy"
  end
  describe "adding a child object" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:survey) }
    include_examples "adding a child object", SurveySeason
  end
  describe "js tree interactions" do
    before(:each) { FactoryGirl.create(:survey) }
    include_examples "js tree"
  end
  describe "unmanaged metadata" do
    before(:each) { login_as_admin }
    include_examples "unmanaged metadata folder", "survey"
  end
end
