require 'spec_helper'


feature "Merging", driver: :selenium do
  before(:each) { login_as_admin }
  describe "captures" do
    it "can merge a capture from a field note based capture with a image based capture", js: true do
      tree_capture = FactoryGirl.create(:capture_from_fn)
      data_pane_capture = FactoryGirl.create(:capture_from_img)
      all_cap_imgs = tree_capture.capture_images+data_pane_capture.capture_images
      all_comparisons = tree_capture.historic_captures+data_pane_capture.historic_captures
      set_speed(:medium)
      visit "/captures/#{data_pane_capture.id}"
      open_node_parents(tree_capture)
      draggable = page.find_by_id("Capture_#{tree_capture.id}").first("a")
      droppable = page.find(".capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_capture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#Capture_#{tree_capture.id}")
      expect(page).to have_merged_fields(tree_capture,data_pane_capture)
      expect(data_pane_capture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_capture.historic_captures).to match_array(all_comparisons)
    end
    it "can merge an image based capture with a field note based capture", js: true do
      tree_capture = FactoryGirl.create(:capture_from_img)
      data_pane_capture = FactoryGirl.create(:capture_from_fn)
      all_cap_imgs = tree_capture.capture_images+data_pane_capture.capture_images
      all_comparisons = tree_capture.historic_captures+data_pane_capture.historic_captures
      set_speed(:medium)
      visit "/captures/#{data_pane_capture.id}"
      open_node_parents(tree_capture)
      draggable = page.find_by_id("Capture_#{tree_capture.id}").first("a")
      droppable = page.find(".capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_capture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#Capture_#{tree_capture.id}")
      expect(page).to have_merged_fields(tree_capture,data_pane_capture)
      expect(data_pane_capture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_capture.historic_captures).to match_array(all_comparisons)
    end

    it "cannot merge two field note based captures", js: true do
      tree_capture = FactoryGirl.create(:capture_from_fn)
      data_pane_capture = FactoryGirl.create(:capture_from_fn)
      set_speed(:medium)
      visit "/captures/#{data_pane_capture.id}"
      open_node_parents(tree_capture)
      draggable = page.find_by_id("Capture_#{tree_capture.id}").first("a")
      droppable = page.find(".capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "These two #{tree_capture.class.name.downcase.pluralize} are not mergeable.")
      expect(page).to have_selector("#Capture_#{tree_capture.id}")
      expect(data_pane_capture.reload).to be_present
      expect(tree_capture.reload).to be_present
    end

    it "can merge two image based captures", js: true do
      tree_capture = FactoryGirl.create(:capture_from_img, :without_camera_info)
      data_pane_capture = FactoryGirl.create(:capture_from_img, :without_camera_info)
      all_cap_imgs = tree_capture.capture_images+data_pane_capture.capture_images
      all_comparisons = tree_capture.historic_captures+data_pane_capture.historic_captures
      set_speed(:medium)
      visit "/captures/#{data_pane_capture.id}"
      open_node_parents(tree_capture)
      draggable = page.find_by_id("Capture_#{tree_capture.id}").first("a")
      droppable = page.find(".capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_capture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#Capture_#{tree_capture.id}")
      expect(page).to have_merged_fields(tree_capture,data_pane_capture)
      expect(data_pane_capture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_capture.historic_captures).to match_array(all_comparisons)
    end

    it "will merge comparison associations", js: true do
      tree_capture = FactoryGirl.create(:capture_from_img, :without_camera_info)
      tree_capture.add_comparison(FactoryGirl.create(:historic_capture))
      data_pane_capture = FactoryGirl.create(:capture_from_img, :without_camera_info)
      data_pane_capture.add_comparison(FactoryGirl.create(:historic_capture))
      all_cap_imgs = tree_capture.capture_images+data_pane_capture.capture_images
      all_comparisons = tree_capture.historic_captures+data_pane_capture.historic_captures
      set_speed(:medium)
      visit "/captures/#{data_pane_capture.id}"
      open_node_parents(tree_capture)
      draggable = page.find_by_id("Capture_#{tree_capture.id}").first("a")
      droppable = page.find(".capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_capture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#Capture_#{tree_capture.id}")
      expect(page).to have_merged_fields(tree_capture,data_pane_capture)
      expect(data_pane_capture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_capture.historic_captures).to match_array(all_comparisons)
    end

  end

  describe "historic captures" do
    it "can merge a historic capture from a field note based historic capture with a image based historic capture", js: true do
      tree_hcapture = FactoryGirl.create(:hcapture_from_fn)
      data_pane_hcapture = FactoryGirl.create(:hcapture_from_img)
      all_cap_imgs = tree_hcapture.capture_images+data_pane_hcapture.capture_images
      all_comparisons = tree_hcapture.captures+data_pane_hcapture.captures
      set_speed(:medium)
      visit "/historic_captures/#{data_pane_hcapture.id}"
      open_node_parents(tree_hcapture)
      draggable = page.find_by_id("HistoricCapture_#{tree_hcapture.id}").first("a")
      droppable = page.find(".historic_capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_hcapture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#HistoricCapture_#{tree_hcapture.id}")
      expect(page).to have_merged_fields(tree_hcapture,data_pane_hcapture)
      expect(data_pane_hcapture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_hcapture.captures).to match_array(all_comparisons)
    end
    it "can merge an image based capture with a field note based capture", js: true do
      tree_hcapture = FactoryGirl.create(:hcapture_from_img)
      data_pane_hcapture = FactoryGirl.create(:hcapture_from_fn)
      all_cap_imgs = tree_hcapture.capture_images+data_pane_hcapture.capture_images
      all_comparisons = tree_hcapture.captures+data_pane_hcapture.captures
      set_speed(:medium)
      visit "/historic_captures/#{data_pane_hcapture.id}"
      open_node_parents(tree_hcapture)
      draggable = page.find_by_id("HistoricCapture_#{tree_hcapture.id}").first("a")
      droppable = page.find(".historic_capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_hcapture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#HistoricCapture_#{tree_hcapture.id}")
      expect(page).to have_merged_fields(tree_hcapture,data_pane_hcapture)
      expect(data_pane_hcapture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_hcapture.captures).to match_array(all_comparisons)
    end

    it "cannot merge two field note based captures", js: true do
      tree_hcapture = FactoryGirl.create(:hcapture_from_fn)
      data_pane_hcapture = FactoryGirl.create(:hcapture_from_fn)
      set_speed(:medium)
      visit "/historic_captures/#{data_pane_hcapture.id}"
      open_node_parents(tree_hcapture)
      draggable = page.find_by_id("HistoricCapture_#{tree_hcapture.id}").first("a")
      droppable = page.find(".historic_capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "These two #{tree_hcapture.class.name.titleize.downcase.pluralize} are not mergeable.")
      expect(page).to have_selector("#HistoricCapture_#{tree_hcapture.id}")
      expect(data_pane_hcapture.reload).to be_present
      expect(tree_hcapture.reload).to be_present
    end

    it "can merge two image based captures", js: true do
      tree_hcapture = FactoryGirl.create(:hcapture_from_img, :without_camera_info)
      data_pane_hcapture = FactoryGirl.create(:hcapture_from_img, :without_camera_info)
      all_cap_imgs = tree_hcapture.capture_images+data_pane_hcapture.capture_images
      all_comparisons = tree_hcapture.captures+data_pane_hcapture.captures
      set_speed(:medium)
      visit "/historic_captures/#{data_pane_hcapture.id}"
      open_node_parents(tree_hcapture)
      draggable = page.find_by_id("HistoricCapture_#{tree_hcapture.id}").first("a")
      droppable = page.find(".historic_capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_hcapture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#HistoricCapture_#{tree_hcapture.id}")
      expect(page).to have_merged_fields(tree_hcapture,data_pane_hcapture)
      expect(data_pane_hcapture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_hcapture.captures).to match_array(all_comparisons)
    end

    it "will merge comparison associations", js: true do
      tree_hcapture = FactoryGirl.create(:hcapture_from_img, :without_camera_info)
      tree_hcapture.add_comparison(FactoryGirl.create(:historic_capture))
      data_pane_hcapture = FactoryGirl.create(:hcapture_from_img, :without_camera_info)
      data_pane_hcapture.add_comparison(FactoryGirl.create(:historic_capture))
      all_cap_imgs = tree_hcapture.capture_images+data_pane_hcapture.capture_images
      all_comparisons = tree_hcapture.captures+data_pane_hcapture.captures
      set_speed(:medium)
      visit "/historic_captures/#{data_pane_hcapture.id}"
      open_node_parents(tree_hcapture)
      draggable = page.find_by_id("HistoricCapture_#{tree_hcapture.id}").first("a")
      droppable = page.find(".historic_capture_well")
      accept_js_confirm {
        draggable.drag_to(droppable)
      }
      expect(page).to have_selector('.jGrowl-message', text: "#{tree_hcapture.class.name.titleize.pluralize} successfully merged!")
      expect(page).to have_no_selector("#HistoricCapture_#{tree_hcapture.id}")
      expect(page).to have_merged_fields(tree_hcapture,data_pane_hcapture)
      expect(data_pane_hcapture.reload.capture_images).to match_array(all_cap_imgs)
      expect(data_pane_hcapture.captures).to match_array(all_comparisons)
    end
  end
end

#the proper way of doing this is to use capybara's accept_confirm. However,
#this does not seem to work with drag_to. In selenium, an unexpected
#alert exception is thrown before have have the chance to handle it.
#This method just monkey patches the confirm method to return true.
def accept_js_confirm
  page.evaluate_script 'window.original_confirm_function = window.confirm;'
  page.evaluate_script 'window.confirm = function(msg) { return true; }'
  yield
  page.evaluate_script 'window.confirm = window.original_confirm_function;'
end
