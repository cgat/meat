require 'spec_helper'

feature "CaptureImage pages" do
  let(:capture_image) { FactoryGirl.create(:repeat_image_color_jpeg) }
  after (:each) do
    VIPS::thread_shutdown()
  end
  describe "show" do
    context "mediumn version is missing" do
      before(:each) do
        path_to_medium = capture_image.reload.image.medium.file.path
        FileUtils.rm(path_to_medium)
        visit capture_image_path(capture_image.id)
      end
      scenario "default image should be shown" do
        expect(page).to have_xpath("//img[@src=\"#{capture_image.image.medium.default_url}\"]")
      end
    end
    context "image file is missing" do
      before(:each) do
        path_to_image = capture_image.reload.image.file.path
        FileUtils.rm(path_to_image)
        visit capture_image_path(capture_image.id)
      end
      scenario "operations related to image should be disabled" do
        expect(page).to have_no_text("Download")
        expect(page).to have_no_selector("a", text: "Download")
        expect(page).to have_css(".alert-error")
      end
    end
  end

  describe "create a new capture image page" do
    let(:capture) { FactoryGirl.create(:capture) }
    before (:each) do
      login_as_admin
      visit edit_capture_path(capture)
    end

    scenario "with remote true", js: true, :driver => :selenium do
      click_link "Add capture image"
      first(:css, '.image').set(Rails.root.join("spec","photos","color_repeat.jpg"))
      first(:css, '.image_state').select("RAW")
      first(:css, '.remote').set(true)
      page.first(:xpath, "//input[@value='Submit']").click

      expect(capture.reload.capture_images.first.remote).to be_truthy
    end
  end
  describe "edit a capture image page" do
    scenario "changing remote from false to true" do
      capture = capture_image.captureable
      login_as_admin
      visit edit_capture_path(capture)
      first(:css, '.remote').set(true)
      page.first(:xpath, "//input[@value='Submit']").click
      expect(capture.reload.capture_images.first.remote).to be_truthy
    end
    scenario "changing remote from true to false" do
      capture_image.reload.remote = true;
      capture = capture_image.captureable
      login_as_admin
      visit edit_capture_path(capture)
      first(:css, '.remote').set(false)
      page.first(:xpath, "//input[@value='Submit']").click
      expect(capture.reload.capture_images.first.remote).to be_falsey
    end
  end
  # end

  describe "object destroy" do
    let(:model) { CaptureImage }
    let(:parent_model) { Catpure }
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:repeat_image_color_jpeg) }
    include_examples "object destroy"
  end

  describe "js tree interactions" do
    scenario "the capture associated with the capture image in the data-pane is selected in the tree", js: true do
      visit capture_image_path(capture_image.id)
      using_wait_time(20) { expect(page).to have_selector("li#Capture_#{capture_image.captureable.id.to_s}")  }
    end
    scenario "the parent folder of the capture image's capture is open", js: true do
      visit capture_image_path(capture_image.id)
      parent = capture_image.captureable.capture_owner
      using_wait_time(20) { expect(page).to have_selector("##{parent.class.name}_#{parent.id}")}
      expect(page.find("##{parent.class.name}_#{parent.id}")[:class]).to include('jstree-open')
    end
  end
end
