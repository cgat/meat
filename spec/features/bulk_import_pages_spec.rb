require 'spec_helper'

shared_examples "bulk import" do |cap_type|
  describe do
    before(:each) do
      visit "/#{subject.class.name.underscore.pluralize}/#{subject.id}"
      click_link "Bulk #{cap_type.titleize} Import"
    end
    scenario "multiple files to #{cap_type.pluralize}" do
      attach_file "Images", [Rails.root.join("spec","photos", "color_repeat.jpg").to_s,Rails.root.join("spec","photos", "bw_historic.jpg").to_s]
      select "RAW", from: "Image state"
      click_button "Submit"
      expect(page).to have_selector("a.brand", text: subject.class.name.titleize )
      association = ""
      if subject.class != Location && subject.class != HistoricVisit
        if cap_type=="capture"
          association = "unsorted_captures"
        elsif cap_type=="historic_capture"
          association = "unsorted_hcaptures"
        end
      elsif subject.class == Location
        association = "captures"
      elsif subject.class == HistoricVisit
        association = "historic_captures"
      end
      expect(subject.send(association).count).to eq(2)
    end
    scenario "images field not set" do
      select "RAW", from: "Image state"
      click_button "Submit"
      expect(page).to have_selector(".alert-error")
    end
    scenario "image state field not set" do
      attach_file "Images", [Rails.root.join("spec","photos", "color_repeat.jpg").to_s,Rails.root.join("spec","photos", "bw_historic.jpg").to_s]
      click_button "Submit"
      expect(page).to have_selector(".alert-error")
    end
  end
end

feature "Bulk Import" do
  before(:each) { login_as_admin }
  context "from Project" do
    subject { FactoryGirl.create(:project) }
    include_examples "bulk import", "capture"
    include_examples "bulk import", "historic_capture"
  end
  context "from Survey" do
    subject { FactoryGirl.create(:survey) }
    include_examples "bulk import", "capture"
    include_examples "bulk import", "historic_capture"
  end
  context "from Survey Season" do
    subject { FactoryGirl.create(:survey_season) }
    include_examples "bulk import", "capture"
    include_examples "bulk import", "historic_capture"
  end
  context "from Visit" do
    subject { FactoryGirl.create(:visit) }
    include_examples "bulk import", "capture"
  end
  context "from Location" do
    subject { FactoryGirl.create(:location) }
    include_examples "bulk import", "capture"
  end
  context "from Historic Visit" do
    subject { FactoryGirl.create(:historic_visit) }
    include_examples "bulk import", "historic_capture"
  end
  # describe "captures" do
  #   scenario "multiple files" do
  #     click_link "Bulk Capture Import"
  #     attach_file "Images", [Rails.root.join("spec","photos", "color_repeat.jpg").to_s,Rails.root.join("spec","photos", "bw_historic.jpg").to_s]
  #     select "RAW", from: "Image state"
  #     click_button "Submit"
  #     expect(page).to have_selector("a.brand", text: "Project")
  #     expect(project.unsorted_captures).to eq(2)
  #   end
  # end
end
