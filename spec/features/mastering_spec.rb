require 'spec_helper'
include ActionDispatch::TestProcess

feature "Mastering" do
  before(:all) { Capybara.ignore_hidden_elements = false }
  after(:all) { Capybara.ignore_hidden_elements = true }


  before(:each) { capture.comparisons << historic_capture; capture.save }

  let(:capture) do
    cap = FactoryGirl.create(:capture)
    FactoryGirl.create(:capture_image, captureable: cap, image: fixture_file_upload(Rails.root.join("spec","photos", "image_initiator.jpg")))
    cap
  end
  let(:historic_capture) do
    hcap = FactoryGirl.create(:historic_capture)
    FactoryGirl.create(:capture_image, captureable: hcap, image: fixture_file_upload(Rails.root.join("spec","photos", "image_reference.jpg")))
    hcap
  end
  let(:points_initiator) do
    [{"x"=>"405", "y"=>"248"},
      {"x"=>"832", "y"=>"239"},
      {"x"=>"360", "y"=>"237"},
      {"x"=>"836", "y"=>"329"}]
  end
  let(:points_reference) do
    [ {"x"=>"224", "y"=>"104"},
      {"x"=>"1124", "y"=>"66"},
      {"x"=>"140", "y"=>"86"},
      {"x"=>"1134", "y"=>"248"} ]
  end

  context 'user is not logged in' do

    scenario "the 'Master Capture Image' link is not displayed on the capture image page" do
      visit capture_image_path(capture.capture_images.first)
      page.should_not have_content("Master Capture Image")
    end

    scenario "access is restricted to the master creation form" do
      visit new_master_creation_path(capture.capture_images.first)
      expect(current_path).to eq(root_path)
    end

  end

  context "user is logged in as admin" do

    before(:each) { login_as_admin }

    describe "basic alignment flow" do

      scenario "cannot initiate a mastering from a historic capture image" do
        visit capture_image_path(historic_capture.capture_images.first)
        page.should_not have_content("Master Capture Image")
      end

      scenario 'manipulate both images to create masters', js: true do
        visit capture_image_path(capture.capture_images.first)
        click_link "Master Capture Image"
        page.should have_content("Select Capture Image To Master Against")
        page.should have_xpath("//input[@type='hidden' and @name='image_pair_form[initiator_derivative_class]' and @value='CaptureImage']")
        page.should have_xpath("//input[@type='hidden' and @name='image_pair_form[initiator_derivative_id]' and @value='#{capture.capture_images.first.id}']")
        page.should have_xpath("//input[@type='hidden' and @name='image_pair_form[reference_derivative_class]' and @value='CaptureImage']")
        page.should have_xpath("//input[@type='radio' and @name='image_pair_form[reference_derivative_id]']")
        page.should have_content("Alignment Options")
        page.should have_xpath("//input[@type='radio' and @name='image_pair_form[aligner]']")
        page.should have_button("Start Mastering")

        choose("image_pair_form_reference_derivative_id_#{historic_capture.capture_images.first.id}")
        choose('image_pair_form_aligner_basicaligner')
        click_button("Start Mastering")

        page.should have_content("Set Control Points")
        page.should have_css('div#pointsable_initiator')
        page.should have_css('div#pointsable_reference')

        set_points("initiator", points_initiator)
        set_points("reference", points_reference)
        click_button("Align")

        page.should have_content("Verify Alignment")
        page.should have_link("Readjust & Try Again")
        page.should have_link("Done")
        #check that the classy compare class are inserted
        page.should have_css('.uc-mask')
        page.should have_css('.uc-bg')

        cap_num_capture_images = capture.capture_images.count
        hcap_num_capture_images = historic_capture.capture_images.count
        click_link "Done"
        page.should have_content("Capture Image")
        current_cap_num_capture_images = capture.capture_images.reload.count
        current_hcap_num_capture_images = historic_capture.capture_images.reload.count
        expect(current_cap_num_capture_images).to eq(cap_num_capture_images+1)
        expect(current_hcap_num_capture_images).to eq(hcap_num_capture_images+1)
      end

      scenario "chooses the 'Maintain Reference Horizon Mode'", js: true do
         visit capture_image_path(capture.capture_images.first)
        click_link "Master Capture Image"

        choose("image_pair_form_reference_derivative_id_#{historic_capture.capture_images.first.id}")
        choose('image_pair_form_aligner_referencehorizonaligner')
        click_button("Start Mastering")

        page.should have_content("Set Control Points")
        page.should have_css('div#pointsable_initiator')
        page.should have_css('div#pointsable_reference')

        set_points("initiator", points_initiator)
        set_points("reference", points_reference)
        click_button("Align")

        cap_num_capture_images = capture.capture_images.count
        hcap_num_capture_images = historic_capture.capture_images.count
        click_link "Done"
        page.should have_content("Capture Image")
        current_cap_num_capture_images = capture.capture_images.reload.count
        current_hcap_num_capture_images = historic_capture.capture_images.reload.count
        expect(current_cap_num_capture_images).to eq(cap_num_capture_images+1)
        expect(current_hcap_num_capture_images).to eq(hcap_num_capture_images+1)
      end

      scenario "the user does not select an image to master against" do
        visit capture_image_path(capture.capture_images.first)
        click_link "Master Capture Image"
        click_button("Start Mastering")
        page.should have_content("Invalid Fields")
        page.should have_content("Reference image can't be blank")
      end

      scenario 'reference image is will be upscaled', js: true do
        visit capture_image_path(capture.capture_images.first)
        click_link "Master Capture Image"

        choose("image_pair_form_reference_derivative_id_#{historic_capture.capture_images.first.id}")
        choose('image_pair_form_aligner_basicaligner')
        click_button("Start Mastering")

        page.should have_content("Set Control Points")

        #swap the points so that the resulting transform is an upscaling
        set_points("initiator", points_reference)
        set_points("reference", points_initiator)
        click_button("Align")

        page.should have_content("Warning")
        page.should have_content("Reference image will increase in size!")
      end

    end

  end

end

def set_points(container_name, points)
  points.each_with_index do |p, index|
    page.execute_script("window['pointsable_#{container_name}'].movePoint(#{index}, {x: #{p['x']}, y: #{p['y']}}, 'full_image');")
  end
end
