##Can't get these working. Too much time spent on this. Jstree+Drag+Drop+Capybara=:(. Commenting out for now.
#require 'spec_helper'

# shared_examples "moving foreign objects to tree" do |parent_association, original_parent, new_parent, obj_type_to_move, view_page|
#   specify "with 1 #{obj_type_to_move.to_s} to a #{new_parent.to_s}", js: true do
#     original_parent_object = FactoryGirl.create(original_parent)
#     subject = FactoryGirl.create(obj_type_to_move, parent_association => original_parent_object)
#     new_parent_object = FactoryGirl.create(new_parent)
#     set_speed(:medium)
#     if view_page==original_parent
#       visit "/#{original_parent_object.class.name.underscore.pluralize}/#{original_parent_object.id}"
#     else
#       ancestor_object = original_parent_object.parent
#       until view_page == ancestor_object.class.name.underscore.to_sym
#         ancestor_object = ancestor_object.parent
#         if ancestor_object.blank?
#           raise StandardError, "Could not find viewing page"
#         end
#       end
#       visit "/#{ancestor_object.class.name.underscore.pluralize}/#{ancestor_object.id}"
#     end
#     click_link('Location Photos')
#     draggable = page.find_by_id("#{subject.class.name}_#{subject.id}_handle")
#     open_node_parents(new_parent_object)
#     droppable = page.find_by_id("#{new_parent_object.class.name}_#{new_parent_object.id}").first("a")
#     selenium_drag_to(page,draggable.native,droppable.native)
#     expect(page).to have_selector('#flash_success', text: "#{subject.class.name.titleize} successfully moved!")
#     expect(page).to have_no_selector("##{subject.class.name}_#{subject.id}_handle")
#     expect(page).to have_text("There are no #{subject.class.name.titleize.downcase.pluralize}")
#     subject.reload
#     expect(subject.send(parent_association)).to eq(new_parent_object)
#   end
#   specify "with multiple #{obj_type_to_move.to_s.pluralize} to a #{new_parent.to_s}", js: true do
#     original_parent_object = FactoryGirl.create(original_parent)
#     subject = FactoryGirl.create(obj_type_to_move, parent_association => original_parent_object)
#     subject2 = FactoryGirl.create(obj_type_to_move, parent_association => original_parent_object)
#     new_parent_object = FactoryGirl.create(new_parent)
#     set_speed(:medium)
#     if view_page==original_parent
#       visit "/#{original_parent_object.class.name.underscore.pluralize}/#{original_parent_object.id}"
#     else
#       ancestor_object = original_parent_object.parent
#       until view_page == ancestor_object.class.name.underscore.to_sym
#         ancestor_object = ancestor_object.parent
#         if ancestor_object.blank?
#           raise StandardError, "Could not find viewing page"
#         end
#       end
#       visit "/#{ancestor_object.class.name.underscore.pluralize}/#{ancestor_object.id}"
#     end
#     click_link('Location Photos')
#     draggable = page.find_by_id("#{subject.class.name}_#{subject.id}_handle")
#     open_node_parents(new_parent_object)
#     droppable = page.find_by_id("#{new_parent_object.class.name}_#{new_parent_object.id}").first("a")
#     selenium_drag_to(page,draggable.native,droppable.native)
#     expect(page).to have_selector('#flash_success', text: "#{subject.class.name.titleize} successfully moved!")
#     expect(page).to have_no_selector("##{subject.class.name}_#{subject.id}_handle")
#     expect(page).to have_selector("##{subject2.class.name}_#{subject2.id}_handle")
#     expect(page).to have_no_text("There are no #{subject.class.name.titleize.downcase.pluralize}")
#     subject.reload
#     expect(subject.send(parent_association)).to eq(new_parent_object)
#   end
# end

# feature "Moving Metadata Files" do
#   before(:each) { login_as_admin }
#   describe do
#     [:visit,:station,:survey_season,:survey,:project].each do |org_parent|
#       [:visit,:station,:survey_season,:survey,:project].each do |new_parent|
#         include_examples "moving foreign objects to tree", :metadata_owner, org_parent, new_parent, :metadata_file, org_parent
#       end
#     end
#   end
# end

# feature "Moving Capture Images" do
#   before(:each) { login_as_admin }
#   describe "from the capture page" do
#     include_examples "moving foreign objects to tree", :captureable, :capture, :capture, :repeat_image_color_jpeg, :capture
#   end
# end

# feature "Moving Location Images" do
#   before(:each) { login_as_admin }
#   describe "from the visit page" do
#     include_examples "moving foreign objects to tree", :image_owner, :visit, :visit, :location_image, :visit
#     include_examples "moving foreign objects to tree", :image_owner, :visit, :location, :location_image, :visit
#     include_examples "moving foreign objects to tree", :image_owner, :visit, :station, :location_image, :visit
#     include_examples "moving foreign objects to tree", :image_owner, :location, :location, :location_image, :visit
#     include_examples "moving foreign objects to tree", :image_owner, :location, :visit, :location_image, :visit
#     include_examples "moving foreign objects to tree", :image_owner, :location, :station, :location_image, :visit
#     it "changes the parent column of the location image when the new parent is another location descendent of the visit", js: true do
#       location = FactoryGirl.create(:location)
#       location_image1 = FactoryGirl.create(:location_image, image_owner: location)
#       location2 = FactoryGirl.create(:location, visit: location.visit)
#       set_speed(:medium)
#       visit "/visits/#{location.visit.id}"
#       draggable = page.find_by_id("LocationImage_#{location_image1.id}_handle")
#       droppable = page.find_by_id("Location_#{location2.id}").first("a")
#       draggable.drag_to(droppable)
#       expect(page).to have_selector('#flash_success', text: "Location Image successfully moved!")
#       within "#location_image_row_#{location_image1.id} .parent_column" do
#         expect(page).to have_selector("a", text: "#{location2.display_name}")
#       end
#     end
#     it "changes the parent column of the location image when the parent changes from location to the visit (showing page)", js: true do
#       location = FactoryGirl.create(:location)
#       location_image1 = FactoryGirl.create(:location_image, image_owner: location)
#       set_speed(:medium)
#       visit "/visits/#{location.visit.id}"
#       draggable = page.find_by_id("LocationImage_#{location_image1.id}_handle")
#       droppable = page.find_by_id("Visit_#{location.visit.id}").first("a")
#       draggable.drag_to(droppable)
#       expect(page).to have_selector('#flash_success', text: "Location Image successfully moved!")
#       within "#location_image_row_#{location_image1.id} .parent_column" do
#         expect(page).to have_selector("a", text: "#{location.visit.display_name}")
#       end
#     end
#   end
#   describe "from the staiton page" do
#     include_examples "moving foreign objects to tree", :image_owner, :station, :station, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :station, :visit, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :station, :location, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :visit, :station, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :visit, :visit, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :visit, :location, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :location, :station, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :location, :visit, :location_image, :station
#     include_examples "moving foreign objects to tree", :image_owner, :location, :location, :location_image, :station
#     it "changes the parent column of the location image when the new parent is another location descendent of the station", js: true do
#       location = FactoryGirl.create(:location)
#       location_image1 = FactoryGirl.create(:location_image, image_owner: location)
#       location2 = FactoryGirl.create(:location, visit: location.visit)
#       set_speed(:medium)
#       visit "/stations/#{location.visit.station.id}"
#       open_node_parents(location2)
#       draggable = page.find_by_id("LocationImage_#{location_image1.id}_handle")
#       droppable = page.find_by_id("Location_#{location2.id}").first("a")
#       draggable.drag_to(droppable)
#       expect(page).to have_selector('#flash_success', text: "Location Image successfully moved!")
#       within "#location_image_row_#{location_image1.id} .parent_column" do
#         expect(page).to have_selector("a", text: "#{location2.display_name}")
#       end
#     end
#     it "changes the parent column of the location image when the parent changes from location to the station (showing page)", js: true do
#       location = FactoryGirl.create(:location)
#       location_image1 = FactoryGirl.create(:location_image, image_owner: location)
#       set_speed(:medium)
#       visit "/stations/#{location.visit.station.id}"
#       draggable = page.find_by_id("LocationImage_#{location_image1.id}_handle")
#       droppable = page.find_by_id("Visit_#{location.visit.id}").first("a")
#       draggable.drag_to(droppable)
#       expect(page).to have_selector('#flash_success', text: "Location Image successfully moved!")
#       within "#location_image_row_#{location_image1.id} .parent_column" do
#         expect(page).to have_selector("a", text: "#{location.visit.display_name}")
#       end
#     end
#   end
#   describe "from the location page" do
#     include_examples "moving foreign objects to tree", :image_owner, :location, :location, :location_image, :location
#     include_examples "moving foreign objects to tree", :image_owner, :location, :visit, :location_image, :location
#     include_examples "moving foreign objects to tree", :image_owner, :location, :station, :location_image, :location
#   end
# end

# def selenium_drag_to(page, draggable, droppable)
#   driver = page.driver.browser
#   driver.mouse.down(draggable)
#   driver.mouse.move_to(droppable)
#   driver.mouse.up
# end
