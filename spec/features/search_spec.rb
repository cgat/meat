require 'spec_helper'

feature "Search" do
  it "a search dialog exists in the navigation pane menu bar" do
    visit root_path
    page.should have_css("form.navbar-search")
  end
  it "submitting a blank search returns a no results message", js: true do
    visit root_path
    fill_in "query", :with => "\n"
    #find('.navbar-search>input').native.send_keys(:return)
    page.should have_text("0 items found...")
  end
  #the enumerated hash should have a key that can be used to
  #create a factory girl object of a searchable model. The value
  #of the key should be a column which is searchable within
  #the model.
  {project: :name,
    surveyor: :given_names,
    survey: :name, survey_season: :geographic_coverage,
    station: :name,
    visit: :visit_narrative,
    location: :location_narrative,
    capture: :fn_photo_reference,
    historic_capture: :fn_photo_reference}.each_pair do |model, attribute|
      it "the model '#{model.to_s}' is returned in the displayed search results", js: true do
        object = FactoryGirl.create(model)
        object.send("#{attribute.to_s}=", "testing123")
        object.save!
        pg_doc = double(PgSearch::Document)
        pg_doc.stub("searchable") { object }
        pg_doc.stub("searchable_type") { model.class.name }
        pg_doc.stub("content") { "testing123" }
        pg_doc.stub("searchable_id") { object.id }
        PgSearch.stub("multisearch").with("testing123") { [pg_doc] }
        visit root_path
        fill_in "query", :with => "testing123\n"
        #find('.navbar-search>input').native.send_keys(:return)
        page.should have_text(object.display_name)
      end
    end
end
