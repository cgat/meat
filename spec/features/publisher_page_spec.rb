require 'spec_helper'


feature "PublisherForms" do
  before(:each) { login_as_admin }
  describe "admin publishes library"
    scenario "publishing criteria has not been set" do
      visit publisher_path
      page.should have_content("Publisher")
      page.should have_content("Current Settings")
      page.should have_content("Update Criteria For Captures: Criteria Currently Not Set")
      page.should have_content("Update Criteria For Historic Captures: Criteria Currently Not Set")
      page.should have_selector("input[value='Update Published Content'][disabled]")
    end
    describe "when update criteria is set"  do
      before(:each) do
        setup_criteria(criteria_option)
      end
      context "for captures, " do
        let(:radio_group) { '#radio_group_criteria_captures'}
        context "with 'All Captures'" do
          let!(:capture_without_images) { FactoryGirl.create(:capture) }
          let!(:capture_with_images) { FactoryGirl.create(:capture, :with_images) }
          let(:criteria_option) { "All Captures" }
          scenario "all captures that have capture images, the capture's preview image, and the capture's ancestors are published" do
            page.should have_content("Update Criteria For Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(capture_without_images.reload.published?).to be_falsey
            expect(capture_with_images.reload.published?).to be_truthy
            expect(capture_with_images.preview_capture_image.published?).to be_truthy
            expect(capture_with_images.capture_owner.published?).to be_truthy
          end
          scenario "all captures are unpublished after setting the publishing criteria to 'No Captures' and running the publisher" do
            click_button "Update Published Content"
            setup_criteria("No Captures")
            click_button "Update Published Content"
            expect(capture_with_images.reload.published?).to be_falsey
            expect(capture_with_images.preview_capture_image.published?).to be_falsey
            expect(capture_with_images.capture_owner.published?).to be_falsey
          end
        end
        context "with 'Captures With Comparisons'" do
          let!(:capture_with_comparisons) { FactoryGirl.create(:capture, :with_comparison_with_images) }
          let!(:capture_with_comparisons_and_images) { FactoryGirl.create(:capture, :with_comparison_with_images, :with_images) }
          let(:criteria_option) { "Captures With Comparisons" }
          scenario "captures with comparison that have capture images are published" do
            page.should have_content("Update Criteria For Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(capture_with_comparisons.reload.published?).to be_falsey
            expect(capture_with_comparisons_and_images.reload.published?).to be_truthy
          end
          scenario "previously published captures that don't have comparisons, its capture images, and ancestors are unpublished" do
            new_capture = FactoryGirl.create(:capture_with_images)
            new_capture.publish
            click_button "Update Published Content"
            expect(new_capture.reload.published?).to be_falsey
            expect(new_capture.preview_capture_image.published?).to be_falsey
            expect(new_capture.capture_owner.published?).to be_falsey
          end
        end
        context "with 'Captures With Mastered Comparisons'" do
          let!(:capture_with_comparisons_and_images) { FactoryGirl.create(:capture, :with_comparison_with_images) }
          let!(:capture_mastered) { FactoryGirl.create(:capture, :mastered) }
          let(:criteria_option) { "Captures With Mastered Comparisons" }
          scenario "captures with comparison that have capture images are published" do
            page.should have_content("Update Criteria For Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(capture_with_comparisons_and_images.reload.published?).to be_falsey
            expect(capture_mastered.reload.published?).to be_truthy
          end
        end
        context "with 'No Captures'" do
          let!(:capture_without_images) { FactoryGirl.create(:capture) }
          let!(:capture_with_images) { FactoryGirl.create(:capture, :with_images) }
          let(:criteria_option) { "No Captures" }
          scenario "no captures get published" do
            page.should have_content("Update Criteria For Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(capture_without_images.reload.published?).to be_falsey
            expect(capture_with_images.reload.published?).to be_falsey
            expect(capture_with_images.preview_capture_image.published?).to be_falsey
            expect(capture_with_images.capture_owner.published?).to be_falsey
          end
        end
        context "with 'Do Not Update Published Captures'" do
          let!(:capture_with_images) { FactoryGirl.create(:capture, :with_images) }
          let(:criteria_option) { "Do Not Update Published Captures" }
          scenario "an publish update does not effect the currently published captures" do
            setup_criteria("All Captures")
            click_button "Update Published Content"
            new_capture = FactoryGirl.create(:capture, :with_images)
            setup_criteria(criteria_option)
            click_button "Update Published Content"
            expect(capture_with_images.reload.published?).to be_truthy
            expect(new_capture.reload.published?).to be_falsey
          end
        end
      end
      describe "for historic captures" do
        let(:radio_group) { '#radio_group_criteria_historic_captures'}
        context "with 'All Historic Captures'" do
          let!(:hcapture_without_images) { FactoryGirl.create(:historic_capture) }
          let!(:hcapture_with_images) { FactoryGirl.create(:historic_capture, :with_images) }
          let(:criteria_option) { "All Historic Captures" }
          scenario "all historic captures that have capture images, the historic capture's preview image, and the historic capture's ancestors are published" do
            page.should have_content("Update Criteria For Historic Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(hcapture_without_images.reload.published?).to be_falsey
            expect(hcapture_with_images.reload.published?).to be_truthy
            expect(hcapture_with_images.preview_capture_image.published?).to be_truthy
            expect(hcapture_with_images.capture_owner.published?).to be_truthy
          end
          scenario "all historic captures are unpublished after setting the publishing criteria to 'No Historic Captures' and running the publisher" do
            click_button "Update Published Content"
            setup_criteria("No Historic Captures")
            click_button "Update Published Content"
            expect(hcapture_with_images.reload.published?).to be_falsey
            expect(hcapture_with_images.preview_capture_image.published?).to be_falsey
            expect(hcapture_with_images.capture_owner.published?).to be_falsey
          end
        end
        context "with 'Historic Captures With Comparisons'" do
          let!(:hcapture_with_comparisons) { FactoryGirl.create(:historic_capture, :with_comparison_with_images) }
          let!(:hcapture_with_comparisons_and_images) { FactoryGirl.create(:historic_capture, :with_comparison_with_images, :with_images) }
          let(:criteria_option) { "Historic Captures With Comparisons" }
          scenario "historic captures with comparison that have capture images are published" do
            page.should have_content("Update Criteria For Historic Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(hcapture_with_comparisons.reload.published?).to be_falsey
            expect(hcapture_with_comparisons_and_images.reload.published?).to be_truthy
          end
          scenario "previously published historic captures that don't have comparisons, its capture images, and ancestors are unpublished" do
            new_hcapture = FactoryGirl.create(:historic_capture_with_images)
            new_hcapture.publish
            click_button "Update Published Content"
            expect(new_hcapture.reload.published?).to be_falsey
            expect(new_hcapture.preview_capture_image.published?).to be_falsey
            expect(new_hcapture.capture_owner.published?).to be_falsey
          end
        end
        context "with 'Historic Captures With Mastered Comparisons'" do
          let!(:hcapture_with_comparisons_and_images) { FactoryGirl.create(:historic_capture, :with_comparison_with_images) }
          let!(:hcapture_mastered) { FactoryGirl.create(:historic_capture, :mastered) }
          let(:criteria_option) { "Historic Captures With Mastered Comparisons" }
          scenario "historic captures with comparison that have capture images are published" do
            page.should have_content("Update Criteria For Historic Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(hcapture_with_comparisons_and_images.reload.published?).to be_falsey
            expect(hcapture_mastered.reload.published?).to be_truthy
          end
        end
        context "with 'No Historic Captures'" do
          let!(:hcapture_without_images) { FactoryGirl.create(:historic_capture) }
          let!(:hcapture_with_images) { FactoryGirl.create(:historic_capture, :with_images) }
          let(:criteria_option) { "No Historic Captures" }
          scenario "no historic captures get published" do
            page.should have_content("Update Criteria For Historic Captures: #{criteria_option}")
            click_button "Update Published Content"
            current_path.should eq "/publisher"
            expect(hcapture_without_images.reload.published?).to be_falsey
            expect(hcapture_with_images.reload.published?).to be_falsey
            expect(hcapture_with_images.preview_capture_image.published?).to be_falsey
            expect(hcapture_with_images.capture_owner.published?).to be_falsey
          end
        end
        context "with 'Do Not Update Published Historic Captures'" do
          let!(:hcapture_with_images) { FactoryGirl.create(:historic_capture, :with_images) }
          let(:criteria_option) { "Do Not Update Published Historic Captures" }
          scenario "an publish update does not effect the currently published historic captures" do
            setup_criteria("All Historic Captures")
            click_button "Update Published Content"
            new_hcapture = FactoryGirl.create(:historic_capture, :with_images)
            setup_criteria(criteria_option)
            click_button "Update Published Content"
            expect(hcapture_with_images.reload.published?).to be_truthy
            expect(new_hcapture.reload.published?).to be_falsey
          end
        end
      end
  end
end

def setup_criteria(criteria_option)
  visit settings_path
  within(:css, radio_group) do
    choose criteria_option
  end
  click_button "Update And Go To Publisher"
end
