require 'spec_helper'

feature "Station pages" do
  let(:model) { Station }
  let(:parent_model) { SurveySeason }
  describe "object show" do
    describe "metadata files" do
      include_examples "metadata file in show page", "station"
    end
  end
  describe "object creation" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("station",:build, reject_attrs: [:station_owner_id,:station_owner_type]) }
    include_examples "object creation"
    describe "metadata files" do
      before(:each) do
        survey_season = FactoryGirl.create(:survey_season)
        visit new_survey_season_station_path(survey_season.id)
        fill_in "Name", with: "Stn1"
      end
      include_examples "metadata file in create page"
    end
    describe "add unsorted capture" do
      let(:model) { Capture }
      let(:parent_model) { Station }
      let!(:attr_hash) { factory_to_attr_hash("capture",:create, reject_attrs: [:capture_owner_id, :capture_owner_type, :camera_id, :lens_id, :f_stop, :shutter_speed, :iso, :focal_length, :capture_datetime]) }
      include_examples "object creation"
      describe do
        scenario "with a capture image", js: true do
          station = FactoryGirl.create(:station)
          visit new_station_capture_path(station.id)
          #click_link "Add capture image"
          page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
          page.all(:css,'.image_state').last.select("RAW")
          click_button "Submit"
          expect(page).to have_text('color_repeat.jpg')
          expect(page).to have_selector("a.brand",text: "Capture")
        end
        scenario "clicking the Add Unsorted Capture link goes to the new capture page" do
          station = FactoryGirl.create(:station)
          visit station_path(station.id)
          click_link "Add Unsorted Capture"
          expect(page).to have_text("Add New Capture")
        end
      end
    end
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    let!(:attr_hash) { factory_to_attr_hash("station",:create, reject_attrs: [:station_owner_id,:station_owner_type]) }
    include_examples "object edit"
    describe "metadata files" do
      subject { FactoryGirl.create(:station_with_metadata) }
      before(:each) do
        visit edit_station_path(subject.id)
      end
      include_examples "metadata file in edit page"
    end
  end
  describe "object destroy" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:station) }
    include_examples "object destroy"
  end
  describe "adding a child object" do
    before(:each) { login_as_admin }
    before(:each) { FactoryGirl.create(:station) }
    include_examples "adding a child object", Visit
    include_examples "adding a child object", HistoricVisit
  end
  describe "js tree interactions" do
    before(:each) { FactoryGirl.create(:station) }
    include_examples "js tree"
  end
end
