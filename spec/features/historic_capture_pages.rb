#!/bin/env ruby
# encoding: utf-8
require 'spec_helper'

feature "Historic Capture pages" do
  let(:historic_capture) { FactoryGirl.create(:historic_capture)}
  shared_examples "selecting captures" do
    scenario "selecting one capture for comparison" do
      visit edit_historic_capture_path(@historic_capture.id)
      select @cap1.display_name, from: "Captures"
      click_button "Submit"
      expect(page).to have_selector("a.brand", text: "Historic Capture")
      expect(page).to have_no_selector("a.brand", text: "Edit Historic Capture")
      expect(page).to have_text(@cap1.display_name)
      expect(page).to have_no_text(@cap2.display_name)
    end
    scenario "selecting two capture for comparison" do
      visit edit_historic_capture_path(@historic_capture.id)
      select @cap1.display_name, from: "Captures"
      select @cap2.display_name, from: "Captures"
      click_button "Submit"
      expect(page).to have_selector("a.brand", text: "Historic Capture")
      expect(page).to have_no_selector("a.brand", text: "Edit Historic Capture")
      expect(page).to have_text(@cap1.display_name)
      expect(page).to have_text(@cap2.display_name)
    end
  end
  describe "object edit" do
    before(:each) { login_as_admin }
    describe do
      let(:model) { HistoricCapture }
      let(:parent_model) { HistoricVisit }
      let!(:attr_hash) { factory_to_attr_hash("historic_capture",:create, reject_attrs: [:capture_owner_id, :capture_owner_type, :condition, :f_stop, :shutter_speed, :focal_length]) }
      include_examples "object edit"
    end
    describe "capture comparisons" do
      context "historic capture is owned by a historic visit and capture by location" do
        #making sure that hcaps and cap have same station
        before(:each) do
          station = FactoryGirl.create(:station)
          visit = FactoryGirl.create(:visit, station: station)
          location = FactoryGirl.create(:location, visit: visit)
          @cap1 = FactoryGirl.create(:capture, capture_owner: location)
          @cap2 = FactoryGirl.create(:capture, capture_owner: location)
          historic_visit = FactoryGirl.create(:historic_visit, station: station)
          @historic_capture = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
        end
        include_examples "selecting captures"
      end
      context "historic capture is owned by a historic visit and capture by visit" do
        #making sure that hcaps and cap have same station
        before(:each) do
          station = FactoryGirl.create(:station)
          visit = FactoryGirl.create(:visit, station: station)
          @cap1 = FactoryGirl.create(:capture, capture_owner: visit)
          @cap2 = FactoryGirl.create(:capture, capture_owner: visit)
          historic_visit = FactoryGirl.create(:historic_visit, station: station)
          @historic_capture = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
        end
        include_examples "selecting captures"
      end
      context "historic capture is owned by a historic visit and capture by station" do
        #making sure that hcaps and cap have same station
        before(:each) do
          station = FactoryGirl.create(:station)
          @cap1 = FactoryGirl.create(:capture, capture_owner: station)
          @cap2 = FactoryGirl.create(:capture, capture_owner: station)
          historic_visit = FactoryGirl.create(:historic_visit, station: station)
          @historic_capture = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
        end
        include_examples "selecting captures"
      end
      context "historic capture is owned by a survey season" do
        let(:historic_capture) {FactoryGirl.create(:unsorted_hcapture_survey_season)}
        scenario "cannot select captures when owned outside station" do
          visit edit_historic_capture_path(historic_capture.id)
          expect(page).to have_selector("a.brand", "Edit Historic Capture")
          expect(page).to have_no_selector(".captures_select")
        end
      end
      context "historic capture is owned by a survey" do
        let(:historic_capture) {FactoryGirl.create(:unsorted_hcapture_survey)}
        scenario "cannot select captures when owned outside station" do
          visit edit_historic_capture_path(historic_capture.id)
          expect(page).to have_selector("a.brand", "Edit Historic Capture")
          expect(page).to have_no_selector(".captures_select")
        end
      end
    end
    describe "capture image edit" do
      let(:historic_capture) {FactoryGirl.create(:historic_capture_with_images)}
      describe "image state" do
        ["RAW","MASTER","INTERIM","MISC","GRIDDED"].each do |image_state|
          scenario "changed to #{image_state}" do
            visit edit_historic_capture_path(historic_capture.id)
            ci = historic_capture.capture_images.first
            within "##{ci.image_secure_token}" do
              page.find('.image_state').select(image_state)
            end
            click_button "Submit"
            expect(page).to have_selector("a.brand", text: "Historic Capture")
            ci.reload
            expect(ci.image_state).to eq(image_state)
          end
        end
      end
      describe "delete" do
        scenario "delete an image using the checkbox" do
          visit edit_historic_capture_path(historic_capture.id)
          ci = historic_capture.capture_images.first
          within "##{ci.image_secure_token}" do
            page.find('.delete_check_box').set(true)
          end
          click_button "Submit"
          expect(page).to have_selector("a.brand","Historic Capture")
          expect(CaptureImage.find_by_id(ci.id)).to_not be_present
        end
      end
      scenario "add a new image when images already exist", js: true, driver: :selenium do
        visit edit_historic_capture_path(historic_capture.id)
        expect {
          click_link "Add capture image"
          page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
          page.all(:css,'.image_state').last.select("RAW")
          click_button "Submit"
        }.to change{historic_capture.capture_images.count}.by(1)
      end
      scenario "add a new image, but don't set image state", js: true do
        visit edit_historic_capture_path(historic_capture.id)
        click_link "Add capture image"
        page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
        click_button "Submit"
        expect(page).to have_text("Invalid")
      end
      scenario "add a new image then remove it", js: true do
        visit edit_historic_capture_path(historic_capture.id)
        expect {
          click_link "Add capture image"
          page.first(:xpath, "//input[@type='file']").set(Rails.root.join('spec','photos','color_repeat.jpg'))
          page.all(:css,'.image_state').last.select("RAW")
          click_link "Remove"
          click_button "Submit"
        }.to_not change{historic_capture.capture_images.count}
      end
    end
  end

end
