require 'spec_helper'

describe Capture do
  subject { FactoryGirl.create(:capture)}

  it "has a valid default FactoryGirl object" do
    expect(subject).to be_valid
  end
  describe "Publisher" do
    it_behaves_like "a Publisher"
    it "uploads its preview image when published" do
      capture = FactoryGirl.create(:capture,:with_images)
      capture.publish
      expect(capture.preview_capture_image.published?).to be_truthy
    end
    it "deletes the remote versions of the preview image when unpublished" do
      capture = FactoryGirl.create(:capture,:with_images)
      capture.publish
      capture.reload
      capture.unpublish
      capture.reload
      expect(capture.preview_capture_image.published?).to be_falsey
    end
    it "unpublishes an ancestor only if it has no children that are published" do
      capture_published1 = FactoryGirl.create(:capture,:with_images)
      capture_published1.publish
      capture_published1.publish_ancestors
      capture_published2 = FactoryGirl.create(:capture, :with_images, capture_owner: capture_published1.capture_owner)
      capture_published2.publish
      capture_published1.unpublish
      capture_published1.unpublish_ancestors
      expect(capture_published1.capture_owner.published?).to be_truthy
    end
  end

  describe "filesystemable behaviour on a has_filesystemable_descendants object" do
    let(:capture_with_images) { FactoryGirl.create(:capture_with_images) }
    it "should move the capture's capture images to the new parent" do
      ci = capture_with_images.capture_images.first
      path_to_ci = ci.image.file.path
      location1 = FactoryGirl.create(:location)
      new_path_to_ci = File.join(location1.visit.filesystem_path,ci.image_state, ci.filesystem_name)
      capture_with_images.capture_owner = location1
      capture_with_images.save!
      expect(File.exists?(new_path_to_ci)).to eq(true)
    end
  end

end


