require 'spec_helper'


describe Image do
  let(:scenic_image) { FactoryGirl.create(:scenic_image) }
  let(:location_image) { FactoryGirl.create(:location_image)}
  #the default will be the scenic image. The use of scenic and location image will be used in the few cases that are specific to those image types
  let(:image) { scenic_image }

  it "has a valid default (scenic) FactoryGirl object" do
    expect(scenic_image).to be_valid
  end
  it "has a valid default (location image) FactoryGirl object" do
    expect(location_image).to be_valid
  end

  describe "filesystemable" do
    let(:owner_attribute) { "image_owner" }
    describe "location image" do
      context "when parent is a location" do
        subject { location_image }
        let(:parents) {["location"]}
        it_behaves_like "moving filesystemable objects"
        it_behaves_like "naming filesystemable objects", 'Location_Photos'
      end
    end
    describe "scenic image" do
      context "when parent is a station" do
        subject do
          scenic_image.image_owner = FactoryGirl.create(:station)
          scenic_image.save!
          scenic_image
        end
        let(:parents) {["station","survey","survey_season"]}
        it_behaves_like "moving filesystemable objects"
        it_behaves_like "naming filesystemable objects", "Scenics"
      end
      context "when parent is a survey" do
        subject do
          scenic_image.image_owner = FactoryGirl.create(:survey)
          scenic_image.save!
          scenic_image
        end
        let(:parents) {["station","survey","survey_season"]}
        it_behaves_like "moving filesystemable objects"
        it_behaves_like "naming filesystemable objects", "Scenics"
      end
      context "when parent is a survey season" do
        subject do
          FactoryGirl.create(:survey_season).scenics << scenic_image
          scenic_image.save!
          scenic_image
        end
        let(:parents) {["station","survey","survey_season"]}
        it_behaves_like "moving filesystemable objects"
        it_behaves_like "naming filesystemable objects", "Scenics"
      end
    end
  end

end
