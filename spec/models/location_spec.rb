# == Schema Information
#
# Table name: locations
#
#  id                 :integer          not null, primary key
#  visit_id           :string(255)
#  location_narrative :text
#  location_identity  :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'spec_helper'

describe Location do
  subject { FactoryGirl.create(:location)}

  it "has a valid default FactoryGirl object" do
    expect(subject).to be_valid
  end

  it_behaves_like "a Publisher"

  describe "filesystemable behaviour on a has_filesystemable_descendants object" do
    let(:location_with_cimages) { FactoryGirl.create(:location_with_cimages) }
    it "should move the location's descendant capture images to the new parent" do
      ci = location_with_cimages.captures.first.capture_images.first
      path_to_ci = ci.image.file.path
      visit1 = FactoryGirl.create(:visit)
      new_path_to_ci = File.join(visit1.filesystem_path,ci.image_state, ci.filesystem_name)
      location_with_cimages.visit = visit1
      location_with_cimages.save!
      expect(File.exists?(new_path_to_ci)).to eq(true)
    end
  end

end
