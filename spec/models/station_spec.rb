# == Schema Information
#
# Table name: stations
#
#  id               :integer          not null, primary key
#  survey_season_id :integer
#  name             :string(255)
#  lat              :float
#  long             :float
#  elevation        :float
#  nts_sheet        :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'spec_helper'

describe Station do
  subject { FactoryGirl.create(:station)}

  it "has a valid default Factory Girl object" do
    expect(subject).to be_valid
  end

  it_behaves_like "a Publisher"

  describe "filesystemable" do
    let(:parents) {["survey_season"]} #add project when implementing
    let(:owner_attribute) { "station_owner" }
    context "when parent is a survey season" do
      #by adding a metadata file to the survey, we ensure that the filesystem object exists (it only exists in the database if it has no real file children or grandchildren)
      subject do
        station = FactoryGirl.create(:station)
        FactoryGirl.create(:metadata_file, metadata_owner: station)
        station
      end
      it_behaves_like "moving filesystemable objects"
      it_behaves_like "naming filesystemable objects"
      it "will change the file system name when changing the survey season year" do
        expect { subject.name = "Name"; subject.save!}.to change(subject,:filesystem_path)
      end
      it "can change name and parent simultaneously" do
        expect { subject.name = "Name"
          subject.station_owner = FactoryGirl.create(:survey_season)
          subject.save! }.to change(subject,:filesystem_path)
      end
    end
  end



end
