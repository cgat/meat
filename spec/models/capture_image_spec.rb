# == Schema Information
#
# Table name: capture_images
#
#  id               :integer          not null, primary key
#  capture_id       :integer
#  hash_key         :string(255)
#  file_path :text
#  file_size        :float
#  x_dim            :integer
#  y_dim            :integer
#  bit_depth        :integer
#  image_state      :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'spec_helper'


#Problem. When trying to update the parents of a capture, the module specific callback functions are not being run (specifically :move_filesystem_obj).
#http://stackoverflow.com/questions/13636989/rspec-issuing-a-save-on-an-existing-but-modified-activerecord-does-not-run-be/13637077#comment18708193_13637077
#work around (in shared_filesystemable file) is to explicitly call the function before updating.
describe CaptureImage do
  subject { FactoryGirl.create(:repeat_image_fake_image) }
  let(:repeat_image) { FactoryGirl.create(:repeat_image_fake_image) }
  let(:historic_image) { FactoryGirl.create(:historic_image_fake_image) }

  it_behaves_like "a Publisher"


  it "can be uploaded with backgrounder" do
    capture = FactoryGirl.create(:capture)
    image_upload=fixture_file_upload(Rails.root.join('spec','photos','color_repeat.jpg'))
    cimage = CaptureImage.new(image: image_upload, image_state: "RAW", captureable_type: "Capture", captureable_id: capture.id)
    #cimage.do_not_save_metadata=true
    cimage.save!
    expect(cimage.image_tmp).to be_present
    sleep(2)
    expect(cimage.reload.image_tmp).to be_nil
  end

  describe do
    it "has a valid repeat image factory object" do
      repeat_image.should be_valid
    end
    it "has a valid historic image factory object" do
      historic_image.should be_valid
    end
  end


  describe 'filesystemable' do
    let(:parents) {["capture", "historic_capture"]}
    let(:owner_attribute) { "captureable" }
    describe "moving capture images" do
      context "when parent is a capture" do
        subject { FactoryGirl.create(:repeat_image_fake_image, image_state: 'MASTER') }
        include_examples "moving filesystemable objects"
        it_behaves_like "naming filesystemable objects", 'Master'
      end
      context "when parent is a historic capture" do
        subject { FactoryGirl.create(:historic_image_fake_image, image_state: 'RAW') }
        include_examples "moving filesystemable objects"
        it_behaves_like "naming filesystemable objects", 'Raw'
      end
    end
  end

end
