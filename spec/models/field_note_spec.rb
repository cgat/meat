require 'spec_helper'

describe FieldNote do
  let(:field_note) { FactoryGirl.create(:field_note) }

  it "has a valid default FactoryGirl object" do
    expect(field_note).to be_valid
  end

  describe "filesystemable" do
    let(:parents) {["visit"]}
    let(:owner_attribute) { "visit" }
    context "when parent is a visit" do
      subject { field_note }
      it_behaves_like "moving filesystemable objects"
      it_behaves_like "naming filesystemable objects"
    end
  end
end
