# == Schema Information
#
# Table name: surveys
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  historical_map_sheet :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'spec_helper'

describe Survey do

  subject { FactoryGirl.create(:survey)}

  it "has a default FactoryGirl object" do
    subject.should be_valid
  end

  it_behaves_like "a Publisher"

  describe "unmanaged metadata folder" do
    shared_examples "deletes the object folder" do
      it "deletes the object folder" do
        survey_path = subject.filesystem_path
        subject.destroy
        expect(Dir.exists?(survey_path)).to be_falsey
      end
    end
    describe "folder creation" do
      it "create_umetadata_folder creates folder" do
        subject.create_umetadata_folder
        expect(Dir.exists?(subject.umetadata_path)).to be_truthy
      end
    end
    describe "destroying object" do
      context "no metadata folder created" do
        include_examples "deletes the object folder"
      end
      context "with no child objects" do
        context "with metadata folder with file content" do
          before do
            subject.create_umetadata_folder
            FileUtils.touch File.join(subject.umetadata_path,"test_file.txt")
          end
          include_examples "deletes the object folder"
        end
        context "with metadata folder with subfolders" do
          before do
            subject.create_umetadata_folder
            FileUtils.mkdir File.join(subject.umetadata_path,"test_subfolder")
            FileUtils.touch File.join(subject.umetadata_path,"test_subfolder","test_file.txt")
          end
          include_examples "deletes the object folder"
        end
      end
      context "with child objects" do
        before do
          #add an unsorted cap as a child
          cap = FactoryGirl.create(:capture_with_images, capture_owner: subject)
        end
        context "with metadata folder with file content" do
          before do
            subject.create_umetadata_folder
            FileUtils.touch File.join(subject.umetadata_path,"test_file.txt")
          end
          include_examples "deletes the object folder"
        end
        context "with metadata folder with subfolders" do
          before do
            subject.create_umetadata_folder
            FileUtils.mkdir File.join(subject.umetadata_path,"test_subfolder")
            FileUtils.touch File.join(subject.umetadata_path,"test_subfolder","test_file.txt")
          end
          include_examples "deletes the object folder"
        end
      end
    end
    describe "destroying of last child (with files)" do
      let(:cap) {cap = FactoryGirl.create(:capture_with_images, capture_owner: subject)}
      context "with metadata folder with file content" do
        before do
          subject.create_umetadata_folder
          FileUtils.touch File.join(subject.umetadata_path,"test_file.txt")
        end
        it "does not delete the metadata folder" do
          cap.destroy
          expect(Dir.exists?(subject.umetadata_path)).to be_truthy
        end
      end
      context "no metdata folder created" do
        it "deletes the survey object folder (object exists, but folder not needed because file structure is sparse" do
          cap.destroy
          expect(Dir.exists?(subject.filesystem_path)).to be_falsey
        end
      end
    end
  end

  describe "filesystemable" do
    let(:parents) {["surveyor"]}
    let(:owner_attribute) { "surveyor" }
    context "when parent is a surveyor" do
      #by adding a metadata file to the survey, we ensure that the filesystem object exists (it only exists in the database if it has no real file children or grandchildren)
      subject { s=FactoryGirl.create(:survey); FactoryGirl.create(:metadata_file, metadata_owner: s); s  }
      it_behaves_like "moving filesystemable objects"
      it_behaves_like "naming filesystemable objects"
      it "will change the file system name when changing the survey name" do
        expect { subject.name = "New Name"; subject.save!}.to change(subject,:filesystem_path)
      end
      it "can change name and parent simultaneously" do
        expect { subject.name = "New Name"
          subject.surveyor = FactoryGirl.create(:surveyor)
          subject.save! }.to change(subject,:filesystem_path)
      end
    end
  end
end
