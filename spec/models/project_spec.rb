require 'spec_helper'

describe Project do
  let(:subject) { FactoryGirl.create(:project) }

  it "has a valid default FactoryGirl object" do
    expect(subject).to be_valid
  end

  it_behaves_like "a Publisher", true

  describe "unmanaged metadata folder" do
    shared_examples "deletes the object folder" do
      it "deletes the object folder" do
        project_path = subject.filesystem_path
        subject.destroy
        expect(Dir.exists?(project_path)).to be_falsey
      end
    end
    describe "folder creation" do
      it "create_umetadata_folder creates folder" do
        subject.create_umetadata_folder
        expect(Dir.exists?(subject.umetadata_path)).to be_truthy
      end
    end
    describe "destroying object" do
      context "no metadata folder created" do
        include_examples "deletes the object folder"
      end
      context "with no child objects" do
        context "with metadata folder with file content" do
          before do
            subject.create_umetadata_folder
            FileUtils.touch File.join(subject.umetadata_path,"test_file.txt")
          end
          include_examples "deletes the object folder"
        end
        context "with metadata folder with subfolders" do
          before do
            subject.create_umetadata_folder
            FileUtils.mkdir File.join(subject.umetadata_path,"test_subfolder")
            FileUtils.touch File.join(subject.umetadata_path,"test_subfolder","test_file.txt")
          end
          include_examples "deletes the object folder"
        end
      end
      context "with child objects" do
        before do
          #add an unsorted cap as a child
          cap = FactoryGirl.create(:capture_with_images, capture_owner: subject)
        end
        context "with metadata folder with file content" do
          before do
            subject.create_umetadata_folder
            FileUtils.touch File.join(subject.umetadata_path,"test_file.txt")
          end
          include_examples "deletes the object folder"
        end
        context "with metadata folder with subfolders" do
          before do
            subject.create_umetadata_folder
            FileUtils.mkdir File.join(subject.umetadata_path,"test_subfolder")
            FileUtils.touch File.join(subject.umetadata_path,"test_subfolder","test_file.txt")
          end
          include_examples "deletes the object folder"
        end
      end
    end
    describe "destroying of last child (with files)" do
      let(:cap) {cap = FactoryGirl.create(:capture_with_images, capture_owner: subject)}
      context "with metadata folder with file content" do
        before do
          subject.create_umetadata_folder
          FileUtils.touch File.join(subject.umetadata_path,"test_file.txt")
        end
        it "does not delete the metadata folder" do
          cap.destroy
          expect(Dir.exists?(subject.umetadata_path)).to be_truthy
        end
      end
      context "no metdata folder created" do
        it "deletes the project object folder (object exists, but folder not needed because file structure is sparse" do
          cap.destroy
          expect(Dir.exists?(subject.filesystem_path)).to be_falsey
        end
      end
    end
  end



end
