

require 'spec_helper'

describe HistoricCapture do
  subject {FactoryGirl.create(:historic_capture)}

  it "has a valid default FactoryGirl object" do
    expect(subject).to be_valid
  end

  it_behaves_like "a Publisher"

  describe "filesystemable behaviour on a has_filesystemable_descendants object" do
    let(:historic_capture_with_images) { FactoryGirl.create(:historic_capture_with_images) }
    it "should move the historic capture's capture images to the new parent" do
      ci = historic_capture_with_images.capture_images.first
      path_to_ci = ci.image.file.path
      historic_visit1 = FactoryGirl.create(:historic_visit)
      new_path_to_ci = File.join(historic_visit1.filesystem_path,ci.image_state, ci.filesystem_name)
      historic_capture_with_images.capture_owner = historic_visit1
      historic_capture_with_images.save!
      expect(File.exists?(new_path_to_ci)).to eq(true)
    end
  end


end
