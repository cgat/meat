# == Schema Information
#
# Table name: surveyors
#
#  id          :integer          not null, primary key
#  last_name   :string(255)
#  given_names :string(255)
#  short_name  :string(255)
#  affiliation :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

describe Surveyor do
  subject { FactoryGirl.create(:surveyor) }

  it_behaves_like "a Publisher", true

  describe 'validations' do
    specify "fs_path should be pouplated upon creation" do
      expect(subject.fs_path).to be_present
    end
  end

end
