# == Schema Information
#
# Table name: visits
#
#  id                          :integer          not null, primary key
#  station_id                  :integer
#  date                        :date
#  start_time                  :time
#  finish_time                 :time
#  pilot                       :string(255)
#  rw_call_sign                :string(255)
#  photographer_participant_id :integer
#  fn_author_participant_id    :integer
#  visit_narrative             :text
#  illustration                :boolean
#  weather_narrative           :text
#  weather_temp                :float
#  weather_ws                  :float
#  weather_gs                  :float
#  weather_pressure            :float
#  weather_rh                  :float
#  weather_wb                  :float
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#

require 'spec_helper'

describe Visit do
  subject { FactoryGirl.create(:visit) }

  it "has valid FactoryGirl object" do
    expect(subject).to be_valid
  end

  it_behaves_like "a Publisher"

  describe "filesystemable" do
    subject do
      visit = FactoryGirl.create(:visit)
      FactoryGirl.create(:metadata_file, metadata_owner: visit)
      visit
    end
    context "when parent is a station" do
      let(:parents) {["station"]}
      let(:owner_attribute) { "station" }
      it_behaves_like "moving filesystemable objects"
      it_behaves_like "naming filesystemable objects"
      it "will change the file system name when changing the visit date" do
        expect { subject.date = Date.today-100.days; subject.save!}.to change(subject,:filesystem_path)
      end
      it "can change date and parent simultaneously" do
        expect { subject.date = Date.today-100.days
          subject.station = FactoryGirl.create(:station)
          subject.save! }.to change(subject,:filesystem_path)
      end
    end
  end


end
