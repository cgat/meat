require 'spec_helper'

describe MetadataFile do
  let(:metadata) { FactoryGirl.create(:metadata_file)}


  describe "filesystemable" do
    let(:parents) {["station", "visit"]}
    let(:owner_attribute) { "metadata_owner" }
    context "when parent is a survey season" do
      subject { metadata }
      it_behaves_like "moving filesystemable objects"
      it_behaves_like "naming filesystemable objects"
    end
  end


end
