# == Schema Information
#
# Table name: historic_visits
#
#  id         :integer          not null, primary key
#  station_id :integer
#  date       :date
#  comments   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe HistoricVisit do
  subject { FactoryGirl.create(:historic_visit)}

  it "has a valid default FactoryGirl object" do
    expect(subject).to be_valid
  end

  it_behaves_like "a Publisher"

  describe "filesystemable" do
    subject do
      historic_visit = FactoryGirl.create(:historic_visit)
      FactoryGirl.create(:historic_capture_with_images, capture_owner: historic_visit)
      historic_visit
    end
    context "when parent is a station" do
      let(:parents) {["station"]}
      let(:owner_attribute) { "station" }
      it_behaves_like "moving filesystemable objects"
      it_behaves_like "naming filesystemable objects"
    end
  end

end
