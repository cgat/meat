require 'rubygems'

ENV["RAILS_ENV"] = 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'capybara/rspec'
require 'sidekiq/testing'
Sidekiq::Testing.inline!
# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

#if using headless browser. However, I cannot get tests to pass.
#Seems to be an issue with the alert dialog
Capybara.default_driver = :rack_test

Capybara.javascript_driver = :webkit

Capybara.register_driver :selenium do |app|
 Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

RSpec.configure do |config|
  config.include Helpers
  config.include ApplicationHelper
  config.treat_symbols_as_metadata_keys_with_true_values = true
  I18n.enforce_available_locales = false
  config.before(:each) do |example|
    # Clears out the jobs for tests using the fake testing
    Sidekiq::Worker.clear_all

    # if example.metadata[:sidekiq] == :fake
    #   Sidekiq::Testing.fake!
    # elsif example.metadata[:sidekiq] == :inline
    #   Sidekiq::Testing.inline!
    # elsif example.metadata[:type] == :acceptance
    #   Sidekiq::Testing.inline!
    # else
    #   Sidekiq::Testing.inline!
    # end
  end

  config.infer_spec_type_from_file_location!

  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  #config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    DatabaseCleaner.start
    Preference.create(mlp_library_local_root: "#{Rails.root}/spec/support/test_library")
  end

  config.after(:each) do
      FileUtils.rm_rf("#{Rails.root}/spec/support/test_library")
      FileUtils.rm_rf("#{Rails.root}/public/uploads/test")
      DatabaseCleaner.clean
  end

  ####
  # the below configuration might help fix a deadlock issue
  # I've seen a few times in tests. However, I think this will
  # break other tests.
  #
  # config.before( :suite ) do
  #   DatabaseCleaner.clean_with :truncation
  #   DatabaseCleaner.strategy = :transaction
  # end

  # config.around( :each ) do |spec|
  #   if spec.metadata[:js]
  #     # JS => doesn't share connections => can't use transactions
  #     spec.run
  #     DatabaseCleaner.clean_with :deletion
  #   else
  #     # No JS/Devise => run with Rack::Test => transactions are ok
  #     DatabaseCleaner.start
  #     spec.run
  #     DatabaseCleaner.clean

  #     # see https://github.com/bmabey/database_cleaner/issues/99
  #     begin
  #       ActiveRecord::Base.connection.send :rollback_transaction_records, true
  #     rescue
  #     end
  #   end
  # end

  ####
end


VIPS::thread_shutdown()

# This file is copied to spec/ when you run 'rails generate rspec:install'


