require 'spec_helper'


describe UnpublishWorker do
  let(:capture_image) { FactoryGirl.create(:repeat_image_color_jpeg) }
  it "enqueues the job", sidekiq: :fake do
    expect { UnpublishWorker.perform_async(capture_image.id, capture_image.class.name) }.to change(UnpublishWorker.jobs, :size).by(1)
  end
  it "properly processes the job", sidekiq: :inline do
    PublishWorker.perform_async(capture_image.id, capture_image.class.name)
    capture_image.reload
    UnpublishWorker.perform_async(capture_image.id, capture_image.class.name)
    expect(capture_image.reload).to_not be_published
    expect(capture_image.parent).to_not be_published
  end
end

