require 'spec_helper'
require 'carrierwave/test/matchers'

shared_examples "carrierwave local image processing" do
  context 'the thumb version' do
    it "scales down an image to fit with 150 by 150 pixels" do
      @uploader.thumb.should be_no_larger_than(150, 150)
    end
    it "is saved as a jpeg file" do
      expect(@uploader.thumb.file.filename).to match(/\.(jpeg|jpg)$/)
    end
  end

  context 'the medium version' do
    it "scales down an image to fit within 900 by 900 pixels" do
      @uploader.medium.should be_no_larger_than(900, 900)
    end
    it "is saved as a jpeg file" do
      expect(@uploader.medium.file.filename).to match(/\.(jpeg|jpg)$/)
    end
  end
end

describe ImageUploader do
  include CarrierWave::Test::Matchers

  before(:each) do
    ImageUploader.enable_processing = true
    ci = FactoryGirl.build(:repeat_image_no_image)
    ci.process_image_upload = true
    ci.image = FilelessIO.new(filepath)
    ci.save!
    @uploader = ci.image
  end

  after(:each) do
    ImageUploader.enable_processing = false
    @uploader.remove!
  end


  describe 'jpeg' do
    include_examples "carrierwave local image processing" do
      let(:filepath) { Rails.root.join('spec','photos','jpeg_full.jpg').to_s }
    end
  end
  describe 'tiff' do
    include_examples 'carrierwave local image processing' do
      let(:filepath) { Rails.root.join('spec','photos','tiff_full.tif').to_s }
    end
  end
  describe '3FR' do
    include_examples 'carrierwave local image processing' do
      let(:filepath) { Rails.root.join('spec','photos','color_repeat.3FR').to_s }
    end
  end
  describe 'fff' do
    include_examples 'carrierwave local image processing' do
      let(:filepath) { Rails.root.join('spec','photos','fff_full.fff').to_s }
    end
  end
  describe 'NEF' do
    include_examples 'carrierwave local image processing' do
      let(:filepath) { Rails.root.join('spec','photos','nef_full.NEF').to_s }
    end
  end
  describe 'RAF' do
    include_examples 'carrierwave local image processing' do
      let(:filepath) { Rails.root.join('spec','photos','raf_full.RAF').to_s }
    end
  end
end

