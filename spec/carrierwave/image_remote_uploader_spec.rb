require 'spec_helper'
require 'carrierwave/test/matchers'

shared_examples "carrierwave remote image processing" do
  context 'the thumb version' do
    it "should scale down an image to fit with 150 by 150 pixels" do
      @uploader.thumb.should be_no_larger_than(150, 150)
    end
    it "is saved as a jpeg file" do
      expect(@uploader.thumb.filename).to match(/\.(jpeg|jpg)$/)
    end
  end

  context 'the medium version' do
    it "should scale down an image to fit within 900 by 900 pixels" do
      @uploader.medium.should be_no_larger_than(900, 900)
    end
    it "is saved as a jpeg file" do
      expect(@uploader.medium.filename).to match(/\.(jpeg|jpg)$/)
    end
  end

  context "the full version" do
    it "should be a jpeg version of the original" do
      exif = MiniExiftool.new(@uploader.model.image_remote.file.path)
      @uploader.medium.should be_no_larger_than(2100, 2100)
    end
  end
end

describe ImageRemoteUploader do
  include CarrierWave::Test::Matchers
  describe 'jpeg tiff fff nef 3fr' do
    before(:each) do
      ImageRemoteUploader.enable_processing = true
      ImageRemoteUploader.storage = :file
      img = FilelessIO.new(filepath)
      ci = FactoryGirl.create(:repeat_image_no_image, image: img, remote: "1")
      @image_uploader = ci.image #used just for cleaning up
      @uploader = ci.image_remote
    end

    after(:each) do
      ImageRemoteUploader.enable_processing = false
      @image_uploader.remove!
      @uploader.remove!
      ImageRemoteUploader.storage = :fog
    end

    describe 'jpeg' do
      include_examples "carrierwave remote image processing" do
        let(:filepath) { Rails.root.join('spec','photos','jpeg_full.jpg').to_s }
      end
    end
    describe 'tiff' do
      include_examples 'carrierwave remote image processing' do
        let(:filepath) { Rails.root.join('spec','photos','tiff_full.tif').to_s }
      end
    end

    describe 'fff' do
      include_examples 'carrierwave remote image processing' do
        let(:filepath) { Rails.root.join('spec','photos','fff_full.fff').to_s }
      end
    end
    describe '3FR' do
      include_examples 'carrierwave remote image processing' do
        let(:filepath) { Rails.root.join('spec','photos','color_repeat.3FR').to_s }
      end
    end
    describe 'NEF' do
      include_examples 'carrierwave remote image processing' do
        let(:filepath) { Rails.root.join('spec','photos','nef_full.NEF').to_s }
      end
    end
  end
end
