require 'spec_helper'

describe ImageReggy::AlignmentUploader do

  before(:each) do
    ImageReggy::AlignmentUploader.enable_processing = true
  end

  let(:derivative) do
    ci = FactoryGirl.build(:capture_image, :repeat_image, :color_jpeg)
    ci.process_image_upload = true
    ci.save!
    ci
  end

  let(:uploader) do
    ai = ImageReggy::AlignmentImage.new
    ai.image = FilelessIO.new(derivative.image.file.path)
    ai.derivative = derivative
    ai.save!
    ai.image
  end

  after(:each) do
    ImageReggy::AlignmentUploader.enable_processing = false
    uploader.remove!
  end

  it "should not have the derivative's UUID in it's name" do
    expect(uploader.file.path).to_not match(/_#{derivative.image_secure_token}/)
  end

end
