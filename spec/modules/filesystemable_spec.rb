require 'spec_helper'


describe Filesystemable do
  # describe "all ancestors are filesystemable" do
  #   let(:survey1) { FactoryGirl.create(:survey) }
  #   let(:survey2) { FactoryGirl.create(:survey) }
  #   let(:file) { FactoryGirl.create(:metadata_file, metadata_owner: survey1) }
  #   context "leaf is moved" do
  #     before(:each) do
  #       file.metadata_owner = survey2; file.save!
  #     end
  #     it "moves the file to the new filesystemable parent" do
  #       expect(file.filesystem_path).to match(/#{survey2.filesystem_path}/)
  #     end
  #     it "deletes the old parent folder that would otherwise be empty" do
  #       expect(Dir.exists?(survey1.filesystem_path)).to eq false
  #     end
  #     it "updates the fs_path" do
  #       expect(file.fs_path).to eq(file.filesystem_path)
  #     end
  #   end
  #   context "non-leaf is moved" do
  #     before(:each) do
  #       survey1.surveyor = survey2.surveyor; survey1.save!
  #     end
  #     it "moves the parent folder" do
  #       expect(file.filesystem_path).to match(/#{survey2.surveyor.filesystem_path}/)
  #       expect(file.filesystem_path).to_not match(/#{survey2.filesystem_path}/)
  #     end
  #   end
  # end



  # it "has the proper file/folder created under the parent folder" do
  #   if parent_folder.present?
  #     expect(subject.filesystem_path).to match(/#{parent_folder}\/#{subject.filesystem_name}/)
  #   else
  #     expect(subject.filesystem_path).to match(/\/#{subject.filesystem_name}/)
  #   end
  # end



  # end
  # it "can clean up a directory after an image delete even if a hidden file is in it" do
  #   capture_image = FactoryGirl.create(:repeat_image_color_jpeg)
  #   expect(File.exists?(capture_image.image.file.path)).to be_truthy
  #   parent_dir = capture_image.filesystem_dir
  #   FileUtils.touch(File.join(parent_dir,"._something"))
  #   expect(File.exists?(File.join(parent_dir,"._something"))).to be_truthy
  #   capture_image.destroy
  #   expect(Dir.exists?(parent_dir)).to_not be_truthy
  # end
  # describe "fs_path updates" do
  #   context "station changed" do
  #     it "updates the fs path of moveable descendents when updated" do
  #       capture1 = FactoryGirl.create(:capture_with_images)
  #       location = capture1.capture_owner
  #       location_photo1 = FactoryGirl.create(:location_image, image_owner: location)
  #       visit = location.visit
  #       field_note = FactoryGirl.create(:field_note, visit: visit)
  #       station = visit.station
  #       metadata_file1 = FactoryGirl.create(:metadata_file, metadata_owner: station)
  #       #create captures from a different parents, to make sure there fs_paths aren't changing
  #       capture2 = FactoryGirl.create(:capture_with_images)
  #       #get the number of moveable objects that match the station
  #       station.name = "NewName123"
  #       station.save!
  #       station.reload; metadata_file1.reload; field_note.reload; location_photo1.reload
  #       expect(File.exist?(station.reload.fs_path)).to be_truthy
  #       expect(station.fs_path).to match("NewName123")
  #       capture1.capture_images.each do |ci|
  #         expect(ci.fs_path).to match(station.fs_path)
  #       end
  #       capture2.capture_images.each do |ci|
  #         expect(ci.fs_path).to_not match(station.fs_path)
  #         expect(ci.fs_path).to be_present
  #       end
  #       expect(metadata_file1.fs_path).to match(station.fs_path)
  #       expect(field_note.fs_path).to match(station.fs_path)
  #       expect(location_photo1.fs_path).to match(station.fs_path)
  #     end
  #   end
  # end


end
