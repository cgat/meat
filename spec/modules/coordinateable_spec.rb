require 'spec_helper'
require 'rake'
load 'lib/tasks/populate.rake'

describe Coordinateable do
  describe "UTM coordinates" do
    describe "converting UTM coordinates to lat long" do
      it "can be done with the geoutm gem" do
        latlon = GeoUtm::UTM.new('11U',665790,5533495).to_lat_lon
        expect(latlon.to_s).to eq("49.930799N 114.689872W")
      end
    end
  end
  describe "converting DMS to decimal" do
      it "can handle trailing zeros on the decimal" do
        station = FactoryGirl.create(:station)
        station.lat_d = 51
        station.lat_m = 12
        station.lat_s = 13.0010
        station.lat_dir = 1
        station.long_d = 114
        station.long_m = 12
        station.long_s = 31.120
        station.long_dir = 1
        station.save!
        expect(station.lat).to be_present
        expect(station.long).to be_present
      end
    end
end
