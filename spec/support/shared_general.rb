#SHARED EXAMPLES REQUIRE SUBJECT#

shared_examples "unsorted captures association" do
  it "is able to have many unsorted captures" do
    FactoryGirl.create(:capture, capture_owner: subject)
    FactoryGirl.create(:capture, capture_owner: subject)
    subject.unsorted_captures.should have(2).captures
  end
  it "deletes all its unsorted captures if destroyed" do
    FactoryGirl.create(:capture, capture_owner: subject)
    FactoryGirl.create(:capture, capture_owner: subject)
    subject.destroy
    subject.unsorted_captures.should have(0).captures
  end
end

shared_examples "unsorted location photos assocation" do
  it "is able to have many unsorted location photos" do
    FactoryGirl.create(:location_image, image_owner: subject)
    FactoryGirl.create(:location_image, image_owner: subject)
    subject.unsorted_location_photos.should have(2).location_images
  end
  it "deletes all its unsorted location photos if destroyed" do
    FactoryGirl.create(:location_image, image_owner: subject)
    FactoryGirl.create(:location_image, image_owner: subject)
    subject.destroy
    subject.unsorted_location_photos.should have(0).location_images
  end
end

shared_examples "unsorted historic captures association" do
  it "is able to have many unsorted historic captures" do
    FactoryGirl.create(:historic_capture, capture_owner: subject)
    FactoryGirl.create(:historic_capture, capture_owner: subject)
    subject.unsorted_hcaptures.should have(2).historic_captures
  end
  it "deletes all its unsorted historic captures if destroyed" do
    FactoryGirl.create(:historic_capture, capture_owner: subject)
    FactoryGirl.create(:historic_capture, capture_owner: subject)
    subject.destroy
    subject.unsorted_hcaptures.should have(0).historic_captures
  end
end

shared_examples "metadata file association" do
  it "is able to have many metadata files" do
    FactoryGirl.create(:metadata_file, metadata_owner: subject)
    FactoryGirl.create(:metadata_file, metadata_owner: subject)
    subject.metadata_files.should have(2).metadata_files
  end
  it "deletes all its metadata files if destroyed" do
    FactoryGirl.create(:metadata_file, metadata_owner: subject)
    FactoryGirl.create(:metadata_file, metadata_owner: subject)
    subject.destroy
    subject.metadata_files.should have(0).metadata_files
  end
  it "is able to have many metadata files when the parent subject is new" do
    subject2 = FactoryGirl.build(subject.class.name.underscore)
    m1=FactoryGirl.build(:metadata_file, metadata_owner: nil)
    m2=FactoryGirl.build(:metadata_file, metadata_owner: nil)
    subject2.metadata_files << m1
    subject2.metadata_files << m2
    subject2.save!
    subject2.reload
    subject2.metadata_files.should have(2).metadata_files
  end

end

shared_examples "scenics association" do
  it "is able to have many scenic images" do
    FactoryGirl.create(:scenic_image, image_owner: subject)
    FactoryGirl.create(:scenic_image, image_owner: subject)
    subject.scenics.should have(2).scenic_images
  end
  it "deletes all its scenic images if destroyed" do
    FactoryGirl.create(:scenic_image, image_owner: subject)
    FactoryGirl.create(:scenic_image, image_owner: subject)
    subject.destroy
    subject.scenics.should have(0).scenic_images
  end
end

shared_examples "camera info metadata" do
  it "has an f-stop" do
    expect(subject.f_stop).to be_present
  end
  specify "an f-stop cannot be a string" do
    subject.f_stop="asdf"
    expect(subject).to_not be_valid
  end
  # specify "an invalid f-stop is invalid" do
  #     subject.f_stop = 13.8
  #     expect(subject).to_not be_valid
  #   end
  it "has an iso" do
    expect(subject.iso).to be_present
  end
  specify "an iso cannot be a string" do
    subject.iso = "asdf"
    expect(subject).to_not be_valid
  end
  # specify "an invalid iso is invalid" do
  #     subject.iso = 123
  #     expect(subject).to_not be_valid
  #   end
  it "has a shutter speed" do
    expect(subject.shutter_speed).to be_present
  end
  # specify "an invalid shutter speed is invalid" do
  #     subject.shutter_speed = "1/3828"
  #     expect(subject).to_not be_valid
  #   end
  it "has a focal length" do
    expect(subject.focal_length).to be_present
  end
  specify "a focal length cannot be a string" do
    subject.focal_length = "35mm"
    expect(subject).to_not be_valid
  end
  specify "a focal length cannot be negative" do
    subject.focal_length = -1
    expect(subject).to_not be_valid
  end
  it "has a camera" do
    expect(subject.camera).to be_present
  end
end
shared_examples "holder of capture images" do
  it "is able to have many capture images" do
    expect(subject).to have(num_of_images).capture_images
  end
  it "will delete the capture images when the #{subject.class.name} is destroyed" do
    subject.destroy
    expect(subject.capture_images).to have(0).capture_images
  end
end
shared_examples "general image" do
  it "should be valid" do
    subject.should be_valid
  end
  it "has a file size in the database" do
    subject.file_size.should_not be_blank
  end
  it "has a x dimension in pixels in the database" do
    subject.x_dim.should_not be_blank
  end
  it "has a y dimension in pixels in the database" do
    subject.y_dim.should_not be_blank
  end
  it "has a bit depth in the database" do
    subject.bit_depth.should_not be_blank
  end
end
shared_examples "locally stored image" do
  it "has file in the file structure" do
    File.exists?(subject.reload.image.file.path).should == true
    expect(subject.image.file.filename).to match(/#{subject.image_secure_token}/)
    expect(subject.image.medium.url).to match(/medium_.*#{subject.image_secure_token}/)
    expect(subject.image.thumb.url).to match(/thumb_.*#{subject.image_secure_token}/)
  end
  it "can recreate all the versions", :skip do
    version_timestamps=subject.reload.image.versions.values.map{|v| File.new(v.file.path).mtime}
    subject.process_image_upload=true #required for carrierwave-backgrounder when recreating versions
    subject.image.recreate_versions!
    expect(subject.reload.image.versions.values.map{|v| File.new(v.file.path).mtime}).to_not eq(version_timestamps)
  end
end
shared_examples "remotely stored image" do
  it "has a url for the remote image when uploaded with remote paramter" do
    subject.reload.image_remote.url.should match /.*\/test-bucket.*#{subject.image_secure_token}/
    subject.image_remote.medium.url.should match /.*\/test-bucket.*medium_.*#{subject.image_secure_token}/
    subject.image_remote.thumb.url.should match /.*\/test-bucket.*thumb_.*#{subject.image_secure_token}/
  end
end
shared_examples "an image that intially uploaded locally, but will be later be uploaded remotely" do
  it "has a url for when remote parameter set to 1 and saved" do
    subject.reload
    subject.remote = "1"
    subject.save!
    subject.reload.image_remote.url.should match /.*\/test-bucket.*/
  end
  it "after being saved remotely, can be deleted remotely" do
    subject.reload
    subject.remote = "1"
    subject.save!
    subject.remote = "0"
    subject.save!
    subject.reload.image_remote.url.should be_blank
  end
end
shared_examples "merging mergeable captures" do
    it "should merge successfully" do
      cap_attr_receiver = capture_receiver.attributes.reject{|k,v| ['created_at','updated_at','id','capture_owner_type','capture_owner_id','condition'].include?(k) }
      cap_attr_argument = capture_argument.attributes.reject{|k,v| ['created_at','updated_at','id','capture_owner_type','capture_owner_id','condition'].include?(k) }
      cap_attributes = cap_attr_receiver.merge(cap_attr_argument) {|key, v_rec, v_arg| value = v_arg.nil? || v_arg=="" ? v_rec : v_arg }
      cap_images_receiver = capture_argument.capture_images
      cap_images_argument = capture_receiver.capture_images
      comparisons_receiver = capture_argument.comparisons
      comparisons_argument = capture_receiver.comparisons
      capture_receiver.merge(capture_argument)
      capture_receiver.reload
      cap_attributes.each do |name,value|
        if value.class==ActiveSupport::TimeWithZone
          [name,capture_receiver.send(name).to_i].should =~([name,value.to_i])
        else
          [name,capture_receiver.send(name)].should =~([name,value])
        end
      end
      (cap_images_receiver+cap_images_argument).each do |ci|
        expect(capture_receiver.capture_images).to include(ci)
      end
      (comparisons_receiver+comparisons_argument).each do |comp|
        expect(capture_receiver.comparisons).to include(comp)
      end
    end
  end
  shared_examples "mergeing unmergeable captures" do
    it "should fail to merge captures" do
      expect(capture_receiver.merge(capture_argument)).to be_falsey
    end
  end

#END=SHARED EXAMPLES REQUIRE SUBJECT=END#
