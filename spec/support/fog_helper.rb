Fog.mock!
fog_credentials = Fog.symbolize_credentials(YAML.load(ERB.new(File.read(Rails.root.join('config/fog_credentials.yml'))).result)["default"])
Fog.credentials=fog_credentials
connection = Fog::Storage.new(:provider => 'AWS')
connection.directories.create(:key => 'test-bucket')
