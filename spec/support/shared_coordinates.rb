shared_examples_for "objects that can have coordinates in decimal format" do
  
  it "is able to receive coordinates" do
    expect(subject).to be_valid
  end
  
  specify "that a latitude over 90 degrees is invalid" do
    subject.lat = 90.1
    expect(subject).to_not be_valid
  end 
  
  specify "that a latitude under -90 degrees is invalid" do
    subject.lat = -90.1
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude over 180 is invalid" do
    subject.long = 180.1
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude under -180 is invalid" do
    subject.long = -180.1
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude cannot be loaded without a longitude" do
    subject.lat = 71
    subject.long = nil
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude cannot be loaded without a latitude" do
    subject.lat = nil
    subject.long = 71
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude numeric string will be properly saved as a float" do
    subject.lat = -71.123.to_s
    subject.long = 81
    subject.save!
    fresh = subject.class.find(subject.id) #I found that subject.reload wasn't reloading from the db
    expect(fresh.lat).to eq(-71.123)
  end
  
  specify "that a longitude numeric string will be properly saved as a float" do
    subject.long = 71.123.to_s
    subject.lat = 81
    subject.save!
    fresh = subject.class.find(subject.id) #I found that subject.reload wasn't reloading from the db
    expect(fresh.long).to eq(71.123)
  end
  
end

shared_examples_for "objects that can have coordinates in DMS format" do
  
  it "is able to receive coordinates in DMS (Degree Minute Second) format" do
    expect(subject).to be_valid
  end
  
  it "is able to receive coordinates in DMS that are numeric strings" do
    [subject.lat_d,subject.lat_m,subject.lat_s,subject.lat_dir, subject.long_d,subject.long_m,subject.long_s, subject.long_dir].each do |coord_var|
      coord_var = coord_var.to_s
    end
    expect(subject).to be_valid
  end
  
  specify "that a latitude degree over 90 degrees is invalid when using the DMS format" do
    subject.lat_d = 91
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude degree under 0 degrees is invalid when using the DMS format" do
    subject.lat_d = -1
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude minute over 60 minutes is invalid when using the DMS format" do
    subject.lat_m = 61
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude minute under 0 minutes is invalid when using the DMS format" do
    subject.lat_m = -1
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude second over 60 seconds is invalid when using the DMS format" do
    subject.lat_s = 61
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude second under 0 seconds is invalid when using the DMS format" do
    subject.lat_s = -1
    expect(subject).to_not be_valid
  end
  
  specify "that a latitude cardinal direction South (2) results in a negative decimal formatted coordinate" do
    subject.lat_dir = 2
    subject.save!
    expect(subject.lat).to be < 0
  end
  
  specify "that a latitude cardinal direction North (1) results in a positive decimal formatted coordinate" do
    subject.lat_dir = 1
    subject.save!
    expect(subject.lat).to be > 0
  end
  
  specify "that a longitude degree over 180 is invalid when using DMS format" do
    subject.long_d = 181
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude degree under 0 is invalid when using DMS format" do
    subject.long_d = -1
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude minute under 0 minutes is invalid when using DMS format" do
    subject.long_m = -1
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude minute over 60 minutes is invalid when using DMS format" do
    subject.long_m = 61
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude second under 0 is seconds invalid when using DMS format" do
    subject.long_s = -1
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude second over 60 is seconds invalid when using DMS format" do
    subject.long_s = 61
    expect(subject).to_not be_valid
  end
  
  specify "that a longitude cardinal direction West (2) results in a negative decimal formatted coordinate" do
    subject.long_dir = 2
    subject.save!
    expect(subject.long).to be < 0
  end
  
  specify "that a longitude cardinal direction East (1) results in a positive decimal formatted coordinate" do
    subject.long_dir = 1
    subject.save!
    expect(subject.long).to be > 0
  end
  
  specify "that the user longitude DMS inputs are properly converted into decimal format" do
    long_dec = -80.72916166666667
    subject.long_d = 80
    subject.long_m = 43
    subject.long_s = 44.982
    subject.long_dir = 2
    subject.save!
    expect(subject.long).to eq(long_dec)
  end
  
  specify "that the user longitude DMS inputs, as numeric strings, are properly converted into decimal format" do
    long_dec = -80.72916166666667
    subject.long_d = 80.to_s
    subject.long_m = 43.to_s
    subject.long_s = 44.982.to_s
    subject.long_dir = 2.to_s
    subject.save!
    expect(subject.long).to eq(long_dec)
  end
  
  specify "that a longitude degree input remains unchanged after saving and retrieving from db" do
    subject.long_d = 81
    subject.save!
    subject.reload
    expect(subject.long_d).to eq(81) 
  end
  
  specify "that a longitude minute input remains unchanged after saving and retrieving from db" do
    subject.long_m = 12
    subject.save!
    subject.reload
    expect(subject.long_m).to eq(12)
  end
  
  specify "that a longitude second input remains unchanged after saving and retrieving from db" do
    subject.long_s = 53.162345
    subject.save!
    subject.reload
    expect(subject.long_s).to eq(53.162345)
  end
  
  
  specify "that the user latitude DMS inputs are properly converted into decimal format" do
    lat_dec = -70.20392305555555
    subject.lat_d = 70
    subject.lat_m = 12
    subject.lat_s = 14.123
    subject.lat_dir = 2
    subject.save!
    expect(subject.lat).to eq(lat_dec)
  end
  
  specify "that the user latitude DMS inputs, as numeric strings, are properly converted into decimal format" do
    lat_dec = -70.20392305555555
    subject.lat_d = 70.to_s
    subject.lat_m = 12.to_s
    subject.lat_s = 14.123.to_s
    subject.lat_dir = 2.to_s
    subject.save!
    expect(subject.lat).to eq(lat_dec)
  end
  
  specify "that elevation is required to be a number" do
    subject.elevation = "hello123"
    expect(subject).to_not be_valid
  end
  
  specify "that seconds cannot have #{Coordinateable::NUM_DECIMAL_PLACES+1} decimal places" do
    subject.lat_s = 10.1234567
    expect(subject).to_not be_valid
  end
  
  specify "that seconds can have #{Coordinateable::NUM_DECIMAL_PLACES} decimal places" do
    subject.lat_s = 10.123456
    expect(subject).to be_valid
  end
  
end

shared_examples_for "objects that can have coordinates in MinDec format" do
  it "is able to receive coordinates in MinDec format" do 
    expect(subject).to be_valid
  end
  
  specify "that seconds cannot have #{Coordinateable::NUM_DECIMAL_PLACES+1} decimal places" do
    subject.lat_m = 10.1234567
    expect(subject).to_not be_valid
  end
  
  specify "that seconds can have #{Coordinateable::NUM_DECIMAL_PLACES} decimal places" do
    subject.lat_m = 10.123456
    expect(subject).to be_valid
  end
  
  
  it "is able to retrieve coordinates in MinDec format"
  
end

shared_examples_for "objects that can have coordinates in UTM format" do
  
  it "is able to receive UTM coordinates" do
    latlong = GeoUtm::UTM.new(subject.zone,subject.easting,subject.northing).to_lat_lon
    lat = latlong.lat
    long = latlong.lon
    expect(subject.lat).to eq(lat)
    expect(subject.long).to eq(long)
  end
  
end
  
shared_examples_for "bad input of coordinates configurations" do
  
  specify "that degree = <float>, minute = <float>, second = <float> is invalid" do
    subject.lat_d = 12.1
    subject.lat_m = 12.1
    subject.lat_s = 34.1
    subject.lat_dir = 1
    subject.long_d = 31.13
    subject.long_m = 12.1
    subject.long_s = 21.1
    subject.long_dir = 1
    expect(subject).to_not be_valid
  end
  
  specify "that degree = <int>, minute = <float>, second = <float> is invalid" do
    subject.lat_d = 12
    subject.lat_m = 12.1
    subject.lat_s = 34.1
    subject.lat_dir = 1
    subject.long_d = 31
    subject.long_m = 12.1
    subject.long_s = 21.1
    subject.long_dir = 1
    expect(subject).to_not be_valid
  end
  
  specify "that degree = <int>, minute = <float>, second = <int> is invalid" do
    subject.lat_d = 12
    subject.lat_m = 12.1
    subject.lat_s = 34
    subject.lat_dir = 1
    subject.long_d = 31
    subject.long_m = 12.1
    subject.long_s = 21
    subject.long_dir = 1
    expect(subject).to_not be_valid
  end
  
  specify "that degree = <float>, minute = <float>, second = nil is invalid" do 
    subject.lat_d = 12.1
    subject.lat_m = 12.1
    subject.lat_s = nil
    subject.lat_dir = 1
    subject.long_d = 31.13
    subject.long_m = 12.1
    subject.long_s = nil
    subject.long_dir = 1
    expect(subject).to_not be_valid
  end 
  
  specify "that degree = <float>, minute = <int>, second = nil is invalid" do
    subject.lat_d = 12.1
    subject.lat_m = 12
    subject.lat_s = nil
    subject.lat_dir = 1
    subject.long_d = 31.13
    subject.long_m = 12
    subject.long_s = nil
    subject.long_dir = 1
    expect(subject).to_not be_valid
  end
  
end