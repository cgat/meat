=begin
  In our application we have several models that include the Filesystemable module.
  This module is a container for methods related to making sure that each of these objects
  has a place within a dynamic file structure. 
  The shared_examples below are to be shared with specs on models that are Filesystemable.
  These shared examples expect a block to be passed in that will initailize the parents,owner_attribute, 
  and subject variables.
  -parents: parents should be an array of strings that can be used to create (using a factory) 
  for all possible parents of the filesystemable_obj. Example: the parents of a capture are 
  ["visit","location","survey","survey_season"]
  -owner_attribute: each filesystem will have a belongs_to type relationship defined. The name
  of this relationship is the owner_attribute. Example: the owner_attribute of a capture is capture_owner
  -filesystemable_obj: an instance of the object being tested.
  The argument block should define these using "let".
=end
shared_examples_for "moving filesystemable objects" do
  it "will move the file successfully" do
    parents.each do |parent|
      old_path_to_obj = subject.filesystem_path
      subject.send(owner_attribute+"=", FactoryGirl.create(parent))
      #Below is a hack. move_filesystem_obj is not being called with the callback and thus I have to manuaully call it here
      #subject.move_filesystem_object
      subject.save!
      subject.should be_valid
      subject.filesystem_path.should_not == old_path_to_obj
    end
  end
end
shared_examples_for "naming filesystemable objects" do |parent_folder|
  it "has the proper file/folder created under the parent folder" do
    if parent_folder.present?
      expect(subject.filesystem_path).to match(/#{parent_folder}\/#{subject.filesystem_name}/)
    else
      expect(subject.filesystem_path).to match(/\/#{subject.filesystem_name}/)
    end
  end
end