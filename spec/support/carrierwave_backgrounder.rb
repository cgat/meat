def wait_until_processed(obj,mount_column)
  while obj.send(mount_column.to_s+"_tmp").present?
    sleep(0.5)
  end
end