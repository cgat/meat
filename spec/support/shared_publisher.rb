shared_examples_for "a Publisher" do |is_root|
  before(:each) do
    ####
    # This hack fixes and issue where carrierwave does not clear
    # it's cache on a reload. The carrierwave_backgrounder gem
    # looks at the cache_name to see if it can process the file
    # (as a fallback). When we reload the subject the file is processed
    # but carrierwave_backgrounder thinks it should try again, but fails
    # because the file is not there. In carrierwave version 1, this
    # is fixed, however, that version introducing numerous changes that
    # affect both backgrounder and carrierwave-meta. Once all these
    # gems updated to work with version 1, this before statement can
    # be removed.
    def subject.reload
      @_mounters = nil
      super
    end
  end
  describe "#published?" do
    it { should respond_to(:published?) }
    it "functions" do
      subject.reload.published=true
      expect(subject.published).to be_truthy
    end
  end
  describe "#publish" do
    it { should respond_to(:publish) }
    it "publishes" do
      #reloaded_subject = fresh_reload(subject)
      subject.reload
      subject.publish
      expect(subject.published?).to be_truthy
    end
  end
  describe "#unpublish" do
    it { should respond_to(:unpublish) }
    it "unpublishes" do
      subject.reload.publish
      subject.unpublish
      expect(subject.published?).to be_falsey
    end
  end
  describe "#unpublish_ancestors"  do
    it { should respond_to(:unpublish_ancestors) }
    it "unpublishes it's parent" do
      subject.publish_ancestors
      #needs to be reloaded in order in case of capture image.
      subject.reload
      #ensure the subject is unpublished (ie publishing a capture will publish its capture_image)
      subject.unpublish
      subject.reload.unpublish_ancestors
      expect(subject.parent.published?).to be_falsey
    end
  end unless is_root

  describe "#publish_ancestors" do
    it { should respond_to(:publish_ancestors) }
    it "publishes it's parent" do
      subject.publish_ancestors
      expect(subject.parent.published?).to be_truthy
    end
  end unless is_root

  def fresh_reload(subject)
    klass = subject.class
    id = subject.id
    temp = klass.send(:find, id)
    temp
  end
end
