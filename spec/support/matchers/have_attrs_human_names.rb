RSpec::Matchers.define :have_attrs_human_names do |expected,model|
  @error_attr
  match do |actual|
    result = true
    expected.each do |k,v|
      if !actual.has_text?(model.human_attribute_name(k.to_s))
        @error_attr = k.to_s
        result = false
        break
      end
    end
    result
  end

  failure_message_for_should do |actual|
    "expected that page (capybara session) would have attributte #{model.human_attribute_name(@error_attr)}"
  end


  description do
    "have all the attributes in the attribute hash"
  end
end