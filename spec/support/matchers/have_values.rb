RSpec::Matchers.define :have_text_values do |expected,value_type,model|
  @error_value
  match do |actual|
    result = true
    expected.each do |field,field_hash|
      next if field_hash[value_type].nil?
      if field_hash[value_type].class==Date || field_hash[value_type].class==DateTime || field_hash[value_type].class==Time || field_hash[value_type].class==ActiveSupport::TimeWithZone
        if (field_hash[value_type].class==Date || field_hash[value_type].class==DateTime) && !actual.has_text?(field_hash[value_type].to_s(:long))
          @error_value = field_hash[value_type].to_s(:long)
          result = false
        elsif (field_hash[value_type].class==Time || field_hash[value_type].class==ActiveSupport::TimeWithZone) && !actual.has_text?(field_hash[value_type].to_s(:time))
          @error_value = field_hash[value_type].to_s(:time)
          result = false
        end
      elsif !actual.has_text?(field_hash[value_type].to_s)
        @error_value = field_hash[value_type].to_s
        result = false
      end
      if result==false
        @error_attr = field.to_s
        break
      end
    end
    result
  end

  failure_message_for_should do |actual|
    "expected that page (capybara session) would have value #{@error_value} for attribute #{model.human_attribute_name(@error_attr)}"
  end


  description do
    "have all the attributes in the attribute hash"
  end
end

RSpec::Matchers.define :have_field_values do |expected,value_type,model|
  @error_value
  match do |actual|
    result = true
    expected.each do |field,field_hash|
      next if field_hash[value_type].nil?
      case field_hash[:input_type]
      when 'select'
        if  field_hash[value_type].class==DateTime || field_hash[value_type].class==Time || field_hash[value_type].class==ActiveSupport::TimeWithZone || field_hash[value_type].class==Date
          if (field_hash[value_type].class==Date || field_hash[value_type].class==DateTime) && \
            (!actual.has_select?(get_form_node_name(field, model, [], "1i"), selected: field_hash[value_type].year.to_s) || \
            !actual.has_select?(get_form_node_name(field, model, [], "2i"), selected: Date::MONTHNAMES[field_hash[value_type].month]) || \
            !actual.has_select?(get_form_node_name(field, model, [], "3i"), selected: field_hash[value_type].day.to_s) )
            @error_value = field_hash[value_type].to_s(:long)
            result = false
          end
          if (field_hash[value_type].class==Time || field_hash[value_type].class==ActiveSupport::TimeWithZone || field_hash[value_type].class==DateTime) && \
            (!actual.has_select?(get_form_node_name(field, model, [], "4i"), selected: sprintf('%02d',field_hash[value_type].hour.to_s)) || \
            !actual.has_select?(get_form_node_name(field, model, [], "5i"), selected: sprintf('%02d',field_hash[value_type].min.to_s)) )
            @error_value = field_hash[value_type].to_s(:time)
            result = false
          end
        elsif !actual.has_select?(get_form_node_name(field, model, [], ""), \
            selected: field_hash[value_type].class=='Array' ? field_hash[value_type].map(&:to_s) : field_hash[value_type].to_s)
          @error_value = field_hash[value_type].to_s
          result = false
        end
      when 'text'
        if actual.find_field(get_form_node_name(field, model, [], "")).value != field_hash[value_type].to_s 
          @error_value = field_hash[value_type].to_s
          result = false
        end
      when 'checkbox'
        if (actual.has_checked_field?(get_form_node_name(field, model, [], "")) && field_hash[value_type]==false) || \
          (!actual.has_checked_field?(get_form_node_name(field, model, [], "")) && field_hash[value_type]==true) 
          @error_value = field_hash[value_type].to_s
          result=false
        end
      when 'attached_file'
       #do nothing
      end
      if result==false
        @error_attr = field.to_s
        break
      end
    end
    result
  end

  failure_message_for_should do |actual|
    "expected that page (capybara session) would have value #{@error_value} for attribute #{model.human_attribute_name(@error_attr)}"
  end


  description do
    "have all the attributes in the attribute hash"
  end
end