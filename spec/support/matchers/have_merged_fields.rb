RSpec::Matchers.define :have_merged_fields do |merger,mergee|
  @error_column
  match do |actual|
    result = true
    #fn photo reference
    Capture.column_names.reject{|c|["id","created_at", "updated_at","capture_owner_type","capture_owner_id"]}.each do |col|
      if merger.send(col).present? || mergee.send(col).present?
        if col=="lat" && actual.has_no_text(merger.send("lat_formatted_DMS") || "noText") && actual.has_no_text(mergee.send("lat_formatted_DMS") || "noText") 
          @error_column = col 
        elsif col=="long" && actual.has_no_text(merger.send("long_formatted_DMS") || "noText") && actual.has_no_text(mergee.send("long_formatted_DMS") || "noText") 
          @error_column = col  
        elsif col=="camera_id" && actual.has_no_text(merger.send(col).present? ? Camera.find_by_id(merger.send(col)) : "noText") && actual.has_no_text(mergee.send(col).present? ? Camera.find_by_id(mergee.send(col)) : "noText")
          @error_column = col
        elsif col=="lens_id" && actual.has_no_text(merger.send(col).present? ? Lens.find_by_id(merger.send(col)) : "noText") && actual.has_no_text(mergee.send(col).present? ? Lens.find_by_id(mergee.send(col)) : "noText")
          @error_column = col
        elsif actual.has_no_text(merger.send(col).to_s) && actual.has_no_text(mergee.send(col).to_s)
          @error_column = col
          break
        end
      end
    end
  end
  
  
  failure_message_for_should do |actual|
    "Captures not merged properly for attribute #{@error_column}."
  end
  
  description do
    "have_merged_fields takes two capture which should have been merged into one. It checks to see that all the fields, from the mergee or merger are present in the current page. "
  end
  
end