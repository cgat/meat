def login_as_admin
  admin = FactoryGirl.create(:user)
  visit new_user_session_path
  fill_in "Email", with: admin.email
  fill_in "Password", with: admin.password
  click_button "Sign in"
end

def logout
  click_link "Logout"
end
