include NumberHelper

shared_examples_for "object show" do
  scenario "page has the correct title", :broken do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}"
    #expect(page).to have_selector('title', text: "MLP Editing and Administering Tool | #{model.model_name.human.titleize}")
    expect(page).to have_title("MLP Editing and Administering Tool | #{model.model_name.human.titleize}")
  end
  scenario "has all the attributes" do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}"
    expect(page).to have_attrs_human_names(attr_hash, model)
  end
  scenario "has all the attribute values" do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}"
    expect(page).to have_text_values(attr_hash,:good_value, model)
  end
end

shared_examples_for "object creation" do
  scenario "page has the correct title", :broken do
    if defined?(parent_model)
      visit "/#{parent_model.to_s.underscore.pluralize}/#{parent_model.first.id}/#{model.to_s.underscore.pluralize}/new"
    else
      visit "/#{model.to_s.underscore.pluralize}/new"
    end
    #expect(page).to have_selector('title', text: "MLP Editing and Administering Tool | Add New #{model.model_name.human.titleize}") #doesn't work with the current version of capybara
    expect(page).to have_title("MLP Editing and Administering Tool | Add New #{model.model_name.human.titleize}")
  end
  scenario "by filling out form with good values and submitting" do
    if defined?(parent_model)
      visit "/#{parent_model.to_s.underscore.pluralize}/#{parent_model.first.id}/#{model.to_s.underscore.pluralize}/new"
    else
      visit "/#{model.to_s.underscore.pluralize}/new"
    end
    fill_out_form(:good_value)
    page.first(:xpath, "//input[@value='Submit']").click#click_button "Submit"
    expect(page).to have_no_text("Invalid")
    expect(page).to have_selector("a.brand", text: "#{model.to_s.titleize}")
    expect(page).to have_no_selector("a.brand", text: "Add New")
    expect(page).to have_no_selector("a.brand", text: "Edit")
    expect(page).to have_text_values(attr_hash,:good_value, model)
  end
  scenario "by filling out form with invalid values and submitting" do
    attr_hash.each do |k,v|
      next if v[:invalid_value].nil?
      if defined?(parent_model)
        visit "/#{parent_model.to_s.underscore.pluralize}/#{parent_model.first.id}/#{model.to_s.underscore.pluralize}/new"
      else
        visit "/#{model.to_s.underscore.pluralize}/new"
      end
      fill_out_form(:good_value)
      fill_out_field(k,v[:invalid_value],v[:input_type])
      page.first(:xpath, "//input[@value='Submit']").click
      expect(page).to have_text("Invalid")
      expect(page).to have_selector("a.brand", text: "Add New")
    end
  end
end

shared_examples_for "object edit" do
  scenario "page has the correct title", :broken do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}/edit"
    #expect(page).to have_selector('title', text: "MLP Editing and Administering Tool | Edit #{model.model_name.human.titleize}")
    expect(page).to have_title("MLP Editing and Administering Tool | Edit #{model.model_name.human.titleize}")
  end
  scenario "has all necessary attributes when page is loaded" do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}/edit"
    expect(page).to have_attrs_human_names(attr_hash, model)
  end
  scenario "has all necessary values when page is loaded" do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}/edit"
    expect(page).to have_field_values(attr_hash, :good_value, model)
  end
  scenario "by editing the values in the form with new values" do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}/edit"
    fill_out_form(:alternate_value)
    page.first(:xpath, "//input[@value='Submit']").click
    expect(page).to have_no_text("Invalid")
    expect(page).to have_selector("a.brand", text: "#{model.to_s.titleize}")
    expect(page).to have_no_selector("a.brand", text: "Add New")
    expect(page).to have_no_selector("a.brand", text: "Edit")
    expect(page).to have_text_values(attr_hash,:alternate_value, model)
  end

  scenario "by editing the values in the form with invalid values" do
    attr_hash.each do |k,v|
      next if v[:invalid_value].nil?
      visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}/edit"
      fill_out_field(k, v[:invalid_value], v[:input_type])
      page.first(:xpath, "//input[@value='Submit']").click
      #debugger if page.has_no_text?("Invalid")
      expect(page).to have_text("Invalid")
      expect(page).to have_selector("a.brand", text: "Edit #{model.to_s.titleize}")
    end
  end
end

shared_examples_for "object destroy" do
  scenario "by clicking the delete link on the show page with javascript", js: true, driver: :webkit do
    VIPS::thread_shutdown()
    obj = model.last
    visit "/#{model.to_s.underscore.pluralize}/#{obj.id}"
    test = accept_confirm do
      click_link('object_delete')
    end
    #page.driver.browser.switch_to.alert.accept
    if model.model_name.human=='Surveyor' || model.model_name.human=='Project'
      expect(page).to have_selector("a.brand", text: "Home")
    elsif model.model_name.human=='CaptureImage'
      expect(page).to have_selector("a.brand", text: "Capture")
    else
      expect(page).to have_selector("a.brand", text: "#{obj.filesystem_parent.class.name.titleize}")
    end
    #note: by putting this after the above have_selectors, we take advantage of capybara's builtin javascript delay
    expect{ model.find(obj.id) }.to raise_error
  end
end

shared_examples_for "adding a child object" do |child_model|
  scenario "by clicking the add child link" do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}"
    click_link("Add #{child_model.model_name.human.titleize}")
    expect(page).to have_selector("a.brand", text: "Add New #{child_model.model_name.human.titleize}")
    expect(current_path).to eq("/#{model.to_s.underscore.pluralize}/#{model.last.id}/#{child_model.model_name.collection.underscore.pluralize}/new")
  end
end

shared_examples "js tree" do
  scenario "the object associated with the data-pane is selected in the tree", js: true do
    visit "/#{model.to_s.underscore.pluralize}/#{model.last.id}"
    using_wait_time(20) { expect(page).to have_selector("li##{model.model_name.name}_#{model.last.id.to_s}")  }
  end
  scenario "the parent folder of the data-pane object is open", js: true do
    obj = model.last
    if obj.filesystem_parent!=nil
      visit "/#{model.to_s.underscore.pluralize}/#{obj.id}"
      parent = obj.filesystem_parent
      using_wait_time(20) { expect(page).to have_selector("##{parent.class.name}_#{parent.id}")}
      expect(page.find("##{parent.class.name}_#{parent.id}")[:class]).to include('jstree-open')
    end
  end
end


### Helper methods ###
def fill_out_field(field, value, input_type, nestings=[])
  case input_type #field_hash[:input_type]
  when 'select'
    if value.class==DateTime || value.class==Time || value.class==ActiveSupport::TimeWithZone || value.class==Date
      if value.class!=Date
        hour_node_id = get_form_node_name(field.to_s, model, nestings, "4i") #{}"#{model.to_s.underscore}_#{field.to_s}_4i"
        hour_value = value.hour
        select sprintf('%02d',hour_value), from: hour_node_id
        minute_node_id = get_form_node_name(field.to_s, model, nestings, "5i") #{}"#{model.to_s.underscore}_#{field.to_s}_5i"
        minute_value = value.min
        select sprintf('%02d',minute_value), from: minute_node_id
      end
      if value.class!=Time && value.class!=ActiveSupport::TimeWithZone
        year_node_id = get_form_node_name(field.to_s, model, nestings, "1i") #{}"#{model.to_s.underscore}_#{field.to_s}_1i"
        year_value = value.year.to_s
        select year_value, from: year_node_id
        month_node_id = get_form_node_name(field.to_s, model, nestings, "2i") #{}"#{model.to_s.underscore}_#{field.to_s}_2i"
        month_value = Date::MONTHNAMES[value.month]
        select month_value, from: month_node_id
        day_node_id = get_form_node_name(field.to_s, model, nestings, "3i") #{}"#{model.to_s.underscore}_#{field.to_s}_3i"
        day_value = value.day.to_s
        select day_value, from: day_node_id
      end
    elsif value.class==Array
      value.each do |i|
        select i, from: get_form_node_name(field.to_s, model, nestings, "")
      end
    else
      select value, from: get_form_node_name(field.to_s, model, nestings, "")
    end
  when 'text'
    fill_in get_form_node_name(field.to_s, model, nestings, ""), with: value
  when 'checkbox'
    if value==true
      check get_form_node_name(field.to_s, model, nestings, "")
    else
      uncheck get_form_node_name(field.to_s, model, nestings, "")
    end
  when 'attached_file'
    attach_file get_form_node_name(field.to_s, model, nestings, ""), value
  end
end

def fill_out_form(value_type, nestings=[])
  attr_hash.each do |field, field_hash|
    next if field_hash[value_type].blank?
    fill_out_field(field,field_hash[value_type],field_hash[:input_type], nestings)
  end
end


#attribute
#model is the model of the top level form.
#nested_models is an array of the arrays [[nested_model_class, 0], [nested_model_class, 0]]
#the number is associated with the x'th iteration of the nested attribute in the form. Only used for nested attrs
#suffix is used for appending date identifiers (1i,2i, etc)
def get_form_node_id(attribute, model, nested_models, suffix="")
  return model.human_attribute_name(attribute) if attribute.to_s=~/_id/
  node_id = "#{model.to_s.underscore}"
  nested_models.each do |level|
    node_id = node_id+"_#{level[0].to_s.underscore.pluralize}_attributes_#{level[1].to_s}"
  end
  node_id = node_id+"_#{attribute}_#{suffix}"
  node_id.chomp("_")
end

def get_form_node_name(attribute,model,nested_models, suffix="")
  return model.human_attribute_name(attribute) if attribute.to_s=~/_id/
  node_id = "#{model.to_s.underscore}"
  nested_models.each do |level|
    node_id = node_id+"[#{level[0].to_s.underscore.pluralize}_attributes][#{level[1].to_s}]"
  end
  if suffix.blank?
    node_id = node_id+"[#{attribute}]"
  else
    node_id = node_id+"[#{attribute}(#{suffix})]"
  end
end

def factory_to_attr_hash(factory, build_type, options={})
  if build_type==:build
    attr_hash_good_values = build_attributes(factory)
  elsif build_type==:create
    attr_hash_good_values = create_attributes(factory)
  else
    raise ArguementError, "build_type can be either :build or :create"
  end
  if options.present? && options.has_key?(:reject_attrs)
    attr_hash_good_values.reject!{ |k,v| options[:reject_attrs].include?(k) }
  end

  attr_hash_invalid_values = build_attributes(factory+"_invalid")
  attr_hash_alternate_values = build_attributes(factory+"_alternate")
  attr_hash = {}
  attr_hash_good_values.each do |k,v|
    if v.class==String
      attr_hash[k] = {good_value: v, invalid_value: attr_hash_invalid_values[k], alternate_value: attr_hash_alternate_values[k] }
      attr_hash[k][:input_type] = 'text'
    elsif k.to_s=~/_id/
      #this assumes that association foreign keys have _id on the end.
      klass = get_belongs_to_class(k,model)#k.to_s.chomp("_id").singularize.camelcase.constantize
      attr_hash[k] = {good_value: klass.find(v).display_name, invalid_value: nil, alternate_value: nil }
      attr_hash[k][:input_type] = 'select'
    elsif v.class==Date  || v.class==DateTime || v.class==Time  || v.class==ActiveSupport::TimeWithZone
      attr_hash[k] = {good_value: v, invalid_value: nil, alternate_value: attr_hash_alternate_values[k] }
      attr_hash[k][:input_type] = 'select'
    elsif !!v==v #check if its a Boolean
      attr_hash[k] = {good_value: v, invalid_value: nil, alternate_value: !v }
      attr_hash[k][:input_type] = 'checkbox'
    elsif v.class==FilelessIO || v.class==Rack::Test::UploadedFile
      attr_hash[k] = {good_value: Rails.root.join("spec","photos","bw_historic.jpg"), invalid_value: nil, alternate_value: nil }
      attr_hash[k][:input_type] = 'attached_file'
    elsif v.class==Fixnum || v.class==Float
      attr_hash[k] = {}
      attr_hash[k][:good_value] = integer?(v) ? v.to_i : v.to_f
      attr_hash[k][:invalid_value] = integer?(attr_hash_invalid_values[k]) ? attr_hash_invalid_values[k].to_i : attr_hash_invalid_values[k].to_f
      attr_hash[k][:alternate_value] = integer?(attr_hash_alternate_values[k]) ? attr_hash_alternate_values[k].to_i : attr_hash_alternate_values[k].to_f
      attr_hash[k][:input_type] = 'text'
    else
      raise StandardError, "You forgot one of the input types"
    end
  end
  attr_hash.delete_if {|k,v| [:lat,:long].member?(k) }
end

def get_belongs_to_class(foreign_key,model)
  raise ArguementError, "Foreign key must be a symbol" if foreign_key.class!=Symbol
  asses = model.reflect_on_all_associations(:belongs_to)
  klass = nil
  asses.each do |ass|
    if ass.foreign_key.to_sym==foreign_key || ass.foreign_key==foreign_key #options.has_key?(foreign_key)
      klass = ass.klass
    # elsif "#{ass.name}_id"==foreign_key.to_s
    #   klass = ass.klass
    end
  end
  if klass.nil?
    raise StandardError, "The foreign key #{foreign_key.to_s} is not related to a belongs to association in the model #{model.model_name.human}"
  else
    return klass
  end
end
