#this function will open the ancestor folders of the object until the object is revealed (jstree)
def open_node_parents(obj)
  relatives = get_relatives(obj)
  if relatives.present?
    while relatives.present?
      oldest_member = relatives.pop
      if oldest_member.class.name =='Surveyor' && page.has_css?("#Surveyors_root.jstree-closed")
        page.find_by_id("Surveyors_root").first(".jstree-icon").click
      elsif oldest_member.class.name == 'Project' && page.has_css?("#Projects_root.jstree-closed")
        page.find_by_id("Projects_root").first(".jstree-icon").click
      end
      oldest_member_attr_id = oldest_member.class.name+"_"+oldest_member.id.to_s
      if page.has_css?("##{oldest_member_attr_id}.jstree-closed")
        jstree_icon = page.find_by_id(oldest_member_attr_id).first(".jstree-icon")
        jstree_icon.click
      end
      #if it's not there yet, try again
      if !page.has_selector?("##{oldest_member_attr_id}")
        page.find_by_id(oldest_member_attr_id).first(".jstree-icon").click if page.has_css?("##{oldest_member_attr_id}.jstree-closed")
      end
    end
  end
end



