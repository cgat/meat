#edit
# before(:each) do
#   @visit = FactoryGirl.create(:visit_with_metadata)
#   visit edit_visit_path(@visit.id)
# end
#requires a before block that visits the edit page and a subject (from a let)
shared_examples "metadata file in edit page" do
  scenario "can delete existing metadata files" do
    within '#metadata_file_fields_table' do
      page.first('.delete_check_box').set(true)
    end
    expect { page.first(:xpath, "//input[@value='Submit']").click }.to change{subject.metadata_files.count}.by(-1)
  end
  scenario "can delete and add metadata files at the same time" do
    attach_file "Metadata files", [Rails.root.join("spec","photos", "scanned_field_note.jpg").to_s ,Rails.root.join("spec","photos", "transcribed_field_note.pdf").to_s]
    within '#metadata_file_fields_table' do
      page.first('.delete_check_box').set(true)
    end
    expect { page.first(:xpath, "//input[@value='Submit']").click }.to change{subject.metadata_files.count}.by(1)
  end
end
#requires a before block that visits create page
shared_examples "metadata file in create page" do
  scenario "has a field in form" do
    expect(page).to have_field("Metadata files")
  end
  scenario "metadata is able to accept multiple files" do
    attach_file "Metadata files", [Rails.root.join("spec","photos","metadata.xls"),Rails.root.join("spec","photos","dummy_file.jpeg")]
    page.first(:xpath, "//input[@value='Submit']").click
    expect(page).to have_selector("a", text: "metadata.xls")
    expect(page).to have_selector("a", text: "dummy_file.jpeg")
  end
end

#show
shared_examples "metadata file in show page" do |metadata_owner|
  scenario "___ has no metadata file" do
    owner = FactoryGirl.create(metadata_owner)
    visit "/#{metadata_owner.to_s.pluralize}/#{owner.id}"
    expect(page).to have_no_text("Metadata Files")
  end
  scenario "___ has one metadata file" do
    owner = FactoryGirl.create("#{metadata_owner.to_s}_with_metadata", metadata_file_count: 1)
    visit "/#{metadata_owner.to_s.pluralize}/#{owner.id}"
    expect(page).to have_selector("table#metadata_files_table")
    expect(page).to have_selector("a", text: "Metadata Files")
    expect(page).to have_selector("#metadata_file_row_#{owner.metadata_files.first.id}")
    expect(page).to have_selector("a", text: "#{owner.metadata_files.first.display_name}")
  end
end

shared_examples "unmanaged metadata folder" do |umetadata_owner|
  scenario "'Browse Metadata Folder' link exists" do
    owner = FactoryGirl.create("#{umetadata_owner.to_s}")
    visit "/#{umetadata_owner.to_s.pluralize}/#{owner.id}"
    expect(page).to have_selector("a", text: "Browse Metadata Folder")
  end
  scenario "clicking 'Browse Metadata Folder' when metadata folder does not exist" do
    owner = FactoryGirl.create("#{umetadata_owner.to_s}")
    visit "/#{umetadata_owner.to_s.pluralize}/#{owner.id}"
    click_link "Browse Metadata Folder"
    #Metadata folder should be created
    expect(Dir.exists?(owner.umetadata_path)).to be_truthy
  end
    #scenario "'Browse Metadata Folder' link has simple file number stats, when metadata folder does not exist"
    #scenario "'Browse Metadata Folder' link has simple file number stats, when metadata folder has file content"
    #scenario "'Browse Metadata Folder' link has simple file number stats, when metadata folder has subfolders"
end

