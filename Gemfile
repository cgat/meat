source 'https://rubygems.org'

gem 'dotenv-rails', "~>2.5.0" #list this early so that any gems that indirectly use the env variables will get the right ones

if ENV['APP_GEMS_DIR'] # for local gem development, use local gem directory so gem changes don't need to be pushed to test
  gem 'meat_base', :path => "#{ENV['APP_GEMS_DIR']}/meat-system/meat_base"
  gem 'pointsable', path: "#{ENV['APP_GEMS_DIR']}/pointsable"
  gem 'image_reggy', :path => "#{ENV['APP_GEMS_DIR']}/image_reggy"
else
  gem 'meat_base', git: 'https://cgat@bitbucket.org/cgat/meat_base.git', ref: 'ed04a512393892417c1758c01a579a72710c5aca'
  gem 'pointsable', git: 'https://github.com/cgat/pointsable.git', ref: '805997b'
  gem 'image_reggy', git: 'https://bitbucket.org/cgat/image_reggy', ref: '4e9dbb261b9cafc45cebb5dde68efb69c90f5917'
end

gem 'puma'
gem 'rails', '~>4.2.0'
gem 'bootstrap-sass', '2.3.0'
gem 'bcrypt-ruby', '3.0.1'
gem 'faker', '1.0.1'
gem 'will_paginate', '3.0.3'
gem 'bootstrap-will_paginate', '0.0.6'
gem 'simple_form'#, '2.0.4'
#gem 'jstree-rails-4'
#gem 'jstree-rails', :git => 'git://github.com/tristanm/jstree-rails.git', ref: 'e2ad211ea'
gem 'multi-select-rails' #, '0.9.5'
gem 'devise' #,'2.2.3'
gem 'cancan', '1.6.9'
gem 'chart-js-rails', '0.0.6'
gem 'naturally', '1.1.0'
gem 'exception_notification', '4.0.1'

gem 'ruby-vips', '0.3.14'
gem 'carrierwave', '~> 0.11.2'
gem "carrierwave-vips", '1.0.5' #:git => "https://github.com/eltiare/carrierwave-vips.git" #required until gem is updated to match master (>1.0.4)
gem 'carrierwave_backgrounder', "~> 0.3.0"
gem 'sidekiq', ' ~> 2.16'
gem 'sinatra', '1.3.5', :require => false
gem 'slim', '1.3.6'

gem 'net-ssh', '2.8.0'

gem 'sprockets-rails', :require => 'sprockets/railtie'
gem 'sass-rails',   '5.0.6'
gem 'coffee-rails'#, '3.2.2'
gem 'uglifier'#, '1.3.0'

gem 'protected_attributes'

gem 'rails_12factor', group: :production

group :development, :test do
  gem 'rspec-rails', '2.99.0'
  gem 'test-unit', '3.0' #needed to do this to get console to run, but don't use this anywhere?
  gem 'database_cleaner', '~> 1.5.3'
  gem "awesome_print", '1.1.0'
  gem 'hirb', '0.7.1'
  gem 'byebug', '~> 9.0'
  gem 'pry-byebug', '~> 3.4.0'
  # gem 'pry-doc'
  gem 'pry-rails'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'ruby-prof'
end

group :development do
  gem "bullet", '4.6.0'
  gem 'annotate', '2.5.0'
  gem 'web-console', '~> 2.0'
  gem 'capistrano-rails',   '~> 1.1', require: false
  gem 'capistrano-bundler', '~> 1.1', require: false
  gem 'capistrano', '~> 3.1', require: false
  gem 'capistrano-rvm',   '~> 0.1', require: false
end

gem 'jquery-rails', '2.1.4'

group :test do
  gem 'capybara', '~> 2.7.1'
  gem 'capybara-webkit', '~> 1.11.1'
  gem 'selenium-webdriver', '~> 3.0.0.beta2.1'
  gem 'factory_girl_rails' #, '4.2.0'
  gem 'launchy', '2.1.1'
  #gem 'test_after_commit'
end


#gem 'rack-mini-profiler' #needs to be last (or at least after the db gems)
