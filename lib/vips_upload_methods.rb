require 'open3'

module VipsUploadMethods

  def convert_to_8bit
    manipulate! do |image|
      #vips specific
      image = (image >> 8).clip2fmt :uchar if image.sizeof_element>1
      image
    end
  end

  # Only use if using carrierwave-vips
  def manipulate!
    cache_stored_file! unless cached?
    @_vimage ||= if is_3fr?(file)
      # Vips cannot process 3FR files natively, so it needs to pass it off to imagemagick
      # This can be done by reading the file into vips with magick instead of new.
      # This relies on the content type set in the overrode set_content_type method
      VIPS::Image.magick(current_path)
    elsif is_raw?(current_path)
      # We use `dcraw` to extract thumbnails from raw images to create the
      # preview images.
      stdout, stderr, status = Open3.capture3("dcraw", "-e", current_path)
      ext = File.extname(current_path)
      extracted_thumbnail_path = current_path.gsub(ext, ".thumb.jpg")
      `mv #{extracted_thumbnail_path} #{current_path}`
      VIPS::Image.jpeg(current_path, :sequential => true)
    elsif file.content_type=="image/jpeg"
      # This is required because carrierwave does not change extension type for versions at the
      # caching stage. This means that the cache file "medium_HB1_A000123_1231231.3FR" may actually be a jpeg.
      # This situation screws around with VIPS. To get around it, we set the content type when we cache the time
      # and ensure that it is read into VIPS using the jpeg reader.
      VIPS::Image.jpeg(current_path, :sequential => true)
    end
  ensure
    super
  end

  def is_raw?(current_path)
    stdout, stderr, status = Open3.capture3("dcraw", "-i", current_path)
    status.success?
  end

  def is_3fr?(file)
    return file.content_type=="image/x-hasselblad-3fr"
  end

  #Overrides defaults set_content_type in order
  #so that content_type can be set manually.
  #Seems to be necessary when using vips
  #and file conversion, since the mime-types
  #gem gets the content_type from the file
  #extension, which doesn't change until
  #the file is processed. Also useful to
  #set the content type of abnormal files.
  #3FR's mime-types were not getting set properly
  #this sets the content type explicitly.
  def set_content_type_with_custom(override=false,*args)
    if !args.empty?
      file.content_type=args.first
    end
  end
end
