#! /bin/bash

# backup-postgresql.sh
# by Craig Sanders
# this script is public domain.  feel free to use or modify as you like.

DUMPALL="/usr/local/bin/pg_dumpall"
PGDUMP="/usr/local/bin/pg_dump"
PSQL="/usr/local/bin/psql"

# directory to save backups in, must be rwx by postgres user
BASE_DIR="/Volumes/mlp/db_backups/postgres"
YMD=$(date "+%Y-%m-%d")
DIR="$BASE_DIR/$YMD"
mkdir -p $DIR
cd $DIR

# get list of databases in system , exclude the tempate dbs
DBS=$($PSQL -l -t | egrep -v 'template[01]' | awk '{print $1}' | egrep -v '\|')

# first dump entire postgres database, including pg_shadow etc.
$DUMPALL | gzip -9 > "$DIR/db.out.gz"

# next dump globals (roles and tablespaces) only
$DUMPALL -g | gzip -9 > "$DIR/globals.gz"

# now loop through each individual database and backup the schema and data separately
for database in $DBS; do
    if [ -n "$DBS" ] ; then
        SCHEMA=$DIR/$database.schema.gz
        DATA=$DIR/$database.data.gz

        # export data from postgres databases to plain text
        $PGDUMP -C -s $database | gzip -9 > $SCHEMA

        # dump data
        $PGDUMP -a $database | gzip -9 > $DATA
    fi
done

# delete backup files older than 90 days
OLD=$(find $BASE_DIR -type d -mtime +90)
if [ -n "$OLD" ] ; then
        echo deleting old backup files: $OLD
        echo $OLD | xargs rm -rfv
fi
