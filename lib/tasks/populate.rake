#!/bin/env ruby
# encoding: utf-8
require 'csv'
load(Rails.root.join('lib','tasks','task_helper_methods.rb'))
namespace :populate do
  #on my own machine: bundle exec rake populate:set_root_dir["/MLPLibraryNew"]
  task :set_root_dir, [:library_root] => :environment do |t,args|
    unless pref = Preference.first
      pref = Preference.create(mlp_library_local_root: args[:library_root])
    else
      pref.update_attributes(mlp_library_local_root: args[:library_root])
    end
  end



  #TODOs
  #-handle repeats and originals outside of stations
  #-handle metadata folders - images
  #-handle non-station leftover files -> move into metadata?
  #-handle scenics_images folder
  #on my own machine: bundle exec rake populate:import_data["/Volumes/mlp/MLP Library_working","TranscribedFieldNotes","'Detriot Photographic Co'"]
  #to use the grep like you would at the command line "/B\ South/". This won't work "B South", but this would '"B South"'
  task :import_data, [:root_to_old_library, :transcribedFN_folder,:grep_filter] => :environment do |t,args|
    debugger
    #add some logic to input an inventory file instead of just the root, which will be scanned for files
    if args[:grep_filter].present?
      inventory = Inventory.new(root_to_old_library: args[:root_to_old_library], grep_filter: args[:grep_filter])
    else
      inventory = Inventory.new(root_to_old_library: args[:root_to_old_library])
    end
    files = inventory.files
    #clear out files that already been imported and update the inventory accordingly
    files.reject! do |f|
      if ci = CaptureImage.find_by_legacy_path(f)
        inventory.update(f, import_status: "Y", obj_id: ci.id, obj_type: ci.class.name)
        true
      elsif fn = FieldNote.find_by_legacy_path(f)
        inventory.update(f, import_status: "Y", obj_id: fn.id, obj_type: fn.class.name)
        true
      elsif mf = MetadataFile.find_by_legacy_path(f)
        inventory.update(f, import_status: "Y", obj_id: mf.id, obj_type: mf.class.name)
        true
      elsif li = LocationImage.find_by_legacy_path(f)
        inventory.update(f, import_status: "Y", obj_id: li.id, obj_type: li.class.name)
        true
      elsif si = ScenicImage.find_by_legacy_path(f)
        inventory.update(f, import_status: "Y", obj_id: si.id, obj_type: si.class.name)
        true
      elsif f=~/blended/
        inventory.update(f, import_status: "N", msg: "blended images will not be imported. These may be deleted after import is complete")
        true
      elsif f=~/\.xmp$/
        inventory.update(f, import_status: "N", msg: "xmp files not supported in application")
        true
      elsif f=~/Image Variants.*pdf$/
        inventory.update(f, import_status: "N", msg: "these should have been converted to png. Png imported instead")
        true
      else
        false
      end
    end
    files_in_stations,files_non_stations = files.partition{|f| f=~/\/Stations\// }
    #this is a hash, with keys being the station paths, and the values being an array of files in that station
    files_by_station = files_in_stations.group_by{|f| f[/.*\/Stations\/[^\/]*/]}

    files_by_station.each do |station_path, station_files|
      begin
        puts "Starting #{station_path}..."

        station = create_station_from_path(station_path)
        location_photos, station_files = station_files.partition{|f|f=~/\/Location Photo/}
        field_notes, station_files = station_files.partition{|f|f =~ /\/Field Notes\//}
        non_images, station_files = station_files.partition{|f|f !~ /(jpeg|jpg|tiff|tif|fff|3FR|png|psd)$/i}
        mlpimages = station_files.map{|f|MLPImage.new(f)}
        repeats, mlpimages = mlpimages.partition{|m|m.repeat?}
        originals, mlpimages = mlpimages.partition{|m|m.original?}

        repeats_grouped = MLPImage.group(repeats)
        originals_grouped = MLPImage.group(originals)

        #at this point we need a historic visit and a visit to house the captures
        #1st, try to do this with transcribed field notes
        transcribed_fns = field_notes.grep(/TranscribedFN/)
        transcribed_fns.each do |fn|
          begin
            #this will create a visit and historic visit in the datbase with info from transcribed field notes
            #if the transcribed fn's aren't formatted properly, it will fail with exception and we'll carry on.
            visit, historic_visit = process_transcribed_fn(station,fn, args[:transcribedFN_folder])
          rescue StandardError => e
            inventory.update(fn,import_status: "N",msg: "MANUAL IMPORT MAY BE NEEDED! #{e.message} \n #{e.backtrace}")
            next
          else
            inventory.update(fn,import_status: "Y",obj_id: visit.id,obj_type: "Visit")
          end
          visit.locations.each do |location|
            lphoto_start_num = location.legacy_photos_start || location.legacy_photos_end
            lphoto_end_num = location.legacy_photos_end || lphoto_start_num
            this_loc_photos, location_photos = location_photos.partition{|lp| lphoto_start_num<=lp.sub(/.(jpg|3FR|tif|tiff|jpeg)/i, "")[/\d*$/].try(:to_i) && lp.sub(/.(jpg|3FR|tif|tiff|jpeg)/i, "")[/\d*$/].try(:to_i)<=lphoto_end_num} if lphoto_end_num
            this_loc_photos.each do |lp|
              lpi = location.location_photos.create(image: FilelessIO.new(lp, dummy: false), legacy_path: lp)
              inventory.update(lp,obj_id: lpi.id, obj_type: "LocationImage",import_status: "Y" )
            end if this_loc_photos.present?
          end #end of for each location of visit
          station.captures.each do |c|
            #the point of this each loop is to match captures from the field notes to
            #to capture image groups. If the capture does not have a fn_photo_reference
            #(a previous import created a capture without fn reference, then it was rerun)
            #then we ignore it.
            next if c.fn_photo_reference.blank?
            #try to match the mlpimages that have been grouped with a capture
            #we try to match based on the end of the field note photo reference if it is 3 characters or more, otherwise, try to match on what we have
            fn_ref_matcher = c.fn_photo_reference.length<=3 ? c.fn_photo_reference : c.fn_photo_reference.slice(-3..-1)
            matching_group = repeats_grouped.select{|a|a.map(&:filename).grep(/#{fn_ref_matcher}$/).present?}
            if matching_group.count>1
              raise StandardError, "The capture field note photo reference #{c.fn_photo_reference} matched more than one of the repeat groups"
            end
            if matching_group.count==1
              matching_group = matching_group.first
              matching_group.each do |m|
                if m.alternate?
                  c.update_attributes(alternate: m.alternate?)
                end
                if c.capture_images.where(legacy_path: m.path.to_s).empty?
                  #clean up any instance of this image that was sorted somewhere else
                  CaptureImage.find_by_legacy_path(m.path.to_s).try(:first).try(:destroy)
                  ci = c.capture_images.create(image: m.to_stringio, image_state: m.image_state, comments: m.comments, legacy_path: m.path.to_s )
                  inventory.update(m.path.to_s, import_status: "Y", obj_id: ci.id, obj_type: "CaptureImage")
                end
              end
              repeats_grouped.delete(matching_group)
              #if the capture has historic capture comparisons from the field notes, try to match those
              #comparisons with a group or originals.
              if !c.historic_captures.empty?
                #stratgey one: find capture images with similar names as the field note photo reference
                c.historic_captures.each do |hcap|
                  originals_grouped.each do |ogroup|
                    if ogroup.any?{ |hm|  historic_similarity_check(hcap.fn_photo_reference,hm.filename) }
                      imported = []
                      ogroup.each do |m|
                        hci = hcap.capture_images.create(image: m.to_stringio, image_state: m.image_state, comments: m.comments, legacy_path: m.path.to_s)
                        if hci.valid?
                          inventory.update(m.path.to_s, import_status: "Y", obj_id: hci.id, obj_type: "CaptureImage")
                          imported << m
                        else
                          inventory.update(m.path.to_s, import_status: "N", msg: "Failed to create the capture image. Error: #{hci.errors.full_messages}")
                        end
                      end
                      ogroup.delete_if{|m|imported.include?(m)}
                    end
                  end
                end
                #clean up empty orginal groups
                originals_grouped.delete_if{|og| og.empty? }
                #strategy two: find a group of original capture images with a similar plateid as the capture images of the current capture (only works if there is one historic capture referenced)
                comparable_group = MLPImage.comparable_groups(matching_group, originals_grouped)
                if comparable_group.present?
                  h = c.historic_captures.first
                  comparable_group.each do |m|
                    if c.historic_captures.count==1
                      if h.capture_images.where(legacy_path: m.path.to_s).empty?
                        hci = h.capture_images.create(image: m.to_stringio, image_state: m.image_state, comments: m.comments, legacy_path: m.path.to_s)
                        inventory.update(m.path.to_s, import_status: "Y", obj_id: hci.id, obj_type: "CaptureImage")
                      end
                    else
                      inventory.update(m.path.to_s, import_status: "N", msg: "The capture with id #{c.id} has more than one Historic Capture related to it. Thus, it is ambiguous which HistoricCapture should get the images.")
                    end
                  end
                  originals_grouped.delete(comparable_group) if c.historic_captures.count==1
                end #end of compareable_group.present?
              end #end of historic_captures empyt?
            end #matching group==1
          end #end of for each capture of station
        end #end of each transcribed fn
        #now deal with images/files that have not be imported in the above process (via relations to transcribed fns)
        #repeats first
        repeats_grouped.each do |rg|
          dates = rg.map{|m|m.capturetime.try(:to_date)}.compact.uniq
          if dates.count>1
            rg.each {|m| inventory.update(m.path.to_s,import_status: "N", msg: "This image was grouped as a repeat with images that have a different capture date. ")}
            next
          elsif dates.empty?
            rg.each {|m| inventory.update(m.path.to_s,import_status: "N", msg: "Cannot create visit for image due to lack of creation date and field notes")}
            next
          else
            date = dates.first
            visit = station.visits.find_by_date(date) || station.visits.create(date: date)
            capture = visit.unsorted_captures.build()
          end
          rg.each{|m| capture.capture_images.build(image: m.to_stringio, image_state: m.image_state, comments: m.comments, legacy_path: m.path.to_s)}
          if capture.save
            rg.each {|m| inventory.update(m.path.to_s, import_status: "Y", obj_id: capture.capture_images.find_by_legacy_path(m.path.to_s).id, obj_type: "CaptureImage")}
          else
            rg.each {|m| inventory.update(m.path.to_s, import_status: "N", msg: "Could not create capture for this image. Error: #{capture.errors.full_messages.to_s}")}
          end
        end

        #now originals
        originals_grouped.each do |og|
          historic_visit = station.historic_visit || station.create_historic_visit
          hcapture = historic_visit.historic_captures.build
          og.each{|m| hcapture.capture_images.build(image: m.to_stringio, image_state: m.image_state, comments: m.comments, legacy_path: m.path.to_s)}
          if hcapture.save
            og.each {|m| inventory.update(m.path.to_s, import_status: "Y", obj_id: hcapture.capture_images.find_by_legacy_path(m.path.to_s).id, obj_type: "CaptureImage")}
          else
            og.each {|m| inventory.update(m.path.to_s, import_status: "N", msg: "Could not create capture for this image. Error: #{capture.errors.full_messages.to_s}")}
          end
          comparable_group = MLPImage.comparable_groups(og, repeats_grouped)
          unless comparable_group.blank?
            comparable_group_ci = comparable_group.map{|m| CaptureImage.find_by_legacy_path(m.path.to_s)}
            if capture = comparable_group_ci.find{|ci|ci.captureable if ci.present? }.try(:captureable)
              hcapture.add_comparison(capture)
            end
          end
        end

        #now deal with field notes. If there is only one visit, then all field note files are stored under that visit, otherwise
        #it is ambiguous where to put the field note files. In this case, report to inventory with error message (may have to do this manually)
        visits = station.visits
        if field_notes.present? && visits.count==1
          visit = visits.first
          field_notes.each do |fn|
            fn_new = visit.field_notes.create(field_note_file: FilelessIO.new(fn, dummy: false), legacy_path: fn)
            inventory.update(fn, import_status: "Y", obj_id: fn_new.id, obj_type: fn_new.class.name)
          end
        elsif field_notes.present? && visits.count>1
          field_notes.each do |fn|
            inventory.update(fn, import_status: "N", msg: "More than one visit exists under this station, therefore it is ambiguous which visit to sort this field note file.")
          end
        else
          field_notes.each do |fn|
            inventory.update(fn, import_status: "N", msg: "No visits exist in this station, therefore no place to put field note file.")
          end
        end

        #now deal with location photos that we were not able to put into a location
        visits = station.visits
        location_photos_grouped = location_photos.group_by{|lp| MiniExiftool.new(lp, :convert_encoding => true).try(:datetimeoriginal).try(:to_date) }
        visits.each do |visit|
          location_photos_grouped.each do |lp_date, lphotos|
            if visits.count==1 && visit.locations.count==1
              #if there is one visit and one location, then we know the location these images should be
              lphotos.each do |lp|
                lpi = visit.locations.first.location_photos.create(image: FilelessIO.new(lp), legacy_path: lp)
                inventory.update(lp,obj_id: lpi.id, obj_type: "LocationImage",import_status: "Y" )
              end
            elsif visits.count==1
              #if there is only one visit associated with the station, assume that the location photos are from that visit
              lphotos.each do |lp|
                lpi = visit.unsorted_location_photos.create(image: FilelessIO.new(lp), legacy_path: lp)
                inventory.update(lp,obj_id: lpi.id, obj_type: "LocationImage",import_status: "Y" )
              end
            elsif visit.date==lp_date
              #if there is more than one visit, then we try to use the dates to figure out where to put them
              lphotos.each do |lp|
                lpi = visit.unsorted_location_photos.create(image: FilelessIO.new(lp), legacy_path: lp)
                inventory.update(lp,obj_id: lpi.id, obj_type: "LocationImage",import_status: "Y" )
              end
            else
              #sort them under the station, as it is difficult to determine in what visit they belong
              lphotos.each do |lp|
                lpi = station.unsorted_location_photos.create(image: FilelessIO.new(lp), legacy_path: lp)
                inventory.update(lp,obj_id: lpi.id, obj_type: "LocationImage",import_status: "Y" )
              end
            end
            location_photos_grouped.delete(lp_date)
          end
        end

        #now handle any left over files... either metadata type files (notes.txt) or unplaced images.
        visits = station.visits
        if non_images.present? && visits.count==1
          visit = visits.first
          non_images.each do |ni|
            #will not track these files in the database
            if ni=~/.*\/([^\/].*\.mlp)$/ || ni=~/.*\/([^\/].*\.log)$/
              FileUtils.cp(ni,File.join(visit.filesystem_path,$1)) if !File.exists?(File.join(visit.filesystem_path,$1))
              next
            end
            v_metadata = visit.metadata_files.create(metadata_file: FilelessIO.new(ni, dummy: false), legacy_path: ni)
            if v_metadata.nil?
              inventory.update(ni, import_status: "N", msg: "Failed to create metadata file: #{ni}. Errors: #{v_metadata.errors.full_messages.to_s}")
            else
              inventory.update(ni, import_status: "Y", obj_id: v_metadata.id, obj_type: v_metadata.class.name)
            end
          end
        end

      rescue => e
        inventory.update(station_path, msg: "Error while processing files in station: #{e.message} and backtrace #{e.backtrace}")
      end
    end #end of station

    #Metadata files from metadata folder
    # files_in_metadata,files_non_stations = files_non_stations.partition{|f| f=~/\/Metadata\// }
    # files_by_metadata = files_in_metadata.group_by{|f| f[/.*\/Metadata\//]}
    # files_by_metadata.each do |metadata_folder, metadata_files|
    #   begin
    #     parent_folder = metadata_folder.chomp("/Metadata/")
    #     if project_path?(parent_folder)
    #       metadata_owner=find_project(parent_folder)
    #     elsif survey_season_path?(parent_folder)
    #       metadata_owner=create_surveyseason_from_path(parent_folder)
    #     elsif survey_path?(parent_folder)
    #       metadata_owner=Survey.find_by_legacy_path(parent_folder)
    #     end
    #     if metadata_owner.nil?
    #       inventory.update(metadata_folder, import_status: "N", msg: "Failed to create season for metadata files. Make sure #{parent_folder} is properly formatted")
    #       next
    #     end
    #     metadata_files.each do |f|
    #       if f=~/\.(jpg|jpeg|tif|tiff|png|3FR|fff|gif|psd)$/i
    #         inventory.update(f, import_status: "N", msg: "Metadata files that are images may be metadata (like maps) or unsorted images. Import manually.")
    #         next
    #       end
    #       mf_new = metadata_owner.metadata_files.create(metadata_file: FilelessIO.new(f, dummy: false), legacy_path: f)
    #       if mf_new.nil? || !mf_new.valid?
    #         inventory.update(f, import_status: "N", msg: "Failed to create metadata file: #{f}. Errors: #{mf_new.errors.full_messages.to_s}")
    #       else
    #         inventory.update(f, import_status: "Y", obj_id: mf_new.id, obj_type: mf_new.class.name)
    #       end
    #     end
    #   rescue => e
    #     inventory.update(metadata_folder, msg: "Error while processing files in metadata folder: #{e.message} and backtrace #{e.backtrace}")
    #   end
    # end

    # #Digital Object Tracker files, located under the survey season folder in the old structure
    # dot_files, file_non_stations = files_non_stations.partition{|f| f=~/Digital Object Tracker/}

    # dot_files.each do |f|
    #   parent_folder = f.sub(/\/Digital Object Tracker.*$/,"")
    #   if project_path?(parent_folder)
    #     dot_owner=find_project(parent_folder)
    #   elsif survey_season_path?(parent_folder)
    #     dot_owner=create_surveyseason_from_path(parent_folder)
    #   elsif survey_path?(parent_folder)
    #     dot_owner=Survey.find_by_legacy_path(parent_folder)
    #   end
    #   if dot_owner.nil?
    #     inventory.update(f, import_status: "N", msg: "The survey season for the digital object tracker, #{f}, could not be created or found.")
    #     next
    #   end
    #   dot_new = dot_owner.metadata_files.create(metadata_file: FilelessIO.new(f, dummy: false), legacy_path: f)
    #   if dot_new.nil? || !dot_new.valid?
    #     inventory.update(f, import_status: "N", msg: "Failed to create the metadata file: #{f}. Errors: #{dot_new.errors.full_messages.to_s}")
    #   else
    #     inventory.update(f, import_status: "Y", obj_id: dot_new.id, obj_type: dot_new.class.name)
    #   end
    # end

    #Scenics from Scenic folders located under survey season folder in old structure
    # files_in_scenics, file_non_stations = files_non_stations.partition{|f| f=~/\/Scenics\// }
    # files_by_scenics = files_in_scenics.group_by{|f| f[/.*\/Scenics\//]}
    # files_by_scenics.each do |scenics_folder, scenics_files|
    #   begin
    #     parent_folder = scenics_folder.chomp("/Scenics/")
    #     if project_path?(parent_folder)
    #       scenics_owner=find_project(parent_folder)
    #     elsif survey_season_path?(parent_folder)
    #       scenics_owner=create_surveyseason_from_path(parent_folder)
    #     elsif survey_path?(parent_folder)
    #       scenics_owner=Survey.find_by_legacy_path(parent_folder)
    #     end
    #     if scenics_owner.nil?
    #       inventory.update(scenics_folder, import_status: "N", msg: "Failed to create season for scenic images. Make sure #{parent_folder} is properly formatted")
    #       next
    #     end
    #     scenics_files.each do |f|
    #       sc_new = scenics_owner.scenics.create(image: FilelessIO.new(f, dummy: false), legacy_path: f)
    #       if sc_new.nil? || !sc_new.valid?
    #         inventory.update(f, import_status: "N", msg: "Failed to create scenic file: #{f}. Errors: #{sc_new.errors.full_messages.to_s}")
    #       else
    #         inventory.update(f, import_status: "Y", obj_id: sc_new.id, obj_type: sc_new.class.name)
    #       end
    #     end
    #   rescue => e
    #     inventory.update(scenics_folder, msg: "Error while processing files in scenics folder: #{e.message} and backtrace #{e.backtrace}")
    #   end
    # end

    inventory.save_to_csv
  end


  def process_transcribed_fn(station,fn, fn_folder)

    fn_filename = fn.chomp(".pdf").split("/").last
    dir_list = Dir.glob(File.join(fn_folder,"**","*#{fn_filename}.txt"))
    if dir_list.empty?
      raise StandardError, "The transcribed fn, #{fn}, did not have a formatted txt version, #{fn_filename}.txt, in folder #{fn_folder}"
    end

    fn_csv_path = dir_list.first
    csv = CSV.read(fn_csv_path, col_sep: "\t", encoding: "MacRoman")
    if csv.count>2
      raise StandardError, "The transcribed fn, #{fn}, was not converted properly (have more than two rows)"
    end
    headers = csv[0]
    fn_array = csv[1]

    #check to see if a visit of this date has already been created
    visit_date = fn_array[headers.index_match(/Repeat Date/)].to_date rescue nil
    if visit_date.nil?
      raise StandardError, "Was not able to format transcribed field notes, #{fn}, because date was not correclty formatted"
    end
    #we don't reprocess a visit if it exists and is from transcribed field notes
    if visit_date.present? && station.historic_visit.present?
      current_visit = station.visits.find_by_date(visit_date)
      if  current_visit.present? && current_visit.from_transcribed_field_notes
        return current_visit, station.historic_visit
      end
    end

    #station lat/long processed first
    lat_string = fn_array[headers.index_match(/Lat/)]
    lat_d,lat_m,lat_s,lat_dir=parse_coordinates(lat_string)
    if lat_dir.blank?
      lat_dir=1 #default to North, which is 1
    elsif lat_dir=="N"
      lat_dir=1
    elsif lat_dir=="S"
      lat_dir=2
    end
    long_string = fn_array[headers.index_match(/Long/)]
    long_d,long_m,long_s,long_dir=parse_coordinates(long_string)
    if long_dir.blank?
      long_dir=2 #default to West, which is 2
    elsif long_dir=="E"
      long_dir=1
    elsif long_dir=="W"
      long_dir=2
    end

    station.update_attributes( lat_d: lat_d, lat_m: lat_m, lat_s: lat_s, lat_dir: lat_dir, long_d: long_d, long_m: long_m, long_s: long_s, long_dir: long_dir) if station.lat.nil? && station.long.nil?
    if fn_array[headers.index_match(/Station Narrative/)]=~/(UTM|Waypoint|GPS)/i
      zone,easting,northing,elevation=parse_UTM_coordinates(fn_array[headers.index_match(/Station Narrative/)])
      station.update_attributes(zone: zone, easting: easting, northing: northing, elevation: elevation) if zone.present? && easting.present? && northing.present?
    end
    visit_hash = { date: visit_date, start_time: fn_array[headers.index_match(/Start Time/)], finish_time: fn_array[headers.index_match(/Finish Time/)], pilot: fn_array[headers.index_match(/Pilot/)], rw_call_sign: fn_array[headers.index_match(/R.W. Call/)], visit_narrative: fn_array[headers.index_match(/Station Narrative/)], illustration: fn_array[headers.index_match(/Illustration/i)], weather_narrative: fn_array[headers.index_match(/Weather Narrative/)], weather_temp: fn_array[headers.index_match(/Temperature/)].try(:chomp,"."), weather_ws: fn_array[headers.index_match(/Average Wind Speed/)].try(:chomp,"."), weather_gs: fn_array[headers.index_match(/Max. Gust/)].try(:chomp,"."), weather_pressure: fn_array[headers.index_match(/Pressure/)].try(:chomp,"."), weather_rh: fn_array[headers.index_match(/Relative Hum/)].try(:chomp,"."), weather_wb: fn_array[headers.index_match(/Wet Bulb/)].try(:chomp,".") }
    historic_visit_hash = { date: nil, comments: nil }

    locations_attributes = {}
    #historic_captures_attributes = {}
    #comparisons = []

    #Locations
    index_of_location_columns = headers.index_grep(/Location (.* image)/)
    values_at_location_columns = index_of_location_columns.map{|i| fn_array[i] }
    location_identifiers = values_at_location_columns.uniq.reject(&:blank?)

    location_sites = headers.group_by {|h| h[/\(.+ site\)/] }; location_sites.delete(nil)
    #the next two lines fix a bug in the older field note pdf's, where second is spect seccond and fifth is spelt fith
    location_sites["(fifth site)"] << location_sites["(fith site)"].first and location_sites.delete("(fith site)") if location_sites.has_key?("(fith site)")
    location_sites["(second site)"] << location_sites["(seccond site)"].first and location_sites.delete("(seccond site)") if location_sites.has_key?("(seccond site)")
    #location sites will be an array of arrays. Each subarray contains: [location_site_id, location_narrative, location_photo_ending_number, location_photo_starting_number]
    location_sites = location_sites.values.map{|a|a.map{|y|fn_array[headers.index(y)]}}.reject{|b|b.all?(&:nil?)}
    location_sites.each_with_index do |site_array,i|
      location_hash = {location_identity: site_array[0]}
      if location_hash[:location_identity].blank? && location_sites.count==1
        location_hash[:location_identity] = "1"
      elsif location_hash[:location_identity].blank?
        raise StandardError, "The location idenity field (site) cannot be blank. Fix and run again, site_array: #{site_array.to_s}"
      end
      location_hash[:location_narrative]=site_array[1]
      location_hash[:zone],location_hash[:easting],location_hash[:northing],location_hash[:elevation]=parse_UTM_coordinates(location_hash[:location_narrative]) if location_hash[:location_narrative]=~/(UTM|Waypoint|GPS)/i
      location_hash[:legacy_photos_start] = site_array[3]
      location_hash[:legacy_photos_end] = site_array[2]
      locations_attributes[i.to_s] = location_hash
    end

    #images is an array of arrays. Each sub array corresponds to one repeat capture. The subarray contains
    #the following elements in the order described [<azimuth>,<location identity>,<original field note reference>, <repeat field note reference>]
    images = headers.group_by {|h| h[/\(\d{1,2}[a-z]{2} image\)/] }; images.delete(nil)
    images = images.values.map{|a|a.map{|y|fn_array[headers.index(y)]}}.reject{|b| b[3].blank?} #map(&:compact).reject(&:blank?)

    visit_hash[:locations_attributes] = locations_attributes
    #historic_visit_hash[:historic_captures_attributes] = historic_captures_attributes


    if station.historic_visit.present?
      historic_visit=station.historic_visit
    else
      historic_visit = station.build_historic_visit(historic_visit_hash)
      unless historic_visit.save
        raise StandardError, "Was not able to create the historic visit upon trying to save. Errors: #{historic_visit.errors.full_messages.to_s}"
      end
    end
    #first see if the visit already exists, if it does, just return with these values. (assume that the fn's have already been imported)
    unless visit=Visit.where(station_id: station.id, date: visit_hash[:date].to_date).first
      visit = station.visits.build(visit_hash)
      unless visit.save
        raise StandardError, "Was not able to create the visit upon trying to save. Errors: #{visit.errors.full_messages.to_s}"
      end
    end

    #there might be originals/repeats where the person filling in the form didn't give them a location.
    #in this this case, we should add them as unsorted captures and normal historic captures or create
    #locations based on
    images.each do |img_array|
      location_id = img_array[1]
      cap_reference, comment = parse_photo_reference(img_array[3])
      alternate = false
      if comment=~/(Alt|alternate)/i
        alternate = true
        comment = ""
      end
      hcap_reference = img_array[2]
      azimuth = img_array[0].present? ? img_array[0].delete("°¼º").to_i : nil
      #if the image has a location identifier
      if location_id.present?
        capture_owner = visit.locations.where(location_identity: location_id).first_or_create()
      else
        capture_owner = visit
      end

      cap = Capture.create(fn_photo_reference: cap_reference, azimuth: azimuth, comments: comment, alternate: alternate, capture_owner_id: capture_owner.id, capture_owner_type: capture_owner.class.name)

      if hcap_reference.present?
        hcap = historic_visit.historic_captures.where(fn_photo_reference: hcap_reference).first_or_create()
        cap.historic_captures << hcap if !cap.historic_captures.include? hcap
      end
    end

    #keywords create
    keywords_array = parse_keywords(fn_array[headers.index_match(/Station Keywords/)])
    if keywords_array.present?
      keywords = keywords_array.map do |k|
        Keyword.where(keyword: k).first_or_create()
      end
      keywords.each{ |k| visit.keywords << k if !visit.keywords.include?(k) }
    end

    #participants create (author, photographer, hiking party)
    hiking_party_array = parse_participants(fn_array[headers.index_match(/Hiking Party/)])
    if hiking_party_array.present?
      hiking_party = hiking_party_array.map do |p|
        Participant.where(given_names: p[:given_names], last_name: p[:last_name]).first_or_create()
      end
      hiking_party.each{ |p| visit.hiking_party_participants << p if !visit.hiking_party_participants.include?(p)}
    end

    authors_array = parse_participants(fn_array[headers.index_match(/Author/)])
    if authors_array.present?
      authors = authors_array.map do |a|
        Participant.where(given_names: a[:given_names], last_name: a[:last_name]).first_or_create()
      end
      authors.each{ |a| visit.fn_authors << a if !visit.fn_authors.include?(a) }
    end
    photographers_array = parse_participants(fn_array[headers.index_match(/Photographer/)])
    if photographers_array.present?
      photogs = photographers_array.map do |p|
        Participant.where(given_names: p[:given_names], last_name: p[:last_name]).first_or_create()
      end
      photogs.each {|p| visit.photographers << p if !visit.photographers.include?(p)}
    end



    #create the comparisons
    # comparisons.each do |c|
    #
    #       capture = Capture.find_by_fn_photo_reference(c[0])
    #       hcapture = HistoricCapture.find_by_fn_photo_reference(c[1])
    #       if !capture.nil? && !hcapture.nil? && ComparisonIndex.where(capture_id: capture.id, historic_capture_id: hcapture.id).empty?
    #         ComparisonIndex.create(capture_id: capture.id, historic_capture_id: hcapture.id)
    #       end
    #     end

    return visit, historic_visit
  end




  class Inventory


    def initialize(options = {})
      if(options.has_key?(:inventory_file))
        slurp = CSV.read(options[:inventory_file])
        @files = slurp.map{|s|s[0]}
        @import_status = slurp.map{|s|s[1]}
        @object_id = slurp.map{|s|s[2]}
        @object_type = slurp.map{|s|s[3]}
        @message = slurp.map{|s|s[4]}
      elsif(options.has_key?(:root_to_old_library))
        old_library_root = options[:root_to_old_library].chomp('/')
        if options.has_key?(:grep_filter)
          files_as_string = `find "#{old_library_root}" -type f \\( ! -iname ".*" \\) | grep #{options[:grep_filter]}`
        else
          files_as_string = `find "#{old_library_root}" -type f \\( ! -iname ".*" \\)`
        end
        files_as_array = files_as_string.split("\n")
        files_as_array.reject!{|f|f[/\/cache\//]} #there is a cache directory in the old mlp library that we want to ignore
        @files = files_as_array
        @import_status = []
        @object_id = []
        @object_type = []
        @message = []
      end
    end

    def files
      @files.dup
    end

    def update(path_string, opt={})
      #if the path is a staiton or metadata folder, then append a slash so that it doesn't match other
      #folders with names that are the same beginning
      if path_string=~/(Stations)\/[^\/]+$/
        indices=@files.index_grep(path_string+"/")
      else
        indices=@files.index_grep(path_string)
      end
      indices.each do |i|
        @import_status[i]=opt[:import_status] if opt.has_key?(:import_status)
        @object_id[i]=opt[:obj_id] if opt.has_key?(:obj_id)
        @object_type[i]=opt[:obj_type] if opt.has_key?(:obj_type)
        @message[i]= "#{@message[i]}\n#{opt[:msg]}" if opt.has_key?(:msg)
      end
    end


    def print
      @files.each_with_index do |f,idx|
        print_by_index(idx)
      end
    end

    def print_by_index(idx)
      puts @files[idx]; puts "\t" ; puts @import_status[idx]; puts "\t"; puts @object_id[idx]; puts "\t" ; puts @object_type[idx]; puts "\t" ;puts @message[idx]; puts "\n"
    end

    def print_by_match(string)
      @files.index_grep("string").each do |i|
        print_by_index(i)
      end
    end

    def save_to_csv
      CSV.open("log/#{Time.now.strftime("%h%m%s")}_populate_import_inventory.csv", "wb"){|csv| @files.each_with_index{|f,idx| csv << [f, @import_status[idx], @object_id[idx], @object_type[idx], @message[idx]] }}
    end
  end

  def create_station_from_path(stn_path)
    station_path=stn_path.chomp("/")
    #first we deal with few the staitons that belong to stations
    if station_path =~ /(BC Archives Images|Mountain Legacy Project_2011_GNP Baseline Photographs|Mountain Legacy Project_2009_Fire Lookout Tower Imagery|AFS Lookout Towers)/
      station_array = station_path.split("/")
      #in this situation, both of these folders are part of the same project, so explicitly look for the the known legacy path
      if station_path =~ /(Mountain Legacy Project_2009_Fire Lookout Tower Imagery|AFS Lookout Towers)/
        #the || is for my local machine as well as the lab machine
        project = Project.find_by_legacy_path("/MLP Library_working/Mountain Legacy Project_2009_Fire Lookout Tower Imagery") || \
          Project.find_by_legacy_path("/Volumes/mlp/MLP Library_working/Mountain Legacy Project_2009_Fire Lookout Tower Imagery")
      else
        project = Project.find_by_legacy_path(File.join(station_array.slice(0..4)))
      end
      if project.nil?
        raise StandardError, "What's up butter cup? Project is nil, which means you probably need to the known_surveyors_surveys task. This shouldn't happen."
      end
      station = Station.where(name: station_array.last, station_owner_id: project.id, station_owner_type: "Project").first_or_create
    elsif station_path =~ /\/MLP Library_working\/([^\/]+)\/([^\/]+)\/Stations\/([^\/]+)/
      #now deal with stations that belong to survey seasons and surveys,etc
      station_array = station_path.split("/")
      survey_season = create_surveyseason_from_path(File.join(station_array.slice(0..-3)))
      station = Station.where(name: station_array.last, station_owner_id: survey_season.id, station_owner_type: "SurveySeason").first || survey_season.stations.create(name: station_array.last)
      if !station || !station.valid?
        raise StandardError, "The station #{station_name} could not be found and could not be created. Errors: #{station.errors.try(:full_messages).to_s}"
      end
      return station
    else
      raise StandardError, "You've given the create_station_from_path method this path: #{stn_path}, but this doesn't match our patterns. Check the Stations matcher before the method is called."
    end
  end

  def create_surveyseason_from_path(survey_season_path)
    survey_season_path.chomp!("/")
    if survey_season_path =~ /\/MLP Library_working\/[^\/]+\/\d{4}/
      survey_season_array = survey_season_path.split("/")
      if season = SurveySeason.find_by_legacy_path(survey_season_path)
        return season
      else
        survey = Survey.find_by_legacy_path(File.join(survey_season_array.slice(0..-2)))
        if !survey
          raise StandardError, "The survey for (legacy_path): #{File.join(survey_season_array)} does not exist. Before you run this task, create this survey"
        end
        season = survey.survey_seasons.create(legacy_path: survey_season_path, geographic_coverage: survey_season_array.last[/_(.*)/].try(:slice,1..-1), year: survey_season_array.last[/^\d{4}/])
        if !season || !season.valid?
          raise StandardError, "The survey season (legacy_path) #{survey_season_path} could not be created. Errors: #{season.errors.try(:full_messages).to_s}"
        end
        return season
      end
    else
      nil
    end
  end


  def find_project(path)
    if path=~/(Mountain Legacy Project_2009_Fire Lookout Tower Imagery|AFS Lookout Towers)/
      Project.find_by_name("Alberta Fire Lookouts")
    elsif path=~/BC Archives Images/
      Project.find_by_name("BC Archives")
    elsif path=~/Mountain Legacy Project_2011_GNP Baseline Photographs/
      Project.find_by_name("Glacier National Park")
    else
      nil
    end
  end

  def project_path?(path)
    if path=~/(Mountain Legacy Project_2009_Fire Lookout Tower Imagery|AFS Lookout Towers)/
      true
    elsif path=~/BC Archives Images/
      true
    elsif path=~/Mountain Legacy Project_2011_GNP Baseline Photographs/
      true
    else
      false
    end
  end

  def survey_path?(path)
    if !project_path?(path) && path=~/\/MLP Library_working\/([^\/]+)\/{0,1}$/
      true
    else
      false
    end
  end

  def survey_season_path?(path)
    if path=~/\/MLP Library_working\/([^\/]+)\/([^\/]+)\/{0,1}$/
      true
    else
      false
    end
  end



  #on my own machine: bundle exec rake populate:known_surveyors_surveys["/MLP Library_working"]
  task :known_surveyors_surveys,[:root_to_old_library] => :environment do |t,args|
    old_root = args[:root_to_old_library]

    #Projects
    Project.where(name: "Alberta Fire Lookouts").first_or_create(legacy_path: "#{old_root}/Mountain Legacy Project_2009_Fire Lookout Tower Imagery")
    Project.where(name: "BC Archives").first_or_create(legacy_path: "#{old_root}/BC Archives Images")
    Project.where(name: "Glacier National Park").first_or_create(legacy_path: "#{old_root}/Mountain Legacy Project_2011_GNP Baseline Photographs")
    #Surveyors/Surveys

    bridgland = Surveyor.where(last_name: "Bridgland").first || Surveyor.create(last_name: "Bridgland", given_names: "M.P.", short_name: "BRI" )
    bridgland.surveys.create(name: "Revelstoke", legacy_path: "#{old_root}/Bridgland_1912_Revelstoke")
    bridgland.surveys.create(name: "Crowsnest Forest Reserve and Waterton Lakes National Park", legacy_path: "#{old_root}/Bridgland_1913-14_Crowsnest Forest Reserve - Waterton Lakes NP")
    bridgland.surveys.create(name: "Jasper National Park", legacy_path: "#{old_root}/Bridgland_1915_Jasper Park")
    bridgland.surveys.create(name: "Bow River and Clearwater Forest Reserves", legacy_path: "#{old_root}/Bridgland_1917-20_Bow River & Clearwater Forest Reserves")
    bridgland.surveys.create(name: "Kootenay and Columbia Valleys", legacy_path: "#{old_root}/Bridgland_1922-23_Kootenay & Columbia Valleys")
    bridgland.surveys.create(name: "Clearwater Forest Reserve", legacy_path: "#{old_root}/Bridgland_1924_Clearwater Forest Reserve")
    bridgland.surveys.create(name: "Revelstoke District", legacy_path: "#{old_root}/Bridgland_1925-26_Revelstoke District")
    bridgland.surveys.create(name: "Brazeau Forest Reserve and Jasper National Park", legacy_path: "#{old_root}/Bridgland_1927-28_Brazeau Forest Reserve & Jasper NP")

    wheeler = Surveyor.where(last_name: "Wheeler").first || Surveyor.create(last_name: "Wheeler", given_names: "A.O.", short_name: "WHE" )
    wheeler.surveys.create(name: "Canadian Irrigation Survey", legacy_path: "#{old_root}/Wheeler_1895-1900_Canadian Irrigation Survey")
    wheeler.surveys.create(name: "Selkirk Range", legacy_path: "#{old_root}/Wheeler_1901-02_Selkirk Range")
    wheeler.surveys.create(name: "Railway Belt", legacy_path: "#{old_root}/Wheeler_1903-07_Railway Belt")
    wheeler.surveys.create(name: "Columbia Valley", legacy_path: "#{old_root}/Wheeler_1908_Columbia Valley")
    wheeler.surveys.create(name: "Shuswap Lakes", legacy_path: "#{old_root}/Wheeler_1909_Shuswap Lakes")
    wheeler.surveys.create(name: "Mt Robson", legacy_path: "#{old_root}/Wheeler_1911_Mt Robson")
    wheeler.surveys.create(name: "Interprovincial Boundary Survey", legacy_path: "#{old_root}/Wheeler_1913-24_Interprovincial Boundary Survey")

    Surveyor.create(last_name: "Cautley", given_names: "R.W.", short_name: "CAU" )

    cochrane = Surveyor.where(last_name: "Cochrane").first || Surveyor.create(last_name: "Cochrane", short_name: "COC")
    cochrane.surveys.create(name: "Internatonal Boundary Commission", legacy_path: "#{old_root}/Cochrane_1909_Internatonal Boundary Commission")

    dawson = Surveyor.where(last_name: "Dawson").first || Surveyor.create(last_name: "Dawson", short_name: "DAW")
    dawson.surveys.create(name: "Geological Survey", legacy_path: "#{old_root}/Dawson_1881&1883_Geological Survey")

    dowling = Surveyor.where(last_name: "Dowling").first || Surveyor.create(last_name: "Dowling", short_name: "DOW")
    dowling.surveys.create(name: "Rocky Mountain Coalfields", legacy_path: "#{old_root}/Dowling_1904-06_Rocky Mountain Coalfields")

    harris = Surveyor.where(last_name: "Harris").first || Surveyor.create(last_name: "Harris", given_names: "L.E.", short_name: "HAR")
    harris.surveys.create(name: "Glacier - Revelstoke - Monashees", legacy_path: "#{old_root}/Harris_1927-30_Glacier - Revelstoke - Monashees")

    lambart = Surveyor.where(last_name: "Lambart").first || Surveyor.create(last_name: "Lambart", given_names: "L.E.", short_name: "LAM")
    lambart.surveys.create(name: "Jasper North Boundary Survey", legacy_path: "#{old_root}/Lambart_1927-28_Jasper North Boundary Survey")

    malloch = Surveyor.where(last_name: "Malloch").first || Surveyor.create(last_name: "Malloch", given_names: "G.S.", short_name: "MAL")
    malloch.surveys.create(name: "Unknown", legacy_path: "#{old_root}/Malloch_1908_")

    mca = Surveyor.where(last_name: "McArthur").first || Surveyor.create(last_name: "McArthur", given_names: "J.J.", short_name: "MCA")
    mca.surveys.create(name: "Rocky Mountains Park and Coalfields", legacy_path: "#{old_root}/McArthur_1888-92_Rocky Mts Park & Coalfields")

    nic = Surveyor.where(last_name: "Nichols").first || Surveyor.create(last_name: "Nichols", given_names: "D.A.", short_name: "NIC")
    nic.surveys.create(name: "Pekisko Creek", legacy_path: "#{old_root}/Nichols_1915_Pekisko Creek")

    nid = Surveyor.where(last_name: "Nidd").first || Surveyor.create(last_name: "Nidd", given_names: "M.E.", short_name: "NID")
    nid.surveys.create(name: "Willmore", legacy_path: "#{old_root}/Nidd_1944-1945")

    notman = Surveyor.where(last_name: "Notman").first || Surveyor.create(last_name: "Notman",given_names: "William McFarlane", short_name: "NOT")
    notman.surveys.create(name: "Unknown", legacy_path: "#{old_root}/Notman, William McFarlane_1887")

    riggal = Surveyor.where(last_name: "Riggal").first || Surveyor.create(last_name: "Riggal", short_name: "RIG")
    riggal.surveys.create(name: "Waterton Lakes", legacy_path: "#{old_root}/Riggal_ca._1905-1940_Waterton Lakes")

    royal = Surveyor.where(affiliation: "Royal Engineers").first || Surveyor.create(affiliation: "Royal Engineers")
    royal.surveys.create(name: "International Boundary Commission", legacy_path: "#{old_root}/Royal Engineers_1861-1874_International Boundary Commission")

    tag = Surveyor.where(last_name: "Taggart").first || Surveyor.create(last_name: "Taggart", given_names: "C.H.", short_name: "TAG")
    tag.surveys.create(name: "Kamloops - Shuswap", legacy_path: "#{old_root}/Taggart_1924-28_Kamloops - Shuswap")

    mcc= Surveyor.where(last_name: "McCaw").first || Surveyor.create(last_name: "McCaw", given_names: "R.D.", short_name: "MCC")
    mcc.surveys.create(name: "Windermere Motor Road", legacy_path: "#{old_root}/McCaw_1913_Windermere Motor Road")
  end

  task :reset => :environment do
    debugger
    #surveyors = Surveyor.all
    #surveyors.each(&:destroy)
    #surveys = Survey.all
    #surveys.each(&:destroy)
    #Project.all.each(&:destroy)
    SurveySeason.all.each(&:destroy)
    Station.all.each(&:destroy)
    Visit.all.each(&:destroy)
    Location.all.each(&:destroy)
    Capture.all.each(&:destroy)
    HistoricCapture.all.each(&:destroy)
    HistoricVisit.all.each(&:destroy)
    Keyword.all.each(&:destroy)
    KeywordVisitAssociation.all.each(&:destroy)
    Camera.all.each(&:destroy)
    Participant.all.each(&:destroy)
    HikingParty.all.each(&:destroy)
    CaptureImage.all.each(&:destroy)
    Image.all.each(&:destroy)
    MetadataFile.all.each(&:destroy)
    FieldNote.all.each(&:destroy)
    ComparisonIndex.all.each(&:destroy)
  end



  class MLPImage
    attr_accessor :path
    def initialize(path)
      raise StandardError, "The file path #{path} does not exist" if !File.exists?(path)
      self.path = Pathname.new(path)
      begin
        @exif = MiniExiftool.new(path.to_s, :convert_encoding => true)
      rescue
        @exif=nil
      end
    end

    #****Class Methods****#

    #group takes an array of mlpimages and "groups" them into sub arrays of similarity
    def self.group(array)
      grouped = []
      until array.empty?
        temp = [array[0]]
        group, array = array.partition do |r|
          #note the trick here is that == is overloaded for mlpimage
          if temp.include?(r)
            temp << r
            true
          else
            false
          end
        end
        grouped << group
      end
      return grouped
    end

    #mlpimage_array is an array of mlpimage objects
    #group_array is an array of mlpimage arrays
    #comparable_groups returns the array of mlpimages
    #in group_array that contains at least mlpimage that
    #is comparable to at least one mlpimage in mlpimages_array
    def self.comparable_groups(mlpimage_array, group_array)
      comparable_group = []
      group_array.each do |g|
        g.each do |m1|
          mlpimage_array.each do |m2|
            return g if m1.comparable?(m2)
          end
        end
      end
      return nil
    end

    #*** End Class Methods ***#

    def filename
      self.path.basename.to_s.chomp(self.path.extname.to_s)
    end

    def extension
      self.path.extname
    end

    def capturetime
      return nil if @exif.nil?
      ct = @exif.datetimeoriginal
      begin
        ct.to_date
        #if ct throws an exception return nil, otherwise return ct (bad programming, i know)
        return ct
      rescue
        return nil
      end
    end

    def gps_capturetime
      return nil if @exif.nil?
      @exif.gpstimestamp
    end

    def description
      return nil if @exif.nil?
      @exif.description
    end

    def image_state
      if self.path.to_s.match(/Image Variant.*Repeat Master/)
        "MISC"
      elsif self.path.to_s.match(/Image Variant.*Original Master/)
        "MISC"
      elsif self.path.to_s.match(/Image Variant.*Repeat 3FR/)
        "MISC"
      elsif self.path.to_s.match(/Image Variant.*Repeat Tiff/)
        "MISC"
      elsif self.path.to_s.match(/Image Variant.*Original Scan/)
        "MISC"
      elsif self.path.to_s.match(/Repeat Master/)
        "MASTER"
      elsif self.path.to_s.match(/Repeat 3FR/)
        "RAW"
      elsif self.path.to_s.match(/Repeat Tiff/)
        "INTERIM"
      elsif self.path.to_s.match(/Original Masters\/old-/)
        "INTERIM"
      elsif self.path.to_s.match(/Original Master/)
        "MASTER"
      elsif self.path.to_s.match(/Original Scan/)
        "RAW"
      elsif self.path.to_s.match(/Gridded/)
        "GRIDDED"
      else
        "MISC"
      end

    end

    def repeat?
      if self.path.to_s[/Repeat/].present? || self.path.to_s[/Alternate/].present? || self.path.to_s[/Illecillewaet.+Image Variants.+/].present?
        return true
      elsif self.filename[/(MLP|HIG)\d{4}/]
        year = self.filename[/\d{4}/].to_i
        return year>1995
      else
        return false
      end
    end

    def original?
      if self.path.to_s[/Original/].present? || self.path.to_s[/AFS Lookout.+Image Variants.+/]
        return true
      elsif self.filename[/[A-Z]{3}\d{4}/]
        year = self.filename[/\d{4}/].to_i
        return (year>1800 && year<1995)
      else
        return false
      end
    end

    def location?
      self.path.to_s[/Location Photo/].present?
    end

    def fieldnote?
      self.path.to_s[/Field Notes/].present?
    end

    def alternate?
      self.filename[/_a\.[^\.]*$/].present?
    end

    def variant?
      self.path.to_s[/Image Variant/].present?
    end

    #When an image is stored under the Image Variants folder and the sub folders are not named with the "Original"
    #or "Repeat" convention, then assume that folder contains information about where the image came from
    def comments
      if (self.repeat? || self.original?) && self.variant? && !self.path.to_s[/(Repeat|Original)/]
        if match_obj = self.path.to_s.match(/.*Image Variants\/([^\/]+)\/#{self.filename}/)
          match_obj[1]
        end
      end
    end

    def comparable?(mlpimage)
      (self.original? && mlpimage.repeat? || self.repeat? && mlpimage.original?) && \
      (!self.parse_plate.blank? && mlpimage.parse_plate==self.parse_plate)
    end

    def to_stringio
      #img_binary = File.open(self.path.to_s){ |i| i.read }
      #img_encoded = Base64.encode64(img_binary)
      #io = FilelessIO.new(Base64.decode64(img_encoded))
      #io.original_filename = self.path.basename.to_s
      #io = FilelessIO.new(self.path.to_s) should only need this now
      io = FilelessIO.new(self.path.to_s,dummy: false)
      return io
    end

    #loosely defined similarity method
    def ==(mlpimage)
      mlpimage.filename==self.filename || \
      (!self.capturetime.blank? && mlpimage.capturetime==self.capturetime) || \
      (!self.gps_capturetime.blank? && mlpimage.gps_capturetime==self.gps_capturetime) || \
      (!self.description.blank? && mlpimage.description==self.description)
    end

    def parse_plate
      if match_obj = self.filename.gsub(/_a$/,"").match(/[A-Z]{3}\d{4}_([^_]+)$/)
        return match_obj[1]
      else
        return nil
      end
    end

  end


  #parsing coordinates like "50°30'04.92","50°30'04.92", "117°30.769'W (5m)"
  #returns degree,minute,second,cardinal direction.
  def parse_coordinates(coord)
    return nil if coord.blank?
    match_obj = /(-){0,1}(\d*)(¼|°|º)(\d*\.{0,1}\d*)'{0,1}(\d*\.{0,1}\d*)"{0,1}(N|W|E|S){0,1}.*/.match(coord)
    return nil if !match_obj
    degree=match_obj[2]
    minute=match_obj[4]
    second=match_obj[5]
    #if there is a minus in the degree, it will override the explicit cardinal direction (although situation isn't expected)
    if match_obj[1]=="-"
      cardinal_direction=2
    else
      cardinal_direction=match_obj[6]
    end
    return [degree,minute,second,cardinal_direction]
  end

  #note there are customization for the expected region of the coordinates. Do not use generally
  def parse_UTM_coordinates(text)
    unless text=~/(UTM|Waypoint|GPS)/i
      raise ArguementError, "This method expects the text input to have UTM coordinates (it should contain the substring UTM)"
    end
    if text=~/.*Zone\s{0,1}(\d{2})[^\d]/i || text=~/.*Zone.*\((\d{2})[A-Z]\)/i
      zone = $1
      zone = zone+"U"
    else
      #it is almost always going to be
      zone = '11U'
    end

    if text=~/0{0,1}(\d{6})\s{0,1}(?:E$|E[^a-zA-Z]|N\\r)/i || text=~/(?:east|e):{0,1}\s{0,1}0{0,1}(\d{6})/i
      easting = $1
      easting = easting.to_i
    elsif text=~/0{0,1}(\d{5})\s{0,1}(?:E$|E[^a-zA-Z]|N\\r)/i || text=~/(?:east|e):{0,1}\s{0,1}0{0,1}(\d{5})/i
      easting = $1
      #assume user error, add a zero to the end, because otherwise it would be in the west coast
      easting = "#{easting}0".to_i
    else
      #debugger
      easting = nil
    end

    if text=~/0{0,1}(\d{7})\s{0,1}(?:N$|N[^a-zA-Z]|N\\r)/i || text=~/(?:north|n):{0,1}\s{0,1}0{0,1}(\d{7})/i
      northing = $1
      northing = northing.to_i
    elsif text=~/0{0,1}(\d{6})\s{0,1}(?:N$|N[^a-zA-Z]|N\\r)/i || text=~/(?:north|n):{0,1}\s{0,1}0{0,1}(\d{6})/i
      northing = $1
      #assumes user error, add a zero to the end, otherwise coordinate is off the coast of mexico
      northing = "#{northing}0".to_i
    else
      #debugger
      northing = nil
    end

    #if both easting and northing are nil, we give it one more chance (sometimes "N" and "E" aren't specified
    if easting.nil? && northing.nil?
      if text=~/\d{5,}/
        matches = text.scan(/\d{5,}/)
        return nil if matches.count!=2
        num1 = matches[0]
        num1 = num1.to_i
        num2 = matches[1]
        num2 = num2.to_i
        #set easting
        if num1<750000 && num1>550000
          #then it is probably an easting
          easting = num1
        elsif num1<75000 && num1>55000
          #then it is probably an easting, but someone forgot to write a zero
          easting = num1*10
        end
        if num2>5400000 && num2<7000000
          northing = num2
        elsif num2>540000 && num2<700000 && easting.present?
          northing = num2*10
        end
      end
      if easting.nil? || northing.nil?
        return nil
      end
    end


    if text=~/Elevation\s{0,1}:{0,1}\s{0,1}(\d+)\s{0,1}(feet|ft)/i || text=~/elev:\s(\d+)\s{0,1}(feet|ft)/i
      elevation = $1
      elevation = elevation.to_i
      #convert to meters
      elevation = elevation*0.3048
    elsif text=~/Elevation\s{0,1}:{0,1}\s{0,1}(\d+)/i || text=~/elev:\s(\d+)/i
      elevation = $1
      elevation = elevation.to_i
      #not sure if it is in feet or meters, guess based on size
      if elevation > 3500
        #convert to meters
        elevation = elevation*0.3048
      end
    else
      #not receiving elevation is not a big deal
      elevation = nil
    end

    return [zone,easting,northing,elevation]
  end

  #Parses a string that is a list of keywords separated by commas. Downcase and remove leading and trailing whitespaces to remove duplicates
  def parse_keywords(keywords_string)
    return keywords_string if keywords_string.nil?
    return keywords_string.split(",").map{|k|k.strip.downcase}.reject(&:blank?)
  end

  def parse_participants(participants_string)
    return nil if participants_string.blank?
    participants_string.strip!
    participants_string.chomp!(",")
    names_array = participants_string.split(",").map do |p|
      names=p.strip.split(" ")
      if names.count==1
        {given_names: names[0], last_name: "Unknown" }
      else
        {given_names: names.slice(0..-2).join(" ") , last_name: names[-1]}
      end
    end
    return names_array.reject{|n| n.all?{|k,v| v.blank? }}
  end

  #sometimes fn_photo_references in the field notes are written like "A00123 (ev+)", "A00123(ev+)", "A00123 ev+"
  #we want the first part as our reference, and the second part as a comment.
  #returns [reference, comment], where comment is "" if there is no comment
  def parse_photo_reference(reference)
    ref = /^[^ \(]*/.match(reference).to_s
    comment = reference.sub(ref,"")
    #remove leading space and brackets, if they exist
    comment = comment.sub(/^ /,"").gsub(/[\(|\)]/, "")
    return [ref,comment]
  end


  task :util, [:manifestDirectories, :manifestFiles]=> :environment do |t,args|
    # -stations with originals, no repeats, no field notes - we have photos but repeats have not been taken
    # -stations that don't have filed notes, but have repeats or originals. Field notes have not been transcribed or are missing
    # -stations with no files and folders
    # -

    directories = File.read(args[:manifestDirectories])
    directories = directories.split("\n")
    files = File.read(args[:manifestFiles])
    files = files.split("\n")
    files.reject!{|f|f[/\/\./]} #we don't want to keep track of the .DS_Store .whatever type files
    stations = directories.grep(/Stations\/[^\/]*$/)
    station_files = files.group_by {|f| f[/MLP Library.*Stations\/([^\/]*){1}/]}
    stations_with_no_files = stations.select{ |s| station_files[s].nil? }
    stations = stations - stations_with_no_files
    #not correct, these have Original Masters, which must have been masters, and therefore repeat are missing
    stations_missing_repeats_fn = stations.select {|s| station_files[s].grep(/Original Masters/).length>0 && \
       station_files[s].grep(/Repeat/).length==0 &&  \
       station_files[s].grep(/Field Notes/).length==0 }
    stations = stations - stations_missing_repeats_fn
    stations_not_repeated = stations.select {|s| station_files[s].grep(/Original Scans/).length>0 && \
       station_files[s].grep(/Repeat/).length==0 &&  \
       station_files[s].grep(/Field Notes/).length==0 }
    stations = stations - stations_not_repeated
    #missing transcription of field notes, but has files in the field notes folder
    stations_missing_transcribed_fn = stations.select{|s| station_files[s].grep(/Field Notes\/TranscribedFN.*/).length==0 && station_files[s].grep(/Field Notes\//).length>0 && station_files[s].grep(/(Repeat|Original)/).length>0 }
    stations = stations - stations_missing_transcribed_fn
    #missing field notes, but has images, so should have them
    stations_missing_fn = stations.select{|s| station_files[s].grep(/Field Notes/).length==0 && station_files[s].grep(/(Repeat|Original)/).length>0  }
    stations = stations - stations_missing_fn
    stations_with_transcribed_fn = stations.select{|s| station_files[s].grep(/TranscribedFN/).length>0}
    stations = stations - stations_with_transcribed_fn
    puts "stations with non-standard file structure #{stations.count} \n stations with transcribed field notes #{stations_with_transcribed_fn.count} \n stations that needs field notes transcribed #{stations_missing_transcribed_fn.count}\n stations that are completely missing fieldnotes, but have repeats or originals #{stations_missing_fn.count}\n stations not repeated (suspected) #{stations_not_repeated.count}\n stations that have original masters, but no repeats #{stations_missing_repeats_fn.count}\n"
    CSV.open("log/stns_non-standard.csv", "wb") {|csv| stations.each {|s| csv << [s] } }
    CSV.open("log/stns_transcribed.csv", "wb") {|csv| stations_with_transcribed_fn.each {|s| csv << [s] } }
    CSV.open("log/stns_missing_transcribed_fn.csv", "wb") {|csv| stations_missing_transcribed_fn.each {|s| csv << [s] } }
    CSV.open("log/stns_missing_fnd.csv", "wb") {|csv| stations_missing_fn.each {|s| csv << [s] } }
    CSV.open("log/stns_not_repeated.csv", "wb") {|csv| stations_not_repeated.each {|s| csv << [s] } }
    CSV.open("log/stns_missing_repeats_fn.csv", "wb") {|csv| stations_missing_repeats_fn.each {|s| csv << [s] } }
  end



end
