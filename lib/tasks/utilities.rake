require 'csv'
load(Rails.root.join('lib','tasks','task_helper_methods.rb'))
namespace :utilities do
  task :import_transcription_records, [:import_file] => :environment do |t,args|
    load 'lib/tasks/transcription_records_parser.rb'
    load 'lib/tasks/custom_visit.rb'
    visit_list = TranscriptionRecordsParser.new(args[:import_file]).parse_records
    visit_list.each do |visit_fn_record|
      if visit = CustomVisit.find_by_info(visit_fn_record[:fns_that_have_been_scanned], visit_fn_record[:station], visit_fn_record[:survey_year])
        visit.update_attributes( fn_physical_location: visit_fn_record[:field_note_book__binder_number_year_pages], fn_transcription_comment: String(visit_fn_record[:commentserrorsadditional_information])<<String(" --#{visit_fn_record[:updated_by]}"))
        puts "updated visit #{visit.date} (id #{visit.id}) with #{visit.fn_physical_location}"
      else
        puts "Where should I put this (enter a visit id)"
        puts visit_fn_record.to_a
        answer = STDIN.gets.chomp
        if answer=~/\d{1,7}/
          id = answer.to_i
          visit = Visit.find(id)
          visit.update_attributes( fn_physical_location: visit_fn_record[:field_note_book__binder_number_year_pages], fn_transcription_comment: String(visit_fn_record[:commentserrorsadditional_information])<<String(" --#{visit_fn_record[:updated_by]}"))
        else
          puts "failed: #{visit_fn_record.to_a}"
        end
      end
    end
  end

  #on my own machine: bundle exec rake utilities:build_task_list["/Users/chrisgat/WHE_OTHERS-Feb021360260437_populate_import_inventory.csv"]
  task :build_task_list, [:import_log,:output_file] => :environment do |t,args|
    debugger
    return "Missing arguments. Must supply import log and an output file name/path" if args[:import_log].blank? || args[:output_file].blank?
    csv_array = CSV.read(args[:import_log])
    ### temporaray for testing ####
    # files = csv_array.map{|s|s[0]}
    #
    #     all_stations = files.group_by{|f| f[/.*Stations\/[^\/]+/]}.keys
    #     all_stations.compact!
    #     status = Array.new(all_stations.count, "")
    #     all_stations.each_with_index do |s,i|
    #       status[i]=import_status(s,csv_array).join(",")
    #     end
    #     CSV.open("test.csv", "wb"){|csv| all_stations.each_with_index{|f,idx| csv << [f, status[idx]] }}
    ### end test code ####
    #script to create station tasks
    col1_legacy_paths = []
    col2_surveyor = []
    col3_survey = []
    col4_season = []
    col5_station_name = []
    col6_import_status = []
    col7_task_status = []
    stations = Station.all

    stations.each_with_index do |stn,i|
      stn_legacy_path=stn.legacy_path
      if stn_legacy_path.blank?
        puts "Station: #{stn.name}, id: #{stn.id.to_s}. Does not have a legacy path"
        next
      end

      col1_legacy_paths[i]=stn_legacy_path
      col5_station_name[i]=stn.name
      #first load named columns for this station
      if stn.station_owner_type=='SurveySeason'
        col4_season[i]=stn.station_owner.year.to_s
        col3_survey[i]=stn.station_owner.survey.name
        col2_surveyor[i]=stn.station_owner.survey.surveyor.display_name
      else
        col2_surveyor[i]=stn.station_owner.name
      end
      #set import status codes
      col6_import_status[i]=import_status(stn_legacy_path,csv_array).join(",")
      #set task_status
      field_note_dir = File.join(stn_legacy_path,"Field Notes")
      if Dir.exists?(field_note_dir) && Dir.entries(field_note_dir).reject{|i| i=~/^\./}.empty?
        col7_task_status[i]=["MISSING_FN"]
      elsif stn.visits.present? && stn.visits.any?{|v| !v.from_transcribed_field_notes }
        col7_task_status[i]=["TRANSCRIBE_AND_MANAGE"]
      else
        #at this point, we know that the visits in the station have come from transcribed field notes
        #so now we just try to see if everything looks alright
        col7_task_status[i]=[]
        if stn.captures.any?{|cap|cap.capture_owner_type!='Location'}
          col7_task_status[i] << "CAP_NOT_IN_LOCATION"
        end
        if stn.visits.any?{|v|v.locations.any?{|l|l.captures.empty?}}
          col7_task_status[i] << "EMPTY_LOCATION"
        end
        if stn.captures.any?{|cap|cap.fn_photo_reference.blank? && cap.capture_images.present?}
          col7_task_status[i] << "CAP_WITHOUT_FN_REF"
        end
        if stn.captures.any?{|cap|cap.historic_captures.empty?}
          col7_task_status[i] << "CAP_WITHOUT_COMPARISON"
        end
        if stn.historic_visit && stn.historic_visit.historic_captures.any?{|hcap| hcap.fn_photo_reference.blank? && hcap.capture_images.present?}
          col7_task_status[i] << "HCAP_WITHOUT_FN_REF"
        end
      end
    end
    CSV.open("#{args[:output_file]}", "wb"){|csv| stations.each_with_index{|f,idx| csv << [col1_legacy_paths[idx],col2_surveyor[idx],col3_survey[idx],col4_season[idx],col5_station_name[idx],col6_import_status[idx],col7_task_status[idx].join(",") ] }}
  end



  def import_status(stn_legacy_path, csv_array)
    files = csv_array.map{|s|s[0]}
    import_status = csv_array.map{|s|s[1]}
    message = csv_array.map{|s|s[4]}
    stn_files = files.select{|f|f=~/#{Regexp.escape(stn_legacy_path)}/}
    return [] if stn_files.empty?
    stn_indices = stn_files.map{|f|files.index(f)}
    status_array=[]
    stn_indices.each do |i|
      if message[i]=~/Cannot create visit for image due to lack of creation date and field notes/
        status_array << "NO_FN_OR_IMG_DATE"
      elsif message[i]=~/More than one visit exists under this station/
        status_array << "MULTI_VISIT_ERROR"
      elsif message[i].blank? && files[i]=~/Original Gridded.+pdf$/
        status_array << "GRIDDED_IMGS_PDF"
      elsif files[i]=~/\.avi$/i
        status_array << "LOCATION_VIDEO_FILES"
      elsif message[i]=~/old_ files will not be automatically imported. Make sure to check that original exists before deleting/
        #status_array << "OLD_IMGS"
      elsif message[i]=~/(\.log|\.mlp|\.xmp)/
        #status_array << "FILES_NOT_FOR_IMPORT"
      elsif message[i]=~/blended images/
        #status_array << "BLENDED_IMGS"
      elsif message[i].present? || (message[i].blank? && import_status[i].blank?)
        status_array << "UNKNOWN"
      elsif import_status[i]!="Y"
        #at this point the import status should be Y, otherwise I've missed something
        raise StandardError, "Unexcepted situation: #{import_status[i]} #{message[i]}"
      end
    end
    status_array = status_array.compact.uniq
    if status_array.empty?
      status_array = ["COMPLETE"]
    end
    return status_array
  end

  task :merge_hcaps, [:merge_log,:noop] => :environment do |t,args|
    debugger
    noop=false
    if args[:noop]=="noop"
      noop=true
    end
    log = File.new(args[:merge_log],'w+')
    begin
      Station.all.each{|s|merge_station_hcaps(s,noop,log)}
    ensure
      log.close
    end
  end

  #some historic captures were erroneously put in the metadata section of the visit.
  #this one off task should fix it
  task :metadata_hcaps_move => :environment do
    debugger
    files = MetadataFile.where("metadata_file similar to '%(TIF|JPG)' and metadata_owner_type = 'Visit'")
    files.each do |m|
      visit = m.metadata_owner
      station = visit.station
      historic_visit = station.historic_visit
      matching_hcap = nil
      historic_visit.historic_captures.each do |hcap|
        matches = hcap.capture_images.select{|ci|ci.display_name.chomp("."+ci.image.file.extension) == m.display_name.chomp("."+m.metadata_file.file.extension) }
        if matches.present?
          matching_hcap = hcap
          break
        end
      end
      if matching_hcap.nil?
        historic_visit.historic_captures.each do |hcap|
          matches = hcap.capture_images.select{|ci|ci.display_name.chomp("."+ci.image.file.extension) =~ /#{Regexp.escape(m.display_name.chomp("."+m.metadata_file.file.extension))}/ || m.display_name.chomp("."+m.metadata_file.file.extension) =~ /#{Regexp.escape(ci.display_name.chomp("."+ci.image.file.extension))}/ }
          if matches.present?
            matching_hcap = hcap
            break
          end
        end
      end
      if matching_hcap.nil?
        historic_visit.historic_captures.each do |hcap|
          if m.display_name.chomp("."+m.metadata_file.file.extension) =~ /#{Regexp.escape(hcap.display_name)}/
            matching_hcap = hcap
            break
          end
        end
      end
      if matching_hcap.present?
        image_state = m.legacy_path =~ /Original Masters/ ? "MASTER" : "MISC"
        image = FilelessIO.new(m.legacy_path)
        ci = matching_hcap.capture_images.create(image: image, image_state: image_state, legacy_path: m.legacy_path)
        if CaptureImage.find_by_legacy_path(m.legacy_path)
          puts "put #{m.display_name} (#{m.id}) into historic capture #{matching_hcap.display_name} (#{matching_hcap.id}). cap img id is : #{ci.id}"
          m.destroy
          puts "destroyed old metadata file."
        else
          puts "failed to create the capture image for #{m.display_name} (#{m.id}). Reason: #{ci.error.full_messages.to_s}"
        end
      else
        puts "failed to find a place to put metadata file #{m.display_name} (#{m.id}). Station is: #{station.id}"
      end
    end
    nil
  end

 def merge_station_hcaps(station,noop,logfile)
   return if station.historic_visit.nil?
   return if station.historic_visit.historic_captures.empty?
   hcaps = station.historic_visit.historic_captures
   #hcaps_fnrefs, hcaps_norefs = hcaps.partition{|h|h.fn_photo_reference.present?}
   ignore_list=[]
   hcaps.each do |i|
     hcaps.each do |j|
       if i==j
         next
       else
         if i.display_name.length<3 || j.display_name.length<3 || ignore_list.include?(i) || ignore_list.include?(j)
           next
         else
           if historic_similarity_check(i.display_name,j.display_name) && !(i.fn_photo_reference.present? && j.fn_photo_reference.present?)
             if noop
               puts "Merged #{j.display_name}(id: #{j.id.to_s}) and #{i.display_name}(id: #{i.id})"
             else
               #save display names because one of these will get destroyed in merge and want to print to log.
               i_display_name = i.display_name
               j_display_name = j.display_name
               puts "Would you like to merge #{i_display_name}(#{i.fn_photo_reference.present?}) with #{j_display_name}(#{j.fn_photo_reference.present?})?[ynR]"
               answer = STDIN.gets
               answer.chomp!
               if answer=='y'
                 merged_hcap=merge_hcaps(i,j)
                 if merged_hcap.nil?
                   puts "Could not merged #{j_display_name}(id: #{j.id.to_s}) and #{i_display_name}(id: #{i.id})"
                   logfile << "Could not merged #{j_display_name}(id: #{j.id.to_s}) and #{i_display_name}(id: #{i.id})\n"
                 else
                   if merged_hcap==i
                     ignore_list << j
                   elsif merged_hcap==j
                     ignore_list << i
                   end
                   puts "Merged #{j_display_name}(id: #{j.id.to_s}) and #{i_display_name}(id: #{i.id})"
                   logfile << "Merged #{j_display_name}(id: #{j.id.to_s}) and #{i_display_name}(id: #{i.id})\n"
                 end
               elsif answer=='R'
                 return nil #leaves the method, essentially moving on to next station.
               end
             end
           end
         end
       end
     end
   end
   return nil
 end



 #Puts all the data from hcap2 into hcap1.
 def merge_hcaps(hcap1,hcap2)
   #we always merge to the hcap with a fn_photo_reference
   if hcap2.fn_photo_reference.present? && hcap1.fn_photo_reference.blank?
     temp = hcap2
     hcap2 = hcap1
     hcap1 = temp
   end
   hcap1.attributes.each_pair do |name1, value1|
     next if ['created_at','updated_at','id','hcapture_owner_type','hcapture_owner_id','condition'].include?(name1)
     value2 = hcap2.send(name1)
     if value1.present? && value2.present?
       return nil
     elsif value1==value2
       next
     elsif value1.blank?
       hcap1.send(name1+"=",value2)
     end
   end
   hcap2.capture_images.each do |ci|
     ci.captureable = hcap1
     ci.save
   end
   hcap2.captures.each do |cap|
     hcap1.add_comparison(cap)
     hcap2.remove_comparison(cap)
   end
   if hcap2.reload.capture_images.present? || hcap2.captures.present?
     raise StandardError, "Historic Capture merge failed becaues hcap2 still has capture_images (all should have been copied to hcap1) or comparisons"
   elsif hcap1.save
     hcap2.destroy
   else
     raise StandardError, "Could not save merged hcap1. Errors: #{hcap1.errors.full_messages.to_s}"
   end
   return hcap1
 end

end


