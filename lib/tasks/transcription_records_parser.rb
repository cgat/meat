require 'csv'
require 'delegate'

class TranscriptionRecordsParser
  def initialize(file_path)
    @table = CSV.table(file_path)
  end

  def parse_records
    filter_blanks(:fns_that_have_been_scanned)
    split_records
    convert_dates
    @table
  end

  private

  def filter_blanks(column_name)
    column_name = column_name.to_sym
    @table.delete_if{|row| row[column_name].blank? }
  end

  def split_records
    splitters, @table = @table.partition{|row| row[:fns_that_have_been_scanned]=~/;/}
    splitters.each do |row|
      row[:fns_that_have_been_scanned].split(";").zip(row[:field_note_book__binder_number_year_pages].split(";")).each do |date, book_string|
        new_row = CSV::Row.new(row.headers, row.fields)
        new_row[:fns_that_have_been_scanned]=date
        new_row[:field_note_book__binder_number_year_pages]=book_string
        @table << new_row
      end
    end
  end

  def convert_dates
    @table.each do |row|
      row[:fns_that_have_been_scanned] = Date.strptime(row[:fns_that_have_been_scanned],"%m/%d/%Y")  rescue row[:fns_that_have_been_scanned]
    end
  end

end
