#load('lib/tasks/task_helper_methods.rb')

#historic similarty check is used to compare two strings, which are from the historic image file name
#or from the historic capture field note reference. Because the formats can vary so much, this doesn't
#guarantee that the check result will be correct. Best used when str1 is an fn_photo_reference and str2
#is a file name.
def historic_similarity_check(str1,str2)
  return false if str1=="BRI1913_B13-1-W"
  str1_end = str1.gsub(/-[A-Z]-\d{1,2}/,"") #take off endings like -W-16, -B-12
  str1_end = str1_end.chomp("_Nichols 1916") #only does anything to Nichols files
  str1_end = str1_end.chomp("tif") #some files like BRI1234_B-234atif (not an extension)
  str1_end = str1_end.gsub(/(-|_)(s|w|n|e|se|sw|nw|ne|v6|copy_2|copy|highg|1920|Test-views)$/i,"")
  if str1_end =~ /\d{1,2}-\d{2}$/ && str2=~/[A-Z]{3}1(?:8|9)(\d{2})/
    str1_end = str1_end.gsub(/-#{$1}$/,"")
  end
  if str1_end.length==1
    return false
  elsif str1_end.length>=3
    #special case first
    if str2=~/(WHE1898_\d{1,2}-\d{2}-W-98|MCA18\d{2})|WHE1897_W97/
      str1_end = str1_end.slice(-4..-1)
    else
      str1_end = str1_end.slice(-3..-1)
    end
  end
  if str2.gsub(/[A-Z]{3}1(?:8|9)(\d{2})/,"")=~/#{Regexp.escape(str1_end)}/
    return true
  else
    return false
  end
end

def repeat_similarity_check(str1,str2)
  return false if str1.blank? || str2.blank?
  str1_end = str1.gsub(/-[A-Z]-\d{1,2}/,"") #take off endings like -W-16, -B-12
  str1_end = str1_end.chomp("tif") #some files like BRI1234_B-234atif (not an extension)
  str1_end = str1_end.sub(/(-|_)(medc|null|master|highc|s|w|n|e|se|sw|nw|ne|v6|copy_2|copy|highg|1920|Test-views)$/i,"")
  str1_end = str1_end.sub(/-W-9\d$/,"")
  if str1_end.length==1
    return false
  elsif str1_end.length>=3
    #special case first
    if str2=~/(MLP\d{4}_\d{1,2}-\d{2}-W-9\d)/
      str1_end = str1_end.slice(-4..-1)
    else
      str1_end = str1_end.slice(-3..-1)
    end
  end
  if str2.gsub(/[A-Z]{3}20(\d{2})/,"")=~/#{Regexp.escape(str1_end)}/
    return true
  else
    return false
  end
end



def metadata_psd_to_hcap(metadata_array, noop=false)
  debugger
  metadata_array.each do |m|
    puts m.legacy_path
    image_state = "MISC"
    #figure out the image state based on the path name
    if m.legacy_path =~ /.*Gridded Images.*/
      image_state = "GRIDDED"
    elsif m.legacy_path =~ /.*Original Masters.*/
      image_state = "MASTER"
    elsif m.legacy_path =~ /.*Original Scan.*/
      image_state = "RAW"
    end
    station = m.metadata_owner.station #assume visit owner
    hcaps = station.historic_captures
    hcap_images = hcaps.map{|h|h.capture_images}.flatten.compact
    hcap_images.each do |hci|
      if historic_similarity_check(m.metadata_file.file.basename, hci.display_name.chomp("."+hci.image.file.extension) ) #      if m.metadata_file.file.basename==hci.display_name.chomp("."+hci.image.file.extension)
        hcap = hci.captureable
        if noop
          puts "noop: moved #{m.display_name} (#{m.id.to_s}) to #{hcap.display_name} (#{hcap.id})."
          break
        elsif new_img=hcap.capture_images.create(image: FilelessIO.new(m.legacy_path), image_state: image_state, legacy_path: m.legacy_path)
          puts "moved #{m.display_name} (#{m.id.to_s}) to #{hcap.display_name} (#{hcap.id}). Capture image is #{new_img.id}"
          m.destroy
          FileUtils.rm(Dir.glob("/private/var/folders/z7/4gdh6fps3874q8tp2rkyk5v4000rqy/T/magick*"))
          break
        else
          puts "hmm, tried to add #{m.display_name} (#{m.id.to_s}) to #{hcap.display_name} (#{hcap.id}), but no luck"
        end
      end
    end

  end
end
#used to import the previously unimported files
#requires a block
def import_unimported(file_filter,msg_filter)
  debugger
  ActiveRecord::Base.logger=nil
  @counter=0
  l = ImportLogCSV.new("log/import_log_google.csv")
  rows = l.grep_filter(file_filter,/N/,"all","all",msg_filter)+l.grep_filter(file_filter,"blank","all","all",msg_filter)
  begin
    rows.each do |row|
      if CaptureImage.where(legacy_path: row[0]).count==0 && \
        FieldNote.where(legacy_path: row[0]).count==0 && \
        MetadataFile.where(legacy_path: row[0]).count==0 && \
        Image.where(legacy_path: row[0]).count==0
        puts "Trying to update ...#{row[0].slice(-70..-1)}"
        if station = get_station(row[0])
          yield row, station, l
        else
          puts "Station does not exist"
        end
      else
        puts "File exists in database already! Updating spreadsheet"
        obj = CaptureImage.where(legacy_path: row[0]).first ||  \
          FieldNote.where(legacy_path: row[0]).first ||  \
          MetadataFile.where(legacy_path: row[0]).first ||  \
          Image.where(legacy_path: row[0]).first
        row[1] = "Y"
        row[2] = obj.id.to_s
        row[3] = obj.class.name
        row[4] = ""
        l.update_row(row)
      end
    end
  ensure
    l.save
    puts "Imported #{@counter} files"
  end
  nil
end

def regroup_station_captures_images(station)
  captures = station.captures
  ci_init_groups = captures.map{|a|a.capture_images.map(&:id).to_set}.reject(&:blank?).to_set
  cap_imgs = station.captures.map{|a|a.capture_images}.flatten
  raws, others = cap_imgs.partition{|a|a.legacy_path=~/.*Stations.*\/B([^\/]+)\.fff$/i}
  others_by_caption = others.group_by{|a|MiniExiftool.new(a.legacy_path, :convert_encoding => true).try(:captionabstract)}
  if others_by_caption.has_key?(nil)
    return false
  end
  raws.each do |r|
    basename = $1 if r.legacy_path=~/.*Stations.*\/([^\/]+)\.[[:alnum:]]{3,4}$/i
    others_by_caption[basename] = [] if others_by_caption[basename].nil?
    others_by_caption[basename] << r
  end
  ci_new_groups = others_by_caption.values.map{|v|v.map(&:id).to_set}.to_set
  if ci_new_groups==ci_init_groups
    puts "not changed"
  else
    puts "change!!!"
    puts "old groups"
    pp ci_init_groups
    puts "new groups"
    pp ci_new_groups
  end
end

def import_gridded_pdfs
  import_unimported(/Stations.*Gridded Images.*pdf$/,"blank") do |row,station,l|
    if mf = station.metadata_files.create(metadata_file: FilelessIO.new(row[0]), legacy_path: row[0])
      row[1]="Y"
      row[2]=mf.id.to_s
      row[3]=mf.class.name
      row[4]=""
      l.update_row(row)
      counter+=1
      puts "Updated!"
    else
      puts "Failed to import."
    end
  end
end

def import_old(noop=false,exact=true)
  import_unimported(/Stations.*\/old-[^\/]+$/, /old/) do |row,station,l|
    image_state = "INTERIM"
    basename = $1 if row[0]=~/.*Stations.*\/old-([^\/]+)\.(tif|tiff|jpg|jpeg)$/i
    hcaps = station.historic_captures
    hcap_images = hcaps.map{|h|h.capture_images}.flatten.compact
    hcap_images.each do |hci|
      if (exact==true && basename==hci.display_name.chomp("."+hci.image.file.extension)) || (exact==false && historic_similarity_check(basename, hci.display_name.chomp("."+hci.image.file.extension) ) ) #      if m.metadata_file.file.basename==hci.display_name.chomp("."+hci.image.file.extension)
        hcap = hci.captureable
        if noop
          puts "noop: Updated! Put file '#{row[0].slice(-80..-1)}' in historic capture #{hcap.display_name} (#{hcap.id})"
          break
        elsif new_img=hcap.capture_images.create(image: FilelessIO.new(row[0]), image_state: image_state, legacy_path: row[0])
          row[1]="Y"
          row[2]=new_img.id.to_s
          row[3]=new_img.class.name
          row[4]=""
          l.update_row(row)
          @counter+=1
          puts "Updated! Put file '#{row[0].slice(-80..-1)}' in historic capture #{hcap.display_name} (#{hcap.id}). Capture image is #{new_img.id}"
          #FileUtils.rm(Dir.glob("/private/var/folders/z7/4gdh6fps3874q8tp2rkyk5v4000rqy/T/magick*"))
          break
        else
          puts "hmm, tried to add '#{row[0].slice(-80..-1)}' to #{hcap.display_name} (#{hcap.id}), but no luck"
        end
      end
    end
  end
end

#recipes:
#Import fff with Cannot error message. about 262 still left. Will either need to add them as unsorted into station or ?
#import_general(noop: true, compare_method: "ci_caption_to_basename", file_filter: /\/Stations.*\/Stn. 64/B0.+\.fff/, msg_filter: /Cannot/, image_state: "RAW", container: "capture")
def import_general(options = {})
  options.reverse_merge({compare_method: "basename", noop: false, file_filter: /match nothing/, msg_filter: /match nothing/, image_state: "MISC", basename_regexp: nil, container: nil, create_cap: false })
  compare_method, noop, file_filter, msg_filter, image_state, basename_regexp, container, create_cap = options[:compare_method], options[:noop], options[:file_filter], options[:msg_filter], options[:image_state], options[:basename_regexp], options[:container], options[:create_cap]
  raise StandardError, "Container must be set to either capture or hcapture or location" unless container=="capture" || container=="hcapture" || container=="location"
  import_unimported(file_filter,msg_filter) do |row,station,l|
    image_state = get_image_state(row[0]) if image_state.blank?
    if basename_regexp.present? && row[0]=~basename_regexp
      basename = $1
    elsif row[0]=~/.*Stations.*\/([^\/]+)\.[[:alnum:]]{3,4}$/i
      basename = $1
    else
      puts "Can't get the basename for #{row[0]}"
      next
    end
    caps = nil
    if container=="capture"
      caps = station.captures
      cap_images = caps.map{|c|c.capture_images}.flatten.compact
    elsif container=="hcapture"
      caps = station.historic_captures
      cap_images = caps.map{|c|c.capture_images}.flatten.compact
    elsif container=="location"
      compare_method="location"
    end
    cap = nil #cap used as container
    case compare_method
    when "basename"
      cap_images.each do |ci|
        ci_basename = $1 if ci.legacy_path=~/.*\/([^\/]+)\.[[:alnum:]]{3,4}/i
        if basename==ci_basename
          cap = ci.captureable
          break
        end
      end
    when "ci_caption_to_basename"
      cap_images.each do |ci|
        caption = MiniExiftool.new(ci.legacy_path, :convert_encoding => true).try(:captionabstract)
        if caption==basename
          cap = ci.captureable
          break
        end
      end
    when "ci_basename_to_caption"
      caption = MiniExiftool.new(row[0], :convert_encoding => true).try(:captionabstract)
      cap_images.each do |ci|
        ci_basename = $1 if ci.legacy_path=~/.*\/([^\/]+)\.[[:alnum:]]{3,4}/i
        if caption==ci_basename
          cap = ci.captureable
          break
        end
      end
    when "similarity_to_basename"
      cap_images.each do |ci|
        ci_basename = $1 if ci.legacy_path=~/.*\/([^\/]+)\.[[:alnum:]]{3,4}/i
        if (container=="capture" && repeat_similarity_check(basename,ci_basename)) || (container=="hcapture" && historic_similarity_check(basename,ci_basename) )
          cap = ci.captureable
          break
        end
      end
    when "similarity_to_fn_ref"
      caps.each do |c|
        if (container=="capture" && c.fn_photo_reference.present? && repeat_similarity_check(c.fn_photo_reference,basename))
          cap = c
          break
        elsif(container=="hcapture" && c.fn_photo_reference.present? && historic_similarity_check(c.fn_photo_reference,basename))
          cap = c
          break
        end
      end
    when "capturetime"
      m = MiniExiftool.new(row[0], :convert_encoding => true)
      captime_dt = m.datetimeoriginal
      captime_gpsdt = m.gpstimestamp
      if captime_dt || captime_gpsdt
        cap_images.each do |ci|
          m_ci = MiniExiftool.new(ci.legacy_path, :convert_encoding => true)
          ci_captime_dt = m_ci.datetimeoriginal
          ci_captime_gpsdt = m_ci.gpstimestamp
          if captime_dt && ci_captime_dt == captime_dt
            cap = ci.captureable
            break
          elsif captime_gpsdt && ci_captime_gpsdt == captime_gpsdt
            cap = ci.captureable
            break
          end
        end
      end
    when "markX" #marks import spread with X, denoting that file does not need to get imported
      if !noop
        row[1]="X"
        row[2]=""
        row[3]=""
        row[4]=""
        l.update_row(row)
      end
    when "markM" #marks import spread with X, denoting that file does not need to get imported
      if !noop
        row[1]="M"
        row[2]=""
        row[3]=""
        row[4]=""
        l.update_row(row)
      end
    when "manual"
      if container!="capture" && container!="hcapture"
        puts "Manual mode only works with capture and hcaptuer containers"
        return
      end
      puts "Choose a capture image which most closely matches ('c' to create new capture):"
      m = MiniExiftool.new(row[0], :convert_encoding => true)
      puts "#{basename} #{m.datetimeoriginal} #{m.gpstimestamp} #{m.captionabstract}"
      fastmode=true
      for i in 1..2
        id_hash = {}
        counter=0
        cap_images.each do |ci|
          if fastmode
            puts "[#{counter}] (#{ci.id}) #{ci.display_name} cap_ref: #{ci.captureable.fn_photo_reference}"
          else
            m_ci = MiniExiftool.new(ci.legacy_path, :convert_encoding => true)
            puts "[#{counter}] (#{ci.id}) #{ci.display_name} datetime:#{m_ci.datetimeoriginal} gpstime:#{m_ci.gpstimestamp} caption:#{m_ci.captionabstract} cap_ref: #{ci.captureable.fn_photo_reference}"
          end
          id_hash[counter] = ci.id
          counter+=1
        end
        #we'll add the option to select a capture with no capture images but with fn reference
        cap_start = counter
        caps.each do |c|
          if c.fn_photo_reference.present? && c.capture_images.count==0
            puts "[#{counter} (#{c.id}) CAPTURE #{c.display_name}]"
            id_hash[counter] = c.id
            counter+=1
          end
        end

        if fastmode
          puts "Do you want capture images with details (Y)?"
          answer = STDIN.gets
          answer.chomp!
          if answer=="Y"
            fastmode=false
          else
            break
          end
        end
      end
      if answer=="Y"
        answer = STDIN.gets
        answer.chomp!
      end
      #'c' means create
      if answer=='c'
        create_cap=true
      elsif answer=='d' #'d' means mark as duplicate in import spreadsheet
        create_cap=false
        row[1]="D"
        row[2]=""
        row[3]=""
        row[4]=""
        l.update_row(row)
      elsif answer.present? && answer=~/\d+/
        create_cap=false
        if cap_start<=answer.to_i
          if container=="capture"
            cap = Capture.find(id_hash[answer.to_i])
          elsif container=="hcapture"
            cap = HistoricCapture.find(id_hash[answer.to_i])
          end
        else
          cap = CaptureImage.find(id_hash[answer.to_i]).captureable
        end
      else
        create_cap=false
      end
    when "location","fn","md"
      cap=nil
    else
      raise StandardError, "compare method, #{compare_method} not recognized"
    end
    if cap.blank? && create_cap && (container=="hcapture" || container=="capture")
      if station.visits.count==1
        if noop
          puts "noop: Create cap in visit"
        else
          if container=="capture"
            cap = station.visits.first.unsorted_captures.create
            puts "Create cap in visit (#{cap.id})"
          elsif container=="hcapture"
            cap = station.historic_visit.historic_captures.create
            puts "Create hcap in hvisit (#{cap.id})"
          end
        end
      else
        if noop
          puts "noop: Create cap in station"
        else
          if container=="capture"
            cap = station.unsorted_captures.create
            puts "Create cap in station (#{cap.id})"
          elsif container=="hcapture"
            cap = station.historic_visit.historic_captures.create
            puts "Create hcap in hvisit (#{cap.id})"
          end
        end
      end
    end
    if cap.present?
      if noop
        puts "noop: Updated! Put file '#{row[0].slice(-80..-1)}' in capture #{cap.display_name} (#{cap.id})"
        @counter+=1
      elsif new_img=cap.capture_images.create(image: FilelessIO.new(row[0]), image_state: image_state, legacy_path: row[0])
        row[1]="Y"
        row[2]=new_img.id.to_s
        row[3]=new_img.class.name
        row[4]=""
        l.update_row(row)
        @counter+=1
        puts "Updated! Put file '#{row[0].slice(-80..-1)}' in capture #{cap.display_name} (#{cap.id}). Capture image is #{new_img.id}"
      else
        puts "hmm, tried to add '#{row[0].slice(-80..-1)}' to #{cap.display_name} (#{cap.id}), but no luck"
      end
    elsif container=="location"
      if noop
        puts "noop: Updated! Added file '#{row[0].slice(-80..-1)}' in a location container"
      else
        #put the file in the most specific location available
        #if there are more than one location or visit, the choice is ambigous, so store higher up in the hierarchy
        lp=nil
        if station.visits.count==1 && station.visits.first.locations.count==1
          loc = station.visits.first.locations.first
          lp=loc.location_photos.create(image: FilelessIO.new(row[0]),legacy_path: row[0])
        elsif station.visits.count==1
          visit = station.visits.first
          lp=visit.unsorted_location_photos.create(image: FilelessIO.new(row[0]),legacy_path: row[0])
        else
          lp=station.unsorted_location_photos.create(image: FilelessIO.new(row[0]),legacy_path: row[0])
        end
        if lp
          row[1]="Y"
          row[2]=lp.id.to_s
          row[3]=lp.class.name
          row[4]=""
          l.update_row(row)
          @counter+=1
          puts "Updated! Added file '#{row[0].slice(-80..-1)}' in #{lp.image_owner_type} id: #{lp.image_owner_id}. LocationImage id: #{lp.id}"
        else
          puts "hmm, tried to add '#{row[0].slice(-80..-1)}', but no luck"
        end
      end
    end
  end
end

def get_image_state(path)
  if path.match(/Image Variant.*Repeat Master/)
    "MISC"
  elsif path.match(/Image Variant.*Original Master/)
    "MISC"
  elsif path.match(/Image Variant.*Repeat 3FR/)
    "MISC"
  elsif path.match(/Image Variant.*Repeat Tiff/)
    "MISC"
  elsif path.match(/Image Variant.*Original Scan/)
    "MISC"
  elsif path.match(/Repeat Master/)
    "MASTER"
  elsif path.match(/Repeat 3FR/)
    "RAW"
  elsif path.match(/Repeat Tiff/)
    "INTERIM"
  elsif path.match(/Original Masters\/old-/)
    "INTERIM"
  elsif path.match(/Original Master/)
    "MASTER"
  elsif path.match(/Original Scan/)
    "RAW"
  elsif path.match(/Gridded/)
    "GRIDDED"
  else
    "MISC"
  end
end



def import_historic_psd(noop=false)
  import_unimported(/Stations.*psd$/,"blank") do |row,station,l|
    next if row[0]=~/Repeat/
    image_state = ""
    #figure out the image state based on the path name
    if row[0] =~ /.*Gridded.*/
      image_state = "GRIDDED"
    elsif row[0] =~ /.*Original Masters.*/
      image_state = "MASTER"
    elsif row[0] =~ /.*Original Scan.*/
      image_state = "RAW"
    elsif row[0] =~ /.*Image Variants.*/
      image_state = 'MISC'
    else
      next
    end
    basename = $1 if row[0]=~/.*Stations.*\/([^\/]+)\.psd$/
    hcaps = station.historic_captures
    hcap_images = hcaps.map{|h|h.capture_images}.flatten.compact
    hcap_images.each do |hci|
      if historic_similarity_check(basename, hci.display_name.chomp("."+hci.image.file.extension) ) #      if m.metadata_file.file.basename==hci.display_name.chomp("."+hci.image.file.extension)
        hcap = hci.captureable
        if noop
          puts "noop: Updated! Put file '#{row[0].slice(-80..-1)}' in historic capture #{hcap.display_name} (#{hcap.id})"
          break
        elsif new_img=hcap.capture_images.create(image: FilelessIO.new(row[0]), image_state: image_state, legacy_path: row[0])
          row[1]="Y"
          row[2]=new_img.id.to_s
          row[3]=new_img.class.name
          row[4]=""
          l.update_row(row)
          counter+=1
          puts "Updated! Put file '#{row[0].slice(-80..-1)}' in historic capture #{hcap.display_name} (#{hcap.id}). Capture image is #{new_img.id}"
          FileUtils.rm(Dir.glob("/private/var/folders/z7/4gdh6fps3874q8tp2rkyk5v4000rqy/T/magick*"))
          break
        else
          puts "hmm, tried to add '#{row[0].slice(-80..-1)}' to #{hcap.display_name} (#{hcap.id}), but no luck"
        end
      end
    end
  end
end


def get_station(path)
  #taking into account one odd station where I had to change the name
  if path=~/RGe29 West 4th Meridian/
    return Station.find(1269)
  elsif path =~/Monument 272/
    return Station.find(531)
  elsif path =~ /(.+)\/Stations\/([^\/]+).*/ && season = SurveySeason.find_by_legacy_path($1)
    return season.stations.find_by_name($2)
  end
end

#one off method to move metadata folder in old file structure to new library as unmanaged metadata
#also updates import log
def import_metadata_folders
  debugger
  ActiveRecord::Base.logger = nil;
  l = ImportLogCSV.new("log/import_log_google.csv")
  m_rows = l.grep_filter(/\/Metadata\//,"all","all","all","all")
  m_files = m_rows.map{|a|a[0]}
  grouped_by_parent_folders = m_files.group_by{|a| a=~/(.*)\/Metadata/;$1}
  parent_folders = grouped_by_parent_folders.keys
  parent_folders.each do |folder|
    obj = Project.find_by_legacy_path(folder) || Survey.find_by_legacy_path(folder) || SurveySeason.find_by_legacy_path(folder)
    if obj.nil?
      puts "Folder #{folder} did not have an object with the legacy path in the db"
      next
    end
    puts "Copyings #{obj.legacy_path}/Metadata to #{obj.filesystem_path} "
    if !Dir.exists?(obj.filesystem_path)
      FileUtils.mkpath(obj.filesystem_path)
    end
    FileUtils.cp_r("#{obj.legacy_path}/Metadata",obj.filesystem_path)

    sleep(1)
    raise "copy not working as expected (#{obj.filesystem_path}/Metadata)" unless Dir.exists?("#{obj.filesystem_path}/Metadata")
    grouped_by_parent_folders[folder].each do |file|
      row = [file,"M","","",""]
      l.update_row(row)
    end
  end
ensure
  l.save
end

def import_scenics(file_date_opt=false)
  debugger
  ActiveRecord::Base.logger=nil
  l = ImportLogCSV.new("log/import_log_google.csv")
  m_rows = l.grep_filter(/\/Scenics\//, "all","all","all","all")
  m_files = m_rows.map{|a|a[0]}
  m_files.each do |f_path|
    date = get_photo_date(f_path,file_date_opt)
    if date.present? && date.year.to_i>=2002 && date.year.to_i<=2012
      puts "Copying #{f_path} to field season scenics #{date.year.to_s}"
      FileUtils.cp(f_path,File.join("/Volumes/mlp/MLPLibraryNew/Unmanaged_content/Scenics_by_Field_Season",date.year.to_s))
      row = [f_path, "U","","",""]
      l.update_row(row)
    else
      if date.blank?
        puts "Could not get date for #{f_path}"
        puts "File dates #{File.ctime(f_path)}"
      else
        puts "Date is not within season years. #{date.to_s}"
      end
      row = [f_path, "N","","","needs to be moved to field season scenics"]
    end
  end
ensure
  l.save
end

def get_photo_date(file_path,file_date_opt)
  exif = MiniExiftool.new(file_path, :convert_encoding => true)
  date = exif.datetimeoriginal or exif.gpstimestamp
  if date.blank? && file_date_opt
    date = File.ctime(file_path)
  end
  return date
end

def import_stn_meta
  debugger
  ActiveRecord::Base.logger=nil
  l = ImportLogCSV.new("log/import_log_google.csv")
  m_rows = l.grep_filter(/\/Stations\/[^\/]+\//, "blank","all","all","all")
  m_files = m_rows.map{|a|a[0]}
  m_files.each do |f_path|
    if f_path=~/(.*)\/Stations\/([^\/]+)\//
      season_path = $1
      station_name = $2
      season = SurveySeason.find_by_legacy_path(season_path)
      station = Station.where(name: station_name, station_owner_id: season.id).first
      if f_path !~/Field Notes/
        puts "Adding #{f_path} as metadata file on station #{station.name}?"
        answer = STDIN.gets
        answer.chomp!
        if answer=="Y"
          md = station.metadata_files.create(metadata_file: FilelessIO.new(f_path), legacy_path: f_path)
          row = [f_path,"Y",md.id.to_s,md.class.name,""]
          l.update_row(row)
        end
      end
    end
  end
ensure
  l.save
end

def import_multi_visit_fns
  debugger
  ActiveRecord::Base.logger=nil
  l = ImportLogCSV.new("log/import_log_google.csv")
  m_rows = l.grep_filter(/Field Notes/, /N/,"all","all",/More than one visit/)
  m_files = m_rows.map{|a|a[0]}

  last_parent_path=nil
  last_station_name=nil
  m_files.each do |f_path|
    if f_path=~/(.*)\/Stations\/([^\/]+)\//
      parent_path = $1
      station_name = $2
      parent = SurveySeason.find_by_legacy_path(parent_path) or Project.find_by_legacy_path(parent_path)
      station = Station.where(name: station_name, station_owner_id: parent.id).first
      visits = station.visits

      fns = visits.map{|v|v.field_notes}.flatten.compact.map{|fn|fn.field_note_file.file.filename}
      if fns.present? && fns.any?{|f|f==File.basename(f_path)}
        puts "The field note has already been manually imported. #{f_path}"
        row = [f_path,"M","","",""]
        l.update_row(row)
        next
      end
      `open "#{parent_path}/Stations/#{station_name}"` if station_name !=last_station_name

      puts "Choose a visit for file #{f_path}"
      visits.each_with_index {|v,i| puts "(#{i+1} #{v.date.to_s}"}

      answer = STDIN.gets
      answer.chomp!
      answer = answer.to_i
      if answer>=1 && answer <=visits.count
        fn = visits[answer-1].field_notes.create(field_note_file: FilelessIO.new(f_path), legacy_path: f_path)
        row = [f_path,"Y",fn.id.to_s,fn.class.name,""]
        l.update_row(row)
      end
      last_parent_path = parent_path
      last_station_name = station_name
    end
  end
ensure
  l.save
end

def import_DOT
  debugger
  ActiveRecord::Base.logger=nil
  l = ImportLogCSV.new("log/import_log_google.csv")
  m_rows = l.grep_filter(/Digital Object Tracker/, "all","all","all","all")
  m_files = m_rows.map{|a|a[0]}
  m_files.each do |f_path|
    if f_path=~/(.*)\/Digital Object Tracker.+/
      parent_path = $1
      parent = Survey.find_by_legacy_path(parent_path) or SurveySeason.find_by_legacy_path(parent_path) or Project.find_by_legacy_path(parent_path)
      if parent.present?
        puts "Copying #{f_path} to #{parent.umetadata_path}"
        FileUtils.cp(f_path,parent.umetadata_path)
        row = [f_path,"M","","",""]
        l.update_row(row)
      end
    end
  end
ensure
  l.save
end

class Array
  #returns the index of the first matching element in the array
  def index_match(regexp)
    self.index { |x| x=~regexp if !x.nil?}
  end

  #returns the index of all the matching elements in the array
  def index_grep(regexp)
    unless regexp.blank?
      #first try exact match as this will be way faster
      index = self.index(regexp)
      if index.present?
        return [index]
      else
        if regexp.class==Regexp
          self.each_with_index.select{|e,idx|e=~regexp}.map{|i|i[1]}
        elsif regexp.class==String
          self.each_with_index.select{|e,idx|e=~/#{regexp}/}.map{|i|i[1]}
        end
      end
    else
      nil
    end
  end

  def index_select
    indices = []
    self.each_with_index.select{|e,idx| yield e }.map{|i|i[1]}
  end

end


require 'csv'
class ImportLogCSV
  def log_array
    @log_array
  end

  def initialize(import_log, save_file=nil)
    @log_changed = false
    @log_file = import_log
    @save_file= save_file || @log_file
    @log_array = CSV.read(@log_file)
    #vert array groups by columns rather than rows
    @log_vert_array = [[],[],[],[],[]]
    @log_array.each{|r| @log_vert_array[0]<< r[0]; @log_vert_array[1]<< r[1]; @log_vert_array[2]<< r[2]; @log_vert_array[3]<< r[3]; @log_vert_array[4]<< r[4]; }
  end
  #this function will filter the rows of the csv based on regular expression matches in the column arguements.
  #defaults to empty RegExp for each column (return everything)
  def grep_filter(file_filter="all",status_filter='all',id_filter='all',type_filter='all', msg_filter='all')
    all_indices = Range.new(0,@log_array.count-1).to_a
    if file_filter == "all"
      file_indices = all_indices
    elsif file_filter == "blank"
      file_indices = @log_vert_array[0].index_select{|l|l.blank? }
    else
      file_indices = @log_vert_array[0].index_grep(file_filter)
    end
    if status_filter == "all"
      status_indices = all_indices
    elsif status_filter == "blank"
       status_indices = @log_vert_array[1].index_select{|l|l.blank? }
    else
      status_indices = @log_vert_array[1].index_grep(status_filter)
    end
    if id_filter == "all"
      id_indices = all_indices
    elsif id_filter == "blank"
      id_indices = @log_vert_array[2].index_select{|l|l.blank? }
    else
      id_indices = @log_vert_array[2].index_grep(id_filter)
    end
    if type_filter == "all"
      type_indices = all_indices
    elsif type_filter == "blank"
      type_indices =  @log_vert_array[3].index_select{|l|l.blank? }
    else
      type_indices = @log_vert_array[3].index_grep(type_filter)
    end
    if msg_filter == "all"
      msg_indices = all_indices
    elsif msg_filter == "blank"
      msg_indices = @log_vert_array[4].index_select{|l|l.blank? }
    else
      msg_indices = @log_vert_array[4].index_grep(msg_filter)
    end
    indices = file_indices & status_indices & id_indices & type_indices & msg_indices
    return indices.map{|i| @log_array[i]}
  end

  def update_row(row)
    @log_changed = true if @log_changed == false
    row_index = @log_vert_array[0].index(row[0])
    if row_index.present?
      @log_array[row_index]=row
    end
  end

  def save
    if @log_changed
      CSV.open(@save_file, "w"){|csv| @log_array.each{|row| csv << row } }
    else
      puts "Log has not changed. Not saving"
    end
    nil
  end

end

def recreate_versions(range)
  start = Time.now
  failed = []
  count = 0
  processed = 0
  total = CaptureImage.where(id: range).count
  CaptureImage.where(id: range).each do |ci|
    count = count + 1
    puts "percent #{(count*100.0)/total} %"
    begin
    unless VIPS::Image.new(ci.image.medium.file.path).x_size>450
      ci.process_image_upload = true
      ci.image.recreate_versions!
      processed = processed + 1
    end
    rescue => e
    failed << [ci.id, e.message]
    end
  end
  finish = Time.now
  puts "estimate time per image: #{(finish-start)/processed}"
  return failed
end
