require 'csv'
load(Rails.root.join('lib','tasks','task_helper_methods.rb'))
namespace :archival do
  task :import_survey_archives, [:import_file] => :environment do |t,args|
    CSV.foreach(args[:import_file]) do |row|
      lastname, initials, givennames = parse_surveyor(row[1]);
      year = row[2]
      affiliation = row[3]
      jurisdiction = row[4]
      location = row[5]
      sources = row[6]
      notes = row[7]
      archive = row[8]
      collection = row[9]
      record_id = row[0]

      puts "lastname: #{lastname}"
      puts "intials: #{initials}"
      puts "givennames: #{givennames}"

      if lastname[/Royal Engineers/]
        #find the surveyor "Roayl Engineers"
        surveyors = Surveyor.where(affiliation: "Royal Engineers")
      else
        surveyors = Surveyor.where(last_name: lastname, given_names: givennames)
        if surveyors.count == 0
          surveyors = Surveyor.where(last_name: lastname, given_names: initials)
        end
      end
      surveyor = nil
      if surveyors.count == 1
        puts "Match found"
        surveyor = surveyor = surveyors.to_a[0];
      elsif surveyors.count > 1
        puts "Multiple matches for #{lastname}. Pick one"
        surveyor = surveyors.to_a.find do |s|
          s.given_names == givennames || s.given_names == initials
        end
      end

      if surveyor.nil?
        puts "NO SURVEYOR FOUND"
        #create surveyor
        #short_name = "#{lastname[0..2]}#{(givennames.try(:slice, 0, 1).try(:short_name) || initials.try(:slice, 0, 1).try(:short_name))}".upcase
        given_names = givennames || initials
        surveyor = Surveyor.new(last_name: lastname, given_names: given_names)
        surveyor.save!
      end

      season = surveyor.survey_seasons.where(year: year).first

      if season.nil?
        puts "NO SEASON FOUND"
        survey = surveyor.surveys.find_by(name: "Unknown")
        if survey.nil?
          survey = surveyor.surveys.create(name: "Unknown")
        end
        #create new survey season in unknown survey
        season = survey.survey_seasons.create(year: year);
      end
      season.affiliation = affiliation
      season.jurisdiction = jurisdiction
      season.location = location
      season.sources = sources
      season.notes = notes
      season.archive = archive
      season.collection = collection
      season.record_id = record_id

      pp season
      season.save!
    end
  end

  task :import_glass_plate_listings, [:import_file] => :environment do |t,args|
    CSV.foreach(args[:import_file]) do |row|
      record_id = row[1]
      container = row[2]
      plates = row[3]
      notes = row[4]
      #puts "#{record_id}||||#{container}||||#{plates}||||#{notes}"
      #make sure we don't have this record already
      if GlassPlateListing.where(plates: plates, notes: notes).count == 0
        if ss = SurveySeason.find_by(record_id: record_id)
          GlassPlateListing.create(survey_season_id: ss.id, container: container, plates: plates, notes: notes)
        else
          puts "Glass Plate Listings does not have record in meat: #{row}"
        end
        #
      end
    end
  end

  task :import_map_listings, [:import_file] => :environment do |t,args|
    CSV.foreach(args[:import_file]) do |row|
      record_id = row[1]
      nts_map = row[2]
      historic_map = row[3]
      links = row[4]
      #puts "#{record_id}||||#{container}||||#{plates}||||#{notes}"
      #make sure we don't have this record already
      if Map.where(nts_map: nts_map, historic_map: historic_map, links: links).count == 0
        if ss = SurveySeason.find_by(record_id: record_id)
          Map.create(nts_map: nts_map, historic_map: historic_map, links: links, survey_season_id: ss.id)
        else
          puts "Maps does not have record in meat: #{row}"
        end
      end
    end
  end

  task :test do |t|
    SurveySeason.all.to_a.each do |s|
      if s.maps.count == 0
        puts "Survey Season has zeros maps:"
        pp s.parent.parent.last_name
        pp s
      end
    end

    Map.all.to_a.each do |m|
      if m.survey_season.nil?
        puts "Map does not have survey season"
        pp m
      end
    end

    GlassPlateListing.all.to_a.each do |g|
      if g.survey_season.nil?
        puts "GlassPlateListing does not have survey season"
        pp g
      end
    end
  end
end

def parse_surveyor(surveyor)
  return surveyor.scan(/(.+?), (.*)\s{0,1}\((.+?)\)\.{0,1}$/).first ||
    surveyor.scan(/(.+?), (.*)$/).first ||
    surveyor.scan(/(.+)/).first
rescue
  puts "???" + surveyor.scan(/(.+?),/).first
end