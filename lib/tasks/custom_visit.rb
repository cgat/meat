class CustomVisit < Visit
  def self.find_by_info(date, station_name, survey_year)
    if date.class==Date
      possible_stations = Station.where{name==my{station_name}}
      visits = Visit.where{station_id.in(possible_stations.select{id})}.select{|v| v.date==date}
      if visits.count==1
        return visits.first
      else
        puts "no visit matches or too many (#{visits.count}"
        return false
      end
    elsif (date.class==String && date=~/\d{4}/) || date.class==Fixnum
      date_year = date.to_i
      possible_survey_seasons = SurveySeason.where{year == my{survey_year.to_i}}
      stations = Station.where{(name == my{station_name}) & (station_owner_id.in(possible_survey_seasons.select{id}))}
      if stations.count==1 && stations.first.visits.count==1 && stations.first.visits.first.date.year==date_year
        return stations.first.visits.first
      elsif stations.count==1 && stations.first.visits.count>1
        matches = stations.first.visits.select{|v|v.date.year==date_year}
        if matches.count==1
          return matches.first
        else
          puts "visit ambiguous from station"
          return false
        end
      else
        puts "more than one station"
        return false
      end
    end
  end
end
